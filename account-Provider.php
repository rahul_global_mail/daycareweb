<?php
ob_start();
require_once 'user-includes/config.inc.php';
require_once USER_MODEL_PATH . 'provider-management.model.php';
//echo "<pre>";
//print_r($_SESSION['providerData']);

$provider_id = $_SESSION['providerData'][0]->provider_id;
$model_provider = new ModelProvidermanage();
$serviceData = $model_provider->get_careservices();
$benefitsData = $model_provider->get_carebenefits();
$cityData = $model_provider->get_city();
$providerData = $model_provider->get_providerDetail($provider_id);
$vacancyData = $model_provider->get_myVacancy($provider_id);
$galleryData = $model_provider->get_myGallery($provider_id);
$feesData = $model_provider->get_myFees($provider_id);
$waitlistData = $model_provider->get_provider_waitlist($provider_id);
$chilData = $model_provider->get_provider_vacancy($provider_id);
$subscriptions = $model_provider->get_providerSubscriptions($provider_id);
$redirect = 'account-Provider.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$_SESSION['add_msg'] = '';
	if (isset($_POST['uploadBanner'])) {
		$res = $model_provider->upload_banner($_POST);
		if ($res > 0) {
			$_SESSION['add_msg'] = 'success';
		} else {
			$_SESSION['add_msg'] = 'error';
		}
		header("Location:" . $redirect);
		exit;
		//echo "<script>window.location=\"account-Provider.php\"</script>";
	}
	if (isset($_POST['updateProfile'])) {
		$res = $model_provider->update_careprofile($_POST);
		if ($res > 0) {
			$_SESSION['add_msg'] = 'success';
		} else {
			$_SESSION['add_msg'] = 'error';
		}
		header("Location:" . $redirect);
		exit;
	}
	if (isset($_POST['updateWaitlist'])) {
		$res = $model_provider->update_profile_waitlist($_POST);
		if ($res > 0) {
			$_SESSION['add_msg'] = 'success';
		} else {
			$_SESSION['add_msg'] = 'error';
		}
		header("Location:" . $redirect);
		exit;
	}
	if (isset($_POST['updateCareservice'])) {
		$res = $model_provider->update_service_details($_POST);
		if ($res > 0) {
			$_SESSION['add_msg'] = 'success';
		} else {
			$_SESSION['add_msg'] = 'error';
		}
		header("Location:" . $redirect);
		exit;
	}
	if (isset($_POST['addCarefees'])) {
		$res = $model_provider->add_care_fees($_POST);
		if ($res > 0) {
			$_SESSION['add_msg'] = 'success';
		} else {
			$_SESSION['add_msg'] = 'error';
		}
		header("Location:" . $redirect);
		exit;
	}
	if (isset($_POST['updateVacancy'])) {
		$res = $model_provider->update_profile_vancancy($_POST);
		if ($res > 0) {
			$_SESSION['add_msg'] = 'success';
		} else {
			$_SESSION['add_msg'] = 'error';
		}
		header("Location:" . $redirect);
		exit;
	}
}
require_once USER_VIEW_PATH . 'account-careprovider.view.php';
?>