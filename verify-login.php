<?php
ob_start();
require_once 'user-includes/config.inc.php';
if (isset($_GET['verifyID']) && !empty($_GET['verifyID']) && isset($_GET['action']) && $_GET['action'] == "verifyuser" && isset($_GET['uID']) && !empty($_GET['uID'])) {
	require_once USER_VIEW_PATH . 'verify-login.view.php';
} else {
	require_once USER_VIEW_PATH . 'nodata.view.php';
}

?>