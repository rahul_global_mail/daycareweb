<?php
ob_start();
require_once 'user-includes/config.inc.php';
require_once USER_MODEL_PATH . 'provider-management.model.php';
$model_provider = new ModelProvidermanage();
if (isset($_GET['careID']) && !empty($_GET['careID'])) {
	$provider_id = base64_decode($_GET['careID']);
	$providerData = $model_provider->get_providerDetail($provider_id);
	$vacancyData = $model_provider->get_myVacancy($provider_id);
	$galleryData = $model_provider->get_myGallery($provider_id);
	$feesData = $model_provider->get_myFees($provider_id);
	$serviceData = $model_provider->get_careservices();
	$waitlistData = $model_provider->get_provider_waitlist($provider_id);
	$locationData = $model_provider->get_carelatlong($providerData[0]->postcode);
	// check subscription
	if ($providerData[0]->subscription == 'free') {
		require_once USER_VIEW_PATH . 'details-careprovider-free.view.php';
	} else {
		require_once USER_VIEW_PATH . 'details-careprovider.view.php';
	}

} else {
	require_once USER_VIEW_PATH . 'nodata.view.php';
}

?>