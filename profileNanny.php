<?php
ob_start();
require_once 'user-includes/config.inc.php';
require_once USER_MODEL_PATH . 'nanny-management.model.php';
$model_nanny = new ModelNannymanage();
if (isset($_GET['nanID']) && !empty($_GET['nanID'])) {
	$nanny_id = base64_decode($_GET['nanID']);
	$nannyData = $model_nanny->get_nannyDetail($nanny_id);
	require_once USER_VIEW_PATH . 'nanny-profile.view.php';
} else {
	require_once USER_VIEW_PATH . 'nodata.view.php';
}

?>