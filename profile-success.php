<?php
ob_start();
require_once 'user-includes/config.inc.php';
if (isset($_GET['verifyID']) && !empty($_GET['verifyID']) && isset($_GET['action'])) {
	require_once USER_MODEL_PATH . 'provider-management.model.php';
	$model_provider = new ModelProvidermanage();

	require_once USER_VIEW_PATH . 'profile-success.view.php';
} else {
	require_once USER_VIEW_PATH . 'nodata.view.php';
}

?>