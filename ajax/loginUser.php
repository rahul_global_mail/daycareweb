<?php
ob_start();
require_once '../user-includes/config.inc.php';
require_once USER_MODEL_PATH . 'parent-management.model.php';
$model_user = new ModelParentmanage();
require_once USER_MODEL_PATH . 'nanny-management.model.php';
$model_nanny = new ModelNannymanage();
require_once USER_MODEL_PATH . 'provider-management.model.php';
$model_provicer = new ModelProvidermanage();

if (isset($_POST["action"]) && $_POST["action"] == 'submituserdata') {
	if (isset($_POST['user_type']) && $_POST['user_type'] == 'parent') {
		$parent = $model_user->get_ParentLogin($_POST);
		if ($parent[0]->parent_id != '' or $parent[0]->parent_id != 0) {
			$_SESSION['parentData'] = $parent;
			echo 'success-parent';
			exit;
		} else {
			echo "fail";
		}
	} elseif (isset($_POST['user_type']) && $_POST['user_type'] == 'care_provider') {
		$provider = $model_provicer->get_careproviderLogin($_POST);
		if ($provider[0]->provider_id != '' or $provider[0]->provider_id != 0) {
			$_SESSION['providerData'] = $provider;
			echo 'success-provider';
			exit;
		} else {
			echo "fail";
		}
	} elseif (isset($_POST['user_type']) && $_POST['user_type'] == 'nanny') {
		$nanny = $model_nanny->get_NannyLogin($_POST);
		if ($nanny[0]->nanny_id != '' or $nanny[0]->nanny_id != 0) {
			$_SESSION['nannyData'] = $nanny;
			echo 'success-nanny';
			exit;
		} else {
			echo "fail";
		}
	} else {
		echo "fail";
	}

}

?>