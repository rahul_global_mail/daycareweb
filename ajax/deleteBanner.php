<?php
ob_start();
require_once '../user-includes/config.inc.php';
require_once USER_MODEL_PATH . 'provider-management.model.php';
$model_provider = new ModelProvidermanage();

if (isset($_POST["action"]) && $_POST["action"] == 'bannerDelete') {
	if (isset($_POST['banner_id']) && !empty($_POST['banner_id'])) {
		$banner_id = $_POST['banner_id'];
		$parent = $model_provider->removebanner($banner_id);
	}
} else {
	echo "fail";
}

?>