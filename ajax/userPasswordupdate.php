<?php
ob_start();
require_once '../user-includes/config.inc.php';
require_once USER_MODEL_PATH . 'parent-management.model.php';
$model_user = new ModelParentmanage();
require_once USER_MODEL_PATH . 'provider-management.model.php';
$model_provider = new ModelProvidermanage();
require_once USER_MODEL_PATH . 'nanny-management.model.php';
$model_nanny = new ModelNannymanage();

if (isset($_POST["action"]) && $_POST["action"] == 'userpasswordchange') {
	if (isset($_POST['user_type']) && $_POST['user_type'] == 'parent') {
		if ($_POST['newpassword'] == $_POST['confirmpassword']) {
			$parent = $model_user->accountPassword_parent($_POST);
			if ($parent > 0) {
				echo 'success-parent';
				exit;
			} else {
				echo "fail";
			}
		} else {
			echo "fail";
		}

	} elseif (isset($_POST['user_type']) && $_POST['user_type'] == 'provider') {
		if ($_POST['newpassword'] == $_POST['confirmpassword']) {
			$provider = $model_provider->accountPassword_provider($_POST);
			if ($provider > 0) {
				echo 'success-provider';
				exit;
			} else {
				echo "fail";
			}
		} else {
			echo "fail";
		}
	} elseif (isset($_POST['user_type']) && $_POST['user_type'] == 'nanny') {
		if ($_POST['newpassword'] == $_POST['confirmpassword']) {
			$nanny = $model_nanny->accountPassword_nanny($_POST);
			if ($nanny > 0) {
				echo 'success-nanny';
				exit;
			} else {
				echo "fail";
			}
		} else {
			echo "fail";
		}
	} else {
		echo "fail";
	}
} else {
	echo "fail";
}

?>