<?php
ob_start();
require_once '../user-includes/config.inc.php';
require_once USER_MODEL_PATH . 'nanny-management.model.php';
$model_nanny = new ModelNannymanage();

if (isset($_POST["action"]) && $_POST["action"] == 'bookingreqaccept') {
	if (isset($_POST['book_nanny_id']) && $_POST['status'] == 'accept') {
		$book_nanny_id = $_POST['book_nanny_id'];
		$status = 'accept';
		$nanny = $model_nanny->bookingStatus_nanny($book_nanny_id, $status);
		if ($nanny > 0) {
			//send mail to parent
			echo 'success-nanny';
			exit;
		} else {
			echo "fail";
		}
	} elseif (isset($_POST['book_nanny_id']) && $_POST['status'] == 'decline') {
		$book_nanny_id = $_POST['book_nanny_id'];
		$status = 'decline';
		$nanny = $model_nanny->bookingStatus_nanny($book_nanny_id, $status);
		if ($nanny > 0) {
			//send mail to parent
			echo 'success-nanny';
			exit;
		} else {
			echo "fail";
		}
	} else {
		echo "fail";
	}
} else {
	echo "fail";
}

?>