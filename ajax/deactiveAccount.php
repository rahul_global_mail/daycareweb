<?php
ob_start();
require_once '../user-includes/config.inc.php';
require_once USER_MODEL_PATH . 'parent-management.model.php';
$model_user = new ModelParentmanage();
require_once USER_MODEL_PATH . 'provider-management.model.php';
$model_provider = new ModelProvidermanage();
require_once USER_MODEL_PATH . 'nanny-management.model.php';
$model_nanny = new ModelNannymanage();

if (isset($_POST["action"]) && $_POST["action"] == 'submituserdata') {
	if (isset($_POST['user_type']) && $_POST['user_type'] == 'parent') {
		$parent_id = $_POST['id'];
		$status = 'remove';
		$parent = $model_user->accountStatus_parent($parent_id, $status);
		if ($parent > 0) {
			unset($_SESSION['parentData']);
			session_destroy();
			echo 'success-parent';
			exit;
		} else {
			echo "fail";
		}
	} elseif (isset($_POST['user_type']) && $_POST['user_type'] == 'provider') {
		$provider_id = $_POST['id'];
		$status = 'remove';
		$provider = $model_provider->accountStatus_provider($provider_id, $status);
		if ($provider > 0) {
			unset($_SESSION['providerData']);
			session_destroy();
			echo 'success-provider';
			exit;
		} else {
			echo "fail";
		}
	} elseif (isset($_POST['user_type']) && $_POST['user_type'] == 'nanny') {
		$nanny_id = $_POST['id'];
		$status = 'remove';
		$nanny = $model_nanny->accountStatus_nanny($nanny_id, $status);
		if ($nanny > 0) {
			unset($_SESSION['nannyData']);
			session_destroy();
			echo 'success-nanny';
			exit;
		} else {
			echo "fail";
		}
	} else {
		echo "fail";
	}
} else {
	echo "fail";
}

?>