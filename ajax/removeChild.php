<?php
ob_start();
require_once '../user-includes/config.inc.php';
require_once USER_MODEL_PATH . 'parent-management.model.php';
$model_user = new ModelParentmanage();

if (isset($_POST["action"]) && $_POST["action"] == 'removeChild') {
	if (isset($_POST['child_id']) && !empty($_POST['child_id'])) {
		$child_id = $_POST['child_id'];
		$parent_id = $_SESSION['parentData'][0]->parent_id;
		$parentchild = $model_user->remove_childata($parent_id, $child_id);
		echo 'delete-child';
	}
} else {
	echo "fail";
}

?>