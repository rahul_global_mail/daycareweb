<?php
ob_start();
require_once '../user-includes/config.inc.php';
require_once USER_MODEL_PATH . 'provider-management.model.php';
$model_provider = new ModelProvidermanage();

if (isset($_POST["action"]) && $_POST["action"] == 'waitlistaccept') {
	if (isset($_POST['waitlist_id']) && $_POST['status'] == 'accept') {
		$waitlist_id = $_POST['waitlist_id'];
		$status = 'accept';
		$provider = $model_provider->waitlitStatus_provider($waitlist_id, $status);
		if ($provider > 0) {
			//send mail to parent
			echo 'success-provider';
			exit;
		} else {
			echo "fail";
		}
	} elseif (isset($_POST['waitlist_id']) && $_POST['status'] == 'decline') {
		$waitlist_id = $_POST['waitlist_id'];
		$status = 'decline';
		$provider = $model_provider->waitlitStatus_provider($waitlist_id, $status);
		if ($provider > 0) {
			//send mail to parent
			echo 'success-provider';
			exit;
		} else {
			echo "fail";
		}
	} else {
		echo "fail";
	}
} else {
	echo "fail";
}

?>