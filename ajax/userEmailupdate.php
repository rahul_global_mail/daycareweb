<?php
ob_start();
require_once '../user-includes/config.inc.php';
require_once USER_MODEL_PATH . 'parent-management.model.php';
$model_user = new ModelParentmanage();
require_once USER_MODEL_PATH . 'provider-management.model.php';
$model_provider = new ModelProvidermanage();

if (isset($_POST["action"]) && $_POST["action"] == 'emailchange') {
	if (isset($_POST['user_type']) && $_POST['user_type'] == 'parent') {
		$parent_id = $_POST['id'];
		$email = $_POST['newemail'];
		$parent = $model_user->accountEmail_parent($parent_id, $email);
		if ($parent > 0) {
			echo 'success-parent';
			exit;
		} else {
			echo "fail";
		}
	} elseif (isset($_POST['user_type']) && $_POST['user_type'] == 'provider') {
		$provider_id = $_POST['id'];
		$status = 'remove';
		$provider = $model_provider->accountStatus_provider($provider_id, $status);
		if ($provider > 0) {
			unset($_SESSION['providerData']);
			session_destroy();
			echo 'success-provider';
			exit;
		} else {
			echo "fail";
		}
	} elseif (isset($_POST['user_type']) && $_POST['user_type'] == 'nanny') {
		$nanny = $model_user->get_ParentLogin($_POST);
		if ($nanny[0]->parent_id != '' or $nanny[0]->parent_id != 0) {
			$_SESSION['nannyData'] = $nanny;
			echo 'success-nanny';
			exit;
		} else {
			echo "fail";
		}
	} else {
		echo "fail";
	}
} else {
	echo "fail";
}

?>