<?php
require_once '../user-includes/config.inc.php';
require_once '../stripe/init.php';
require_once USER_MODEL_PATH . 'provider-management.model.php';
require_once USER_MODEL_PATH . 'nanny-management.model.php';
require_once USER_MODEL_PATH . 'parent-management.model.php';

$stripe = new \Stripe\StripeClient(STRIPE_API_SECRET_KEY);
$result = [];

$expmonth = explode('/', $_POST['card_exp']);

try {
	$token = $stripe->tokens->create([
	  	'card' => [
		    'number' => $_POST['card_number'],
		    'exp_month' => $expmonth['0'],
		    'exp_year' => $expmonth['1'],
		    'cvc' => $_POST['card_cvc'],
		    'name' => $_POST['c_name'],
		    'address_zip' => $_POST['post_code'],
	  	],
	]);

	$result = [
		'success' => 1,
		'token' => $token->id
	];
}catch(Exception $e) {  
    $api_error = $e->getMessage();
    $result = [
		'success' => 0,
		'token' => $api_error
	];
}

if(isset($_POST['subscribe_plan']) && $_POST['subscribe_plan'] == 'subscribe_plan'){


	/* Provider Code Start */

	if(isset($_POST['provider_id']) && $_POST['provider_id'] != ''){
		$model_provider = new ModelProvidermanage();
		$providerData = $model_provider->get_providerDetail($_POST['provider_id'])[0];

		if(isset($_POST['plan'])){
			$price_package =  ($_POST['plan'] == 0) ? PRONAN_PRICE_MONTH_ID : PRONAN_PRICE_YEAR_ID;
			$subscription = 'paid';
			$plan_interval = ($_POST['plan'] == 0) ? 'MONTHLY' : 'YEARLY';
			$plan_amount = ($_POST['plan'] == 0) ? PRONAN_MONTH_PRICE : PRONAN_YEAR_PRICE;

		}else{
			$result = [
				'success' => 0,
				'token' => "Please select Plan"
			];
			echo json_encode($result);
			die();
		}

		try {
			$customer = $stripe->customers->create([
				'source'  => $token->id,
				'name' => $providerData->provider_name,
				'email' => $providerData->provider_email,
				'phone' => $providerData->phone,
			]);
		}catch(Exception $e) {
	        $api_error = $e->getMessage(); 
	        $result = [
				'success' => 0,
				'token' => $api_error
			];
			echo json_encode($result);
			die();
	    }

	    if(empty($api_error) && $customer){
	    	try { 
				$subscribe_data = $stripe->subscriptions->create([
				  	'customer' => $customer->id,
				  	'collection_method' => 'send_invoice',
				  	'days_until_due' => 30,
			  		'items' => [
				    	['price' => $price_package],
				  	],
				  	'metadata' => [
				  		'userid' => $_POST['provider_id']
				  	]
				]);
			}catch(Exception $e) {  
		        $api_error = $e->getMessage();  
		        $result = [
					'success' => 0,
					'token' => $api_error
				];
				echo json_encode($result);
				die();
		    }
	    	if($subscribe_data->status == 'active'){

	    		$timestamp = $subscribe_data->current_period_end;
				$next_payment_date = gmdate("Y-m-d H:i:s", $timestamp);

				$upd_payment = [
					'user_id'  => $_POST['provider_id'],
					'stripe_subscription_id' => $subscribe_data->id,
					'stripe_customer_id' => $customer->id,
					'plan_amount' => $plan_amount,
					'plan_amount_currency' => CURRENCY,
					'plan_interval' => $plan_interval,
					'plan_interval_count' => 1,
					'payer_email' => $providerData->provider_email,
					'created' => date('Y-m-d H:i:s'),
					'enddate' => $next_payment_date,
					'status' => $subscribe_data->status
				];

				$subscription_id = $model_provider->provider_payment_update($upd_payment);
				if($subscription_id){
					$result = [
						'success' => 1,
						'token' => "You have successfully subscribe to premium plan."
					];
					echo json_encode($result);
					die();
				}
			}
		}
	}

	/* Provider Code Start */

	/* Baby Sitter / Nany Code start here */

	if(isset($_POST['nanny_id']) && $_POST['nanny_id'] != ''){
		$model_nanny = new ModelNannymanage();
		$nannyData = $model_nanny->get_nannyDetail($_POST['nanny_id'])[0];

		if(isset($_POST['plan'])){
			$price_package =  ($_POST['plan'] == 0) ? PRONAN_PRICE_MONTH_ID : PRONAN_PRICE_YEAR_ID;
			$subscription = 'paid';
			$plan_interval = ($_POST['plan'] == 0) ? 'MONTHLY' : 'YEARLY';
			$plan_amount = ($_POST['plan'] == 0) ? PRONAN_MONTH_PRICE : PRONAN_YEAR_PRICE;
		}else{
			$result = [
				'success' => 0,
				'token' => "Please select Plan"
			];
			echo json_encode($result);
			die();
		}

		try {
			$customer = $stripe->customers->create([
				'source'  => $token->id,
				'name' => $nannyData->fname.' '.$nannyData->lname,
				'email' => $nannyData->email_id,
				'phone' => $nannyData->contact,
			]);
		}catch(Exception $e) {
	        $api_error = $e->getMessage(); 
	        $result = [
				'success' => 0,
				'token' => $api_error
			];
			echo json_encode($result);
			die();
	    }

	    if(empty($api_error) && $customer){
	    	try { 
				$subscribe_data = $stripe->subscriptions->create([
				  	'customer' => $customer->id,
				  	'collection_method' => 'send_invoice',
				  	'days_until_due' => 30,
			  		'items' => [
				    	['price' => $price_package],
				  	],
				  	'metadata' => [
				  		'userid' => $_POST['nanny_id']
				  	]
				]);
			}catch(Exception $e) {  
		        $api_error = $e->getMessage();  
		        $result = [
					'success' => 0,
					'token' => $api_error
				];
				echo json_encode($result);
				die();
		    }
	    	if($subscribe_data->status == 'active'){
	    		$timestamp = $subscribe_data->current_period_end;
				$next_payment_date = gmdate("Y-m-d H:i:s", $timestamp);

				$upd_payment = [
					'user_id'  => $_POST['nanny_id'],
					'stripe_subscription_id' => $subscribe_data->id,
					'stripe_customer_id' => $customer->id,
					'plan_amount' => $plan_amount,
					'plan_amount_currency' => CURRENCY,
					'plan_interval' => $plan_interval,
					'plan_interval_count' => 1,
					'payer_email' => $nannyData->email_id,
					'created' => date('Y-m-d H:i:s'),
					'enddate' => $next_payment_date,
					'status' => $subscribe_data->status
				];

				$subscription_id = $model_nanny->nanny_payment_update($upd_payment);
				if($subscription_id){
					$result = [
						'success' => 1,
						'token' => "You have successfully subscribe to premium plan."
					];
					echo json_encode($result);
					die();
				}
			}
		}
	}

	/* Baby Sitter / Nany Code start end */



	/* Parent Code Start */

	if(isset($_POST['parent_id']) && $_POST['parent_id'] != ''){
		$model_parent = new ModelParentmanage();
		$parentData = $model_parent->get_parentdetail($_POST['parent_id'])[0];

		if(isset($_POST['plan'])){
			$price_package =  ($_POST['plan'] == 0) ? PARENT_PRICE_MONTH_ID : PARENT_PRICE_YEAR_ID;
			$subscription = 'paid';
			$plan_interval = ($_POST['plan'] == 0) ? 'MONTHLY' : 'YEARLY';
			$plan_amount = ($_POST['plan'] == 0) ? PARENT_MONTH_PRICE : PRONAN_YEAR_PRICE;

		}else{
			$result = [
				'success' => 0,
				'token' => "Please select Plan"
			];
			echo json_encode($result);
			die();
		}

		try {
			$customer = $stripe->customers->create([
				'source'  => $token->id,
				'name' => $parentData->fname.' '.$parentData->lname,
				'email' => $parentData->parent_email,
			]);
		}catch(Exception $e) {
	        $api_error = $e->getMessage(); 
	        $result = [
				'success' => 0,
				'token' => $api_error
			];
			echo json_encode($result);
			die();
	    }

	    if(empty($api_error) && $customer){
	    	try { 
				$subscribe_data = $stripe->subscriptions->create([
				  	'customer' => $customer->id,
				  	'collection_method' => 'send_invoice',
				  	'days_until_due' => 30,
			  		'items' => [
				    	['price' => $price_package],
				  	],
				  	'metadata' => [
				  		'userid' => $_POST['parent_id']
				  	]
				]);
			}catch(Exception $e) {  
		        $api_error = $e->getMessage();  
		        $result = [
					'success' => 0,
					'token' => $api_error
				];
				echo json_encode($result);
				die();
		    }
	    	if($subscribe_data->status == 'active'){
	    		$timestamp = $subscribe_data->current_period_end;
				$next_payment_date = gmdate("Y-m-d H:i:s", $timestamp);

				$upd_payment = [
					'user_id'  => $_POST['parent_id'],
					'stripe_subscription_id' => $subscribe_data->id,
					'stripe_customer_id' => $customer->id,
					'plan_amount' => $plan_amount,
					'plan_amount_currency' => CURRENCY,
					'plan_interval' => $plan_interval,
					'plan_interval_count' => 1,
					'payer_email' => $parentData->parent_email,
					'created' => date('Y-m-d H:i:s'),
					'enddate' => $next_payment_date,
					'status' => $subscribe_data->status
				];

				$subscription_id = $model_parent->parent_payment_update($upd_payment);
				if($subscription_id){
					$result = [
						'success' => 1,
						'token' => "You have successfully subscribe to premium plan."
					];
					echo json_encode($result);
					die();
				}
			}
		}
	}

	/* Parent Code Start */
	
}
echo json_encode($result);
die();
?>