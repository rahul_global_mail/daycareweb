<?php
ob_start();
require_once '../user-includes/config.inc.php';
require_once USER_MODEL_PATH . 'provider-management.model.php';
$model_provider = new ModelProvidermanage();

if (isset($_POST["action"]) && $_POST["action"] == 'carefeesDelete') {
	if (isset($_POST['fees_id']) && !empty($_POST['fees_id'])) {
		$fees_id = $_POST['fees_id'];
		$parent = $model_provider->removedeleteCarefees($fees_id);
	}
} else {
	echo "fail";
}

?>