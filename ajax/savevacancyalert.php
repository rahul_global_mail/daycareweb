<?php
ob_start();
require_once '../user-includes/config.inc.php';
require_once USER_MODEL_PATH . 'parent-management.model.php';
$model_user = new ModelParentmanage();

if (isset($_POST["action"]) && $_POST["action"] == 'savevacancyalertdetail') {
	//check data
	$checkData = $model_user->vacancyalert_chkExist($_POST);
	if ($checkData) {
		echo "alert-exist";
	} else {
		$alertsave = $model_user->vacancyalert_save($_POST);
		if ($alertsave > 0) {
			echo 'success-alert';
			exit;
		} else {
			echo "error";
		}
	}

} else {
	echo "fail";
}

?>