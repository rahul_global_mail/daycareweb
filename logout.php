<?php
session_start();
unset($_SESSION['parentData']);
unset($_SESSION['providerData']);
unset($_SESSION['nannyData']);
session_destroy();
header("location: index.php");
?>