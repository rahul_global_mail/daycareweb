<?php
ob_start();
require_once 'user-includes/config.inc.php';
require_once 'stripe/init.php';
require_once USER_MODEL_PATH . 'parent-management.model.php';
$model_parent = new ModelParentmanage();
$redirect = 'parent-success.php';
$failredirect = 'addParent.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$_SESSION['add_msg'] = '';
	if (isset($_POST['submit'])) {

		$stripe = new \Stripe\StripeClient(STRIPE_API_SECRET_KEY);
		$checkData = $model_parent->parent_chkExist($_POST);
		if(isset($_POST['price_id'])){
			$price_package =  ($_POST['price_id'] == 0) ? PARENT_PRICE_MONTH_ID : PARENT_PRICE_YEAR_ID;
			//$price_package =  ($_POST['price_id'] == 0) ? PRICE_MONTH_ID : PRICE_YEAR_ID;
			$subscription = 'paid';
			$plan_interval = ($_POST['price_id'] == 0) ? 'MONTHLY' : 'YEARLY';
			$plan_amount = ($_POST['price_id'] == 0) ? PARENT_MONTH_PRICE : PRONAN_YEAR_PRICE;
		}else{
			$price_package = 0;
			$subscription = 'free';
			$plan_interval = 'FREE';
			$plan_amount = 0;
		}

		if ($checkData) {
			if($checkData[0]->customer_id != ''){
				$exist_customer =  $stripe->customers->retrieve(
				  $checkData[0]->customer_id
				);
				if($exist_customer){
					$exist_plan = $stripe->subscriptions->retrieve(
					  $checkData[0]->subscription_id
					);

					if($exist_plan->plan->id == PRONAN_PRICE_MONTH_ID || $exist_plan->plan->id == PRONAN_PRICE_YEAR_ID){
						header("Location:" . $redirect . "?action=exist&verifyID=" . base64_encode($checkData[0]->parent_id));
					}else{

						try {
							$subscribe_data = $stripe->subscriptions->create([
							  	'customer' => $customer->id,
							  	'enable_incomplete_payments'=>true,
						  		'items' => [
							    	['price' => $price_package],
							  	],
							  	'metadata' => [
							  		'userid' => $checkData[0]->parent_id
							  	]
							]);
						}catch(Exception $e) {  
					        $api_error = $e->getMessage();  
					    }

					    //$subsData = $subscription->jsonSerialize();
				    	if($subscribe_data->status == 'active'){

				    		$invoice = $stripe->invoices->retrieve(
							  	$subscribe_data->latest_invoice,
							  	[]
							);

			                $PaymentIntent =  $stripe->paymentIntents->retrieve(
							  $invoice->payment_intent,
							  []
							);

							$timestamp = $subscribe_data->current_period_end;
							$next_payment_date = gmdate("Y-m-d H:i:s", $timestamp);

							$upd_payment = [
								'user_id'  => $res,
								'stripe_subscription_id' => $subscribe_data->id,
								'stripe_customer_id' => $customer->id,
								'plan_amount' => $plan_amount,
								'plan_amount_currency' => CURRENCY,
								'plan_interval' => $plan_interval,
								'plan_interval_count' => 1,
								'payer_email' => $_POST['parent_email'],
								'created' => date('Y-m-d H:i:s'),
								'enddate' => $next_payment_date,
								'status' => $subscribe_data->status
							];

							$subscription_id = $model_parent->parent_payment_update($upd_payment);
						}
					}
				}else{
					try {
						$customer = $stripe->customers->create([
							'source'  => $_POST['token'],
							'name' => $_POST['fname'].' '.$_POST['lname'],
							'email' => $_POST['parent_email']
						]);
					}catch(Exception $e) {
				        $api_error = $e->getMessage();  
				    }

				    if(empty($api_error) && $customer){
				    	try { 
							$subscribe_data = $stripe->subscriptions->create([
							  	'customer' => $customer->id,
							  	'enable_incomplete_payments'=>true,
						  		'items' => [
							    	['price' => $price_package],
							  	],
							  	'metadata' => [
							  		'userid' => $checkData[0]->parent_id
							  	]
							]);
						}catch(Exception $e) {  
					        $api_error = $e->getMessage();  
					    }

					    //$subsData = $subscription->jsonSerialize();
				    	if($subscribe_data->status == 'active'){

				    		$invoice = $stripe->invoices->retrieve(
							  	$subscribe_data->latest_invoice,
							  	[]
							);

			                $PaymentIntent =  $stripe->paymentIntents->retrieve(
							  $invoice->payment_intent,
							  []
							);
							
							$timestamp = $subscribe_data->current_period_end;
							$next_payment_date = gmdate("Y-m-d H:i:s", $timestamp);

							$upd_payment = [
								'user_id'  => $res,
								'stripe_subscription_id' => $subscribe_data->id,
								'stripe_customer_id' => $customer->id,
								'plan_amount' => $plan_amount,
								'plan_amount_currency' => CURRENCY,
								'plan_interval' => $plan_interval,
								'plan_interval_count' => 1,
								'payer_email' => $_POST['parent_email'],
								'created' => date('Y-m-d H:i:s'),
								'enddate' => $next_payment_date,
								'status' => $subscribe_data->status
							];

							$subscription_id = $model_parent->parent_payment_update($upd_payment);
						}
					}
				}
			}else{
				try {
					$customer = $stripe->customers->create([
						'source'  => $_POST['token'],
						'name' => $_POST['fname'].' '.$_POST['lname'],
						'email' => $_POST['parent_email']
					]);
				}catch(Exception $e) {  
			        $api_error = $e->getMessage();  
			    }

			    if(empty($api_error) && $customer){
				    
				    $res = $model_parent->parent_save($_POST);
			    	
			    	try { 
						$subscribe_data = $stripe->subscriptions->create([
						  	'customer' => $customer->id,
						  	'enable_incomplete_payments'=>true,
					  		'items' => [
						    	['price' => $price_package],
						  	],
						  	'metadata' => [
						  		'userid' => $res
						  	]
						]);
					}catch(Exception $e) {  
				        $api_error = $e->getMessage();  
				    }


				    if($subscribe_data->status == 'active'){

				    	$invoice = $stripe->invoices->retrieve(
						  	$subscribe_data->latest_invoice,
						  	[]
						);

		                $PaymentIntent =  $stripe->paymentIntents->retrieve(
						  $invoice->payment_intent,
						  []
						);
						
						$timestamp = $subscribe_data->current_period_end;
						$next_payment_date = gmdate("Y-m-d H:i:s", $timestamp);

						$upd_payment = [
							'user_id'  => $res,
							'stripe_subscription_id' => $subscribe_data->id,
							'stripe_customer_id' => $customer->id,
							'plan_amount' => $plan_amount,
							'plan_amount_currency' => CURRENCY,
							'plan_interval' => $plan_interval,
							'plan_interval_count' => 1,
							'payer_email' => $_POST['parent_email'],
							'created' => date('Y-m-d H:i:s'),
							'enddate' => $next_payment_date,
							'status' => $subscribe_data->status
						];

						$subscription_id = $model_parent->parent_payment_update($upd_payment);
					}
				}
			}
			

			header("Location:" . $redirect . "?action=exist&verifyID=" . base64_encode($checkData[0]->parent_id));
		} else {
			
			try {
				$customer = $stripe->customers->create([
					'source'  => $_POST['token'],
					'name' => $_POST['fname'].' '.$_POST['lname'],
					'email' => $_POST['parent_email']
				]);
			}catch(Exception $e) {  
		        $api_error = $e->getMessage();  
		    }

		    try {
				$subscribe_data = $stripe->subscriptions->create([
				  	'customer' => $customer->id,
				  	'enable_incomplete_payments'=>true,
			  		'items' => [
				    	['price' => $price_package],
				  	],
				  	'metadata' => [
				  		'userid' => $res
				  	]
				]);
			}catch(Exception $e) {  
		        $api_error = $e->getMessage();  
		    }


		    //$res = $model_provider->provider_save($_POST);
		    $res = $model_parent->parent_save($_POST);

		    if($subscribe_data->status == 'active'){

		    	$invoice = $stripe->invoices->retrieve(
				  	$subscribe_data->latest_invoice,
				  	[]
				);

                $PaymentIntent =  $stripe->paymentIntents->retrieve(
				  $invoice->payment_intent,
				  []
				);

		    	$timestamp = $subscribe_data->current_period_end;
				$next_payment_date = gmdate("Y-m-d H:i:s", $timestamp);

				$upd_payment = [
					'user_id'  => $res,
					'stripe_subscription_id' => $subscribe_data->id,
					'stripe_customer_id' => $customer->id,
					'plan_amount' => $plan_amount,
					'plan_amount_currency' => CURRENCY,
					'plan_interval' => $plan_interval,
					'plan_interval_count' => 1,
					'payer_email' => $_POST['parent_email'],
					'created' => date('Y-m-d H:i:s'),
					'enddate' => $next_payment_date,
					'status' => $subscribe_data->status
				];

				$subscription_id = $model_parent->parent_payment_update($upd_payment);
			}

			if ($res > 0) {
				$_SESSION['parent_mail'] = $_POST['parent_email'];
				$_SESSION['add_msg'] = 'success';
			} else {
				$_SESSION['add_msg'] = 'error';
			}
			header("Location:" . $redirect . "?action=verify&verifyID=" . base64_encode($res));
		}
	}
}
// else
// {
// 	$company_data = $model_parent->getcompany($company_id);
// }
require_once USER_VIEW_PATH . 'add-parent.view.php';

?>