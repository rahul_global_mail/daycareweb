/*------------------------------------------------------------------
 * Project:        DayCare
 * URL:            -
 * Created:        30/04/2020
 -------------------------------------------------------------------
 */

/**
 TABLE OF CONTENT
 
 1. Loader
 2. Scroll Add Sticky
 3. Scroll Effect Top
 
 
 
 **/

$(document).ready(function () {
    /*======================================================================
     1. Loader
     ========================================================================*/
//    $.fn.exists = function () {
//        return this.length > 0
//    }, $(window).on("load", function () {
//        $(".loader-wrapper").delay(50).fadeOut("slow")
//    });

    /*======================================================================
     2. Scroll Add Sticky
     ========================================================================*/
    if ($(window).width() > 768) {
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();

            if (scroll >= 1) {
                $("body").addClass("sticky-header");
                $("header").addClass("is-sticky");
            } else {
                $("body").removeClass("sticky-header");
                $("header").removeClass("is-sticky");
            }
            if ($(this).scrollTop() > 400) {
                $('#scroll-up').fadeIn(300);
            } else {
                $('#scroll-up').fadeOut(300);
            }
        });
    }

    $(".el-heroslider-scrolldown").on("click", function (i) {
        i.preventDefault();
        var e = $(this);
        $("html, body").stop().animate({
            scrollTop: $(e.attr("href")).offset().top - 65
        }, 1e3)
    })

    /*======================================================================
     3. Scroll Effect Top
     ========================================================================*/
    $(window).scroll(function () {
        if ($(this).scrollTop() > 400) {
            $('#scroll-up').fadeIn(300);
        } else {
            $('#scroll-up').fadeOut(300);
        }
    });
    $('#scroll-up').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    /*======================================================================
     6. Master Slider
     ========================================================================*/
    var mainslider = new MasterSlider();
    mainslider.control('arrows', {autohide: true, overVideo: true});
    mainslider.control('bullets', {autohide: true, overVideo: true, dir: 'h', align: 'bottom', space: 6, margin: 10});
    mainslider.control('timebar', {autohide: false, overVideo: true, align: 'bottom', color: '#FFFFFF', width: 4});
    // slider setup
    mainslider.setup("mainslider", {
        width: 1140,
        height: 600,
        minHeight: 0,
        space: 0,
        start: 1,
        grabCursor: true,
        swipe: true,
        mouse: true,
        keyboard: false,
        layout: "fullwidth",
        wheel: false,
        autoplay: true,
        instantStartLayers: true,
        loop: true,
        shuffle: false,
        preload: 0,
        heightLimit: true,
        autoHeight: false,
        smoothHeight: true,
        endPause: false,
        overPause: false,
        fillMode: "fill",
        centerControls: false,
        startOnAppear: false,
        layersMode: "center",
        autofillTarget: "",
        hideLayers: false,
        fullscreenMargin: 0,
        speed: 20,
        dir: "h",
        parallaxMode: 'swipe',
        view: "basic"
    });

    $('[data-toggle="tooltip"]').tooltip()

    $('.select2-show-search').select2({
        minimumResultsForSearch: '',
        placeholder: "Search"
    });
    $('.multiselect-lang').select2();

    $('.summernote').summernote({
        placeholder: 'Edit Profile...',
        tabsize: 2,
        height: 200,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ]
    });

    $(".reviews-carousel.owl-carousel").owlCarousel({
        items: 1,
        loop: true,
        margin: 30,
        nav: false,
        dots: true,
        autoplay: true,
        autoplayTimeout: 8000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            450: {
                items: 1
            },
            750: {
                items: 1
            },
            900: {
                items: 2
            },
            1100: {
                items: 2
            }
        }
    });

    $(".testimonials-section .owl-carousel").owlCarousel({
        loop: true,
        margin: 30,
        nav: false,
        dots: true,
        autoplay: true,
        autoplayTimeout: 8000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            450: {
                items: 1
            },
            750: {
                items: 1
            },
            900: {
                items: 1
            },
            1100: {
                items: 1
            }
        }
    });
    $(".adv-banner .owl-carousel").owlCarousel({
        loop: true,
        margin: 30,
        nav: false,
        dots: false,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            450: {
                items: 1
            },
            750: {
                items: 1
            },
            900: {
                items: 1
            },
            1100: {
                items: 1
            }
        }
    });

    //--- Image Upload :: Start ---
    function readURL() {
        var $input = $(this);
        var $newinput = $(this).parent().parent().parent().find('.portimg');
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                reset($newinput.next('.delbtn'), true);
                $newinput.attr('src', e.target.result).show();
                $newinput.after('<span class="delbtn remove-btn"><i class="fas fa-times"></i></span>');
            }
            reader.readAsDataURL(this.files[0]);
        }
    }
    $(".fileUpload").change(readURL);
    $("form").on('click', '.delbtn', function (e) {
        reset($(this));
    });
    function reset(elm, prserveFileName) {
        if (elm && elm.length > 0) {
            var $input = elm;
            $input.prev('.portimg').attr('src', './assets/images/default-img.jpg').show();
            if (!prserveFileName) {
                $($input).parent().parent().parent().find('input.fileUpload ').val("");
            }
            elm.remove();
        }
    }
    //--- Image Upload :: End ---

    //--- Datatable ---
    $('.DataTables').dataTable({
        "ordering": true,
        columnDefs: [{
                orderable: false,
                targets: "no-sort"
            }]
    });
    $(".dataTables_wrapper .dataTables_filter label input").attr("placeholder", "Search here...");
    $('#table').DataTable({
        "ordering": true,
        dom: 'Bfrtip',
        buttons: [
            'csv', 'excel'
        ]
    });


    //--- selectpicker :: Start --
    $('select.selectinterested').selectpicker({});
    $('select.selectinterested').on('change', function () {
        var selectPicker = $(this);
        var selectAllOption = selectPicker.find('option.select-all');
        var checkedAll = selectAllOption.prop('selected');
        var optionValues = selectPicker.find('option[value!="all"][data-divider!="true"]');

        if (checkedAll) {
            // Process 'all/none' checking
            var allChecked = selectAllOption.data("all") || false;

            if (!allChecked) {
                optionValues.prop('selected', true).parent().selectpicker('refresh');
                selectAllOption.data("all", true);
            } else {
                optionValues.prop('selected', false).parent().selectpicker('refresh');
                selectAllOption.data("all", false);
            }

            selectAllOption.prop('selected', false).parent().selectpicker('refresh');
        } else {
            // Clicked another item, determine if all selected
            var allSelected = optionValues.filter(":selected").length == optionValues.length;
            selectAllOption.data("all", allSelected);
        }
    }).trigger('change');

    //--- Nav Smooth Scroll :: Start --
    $(window).on('scroll', function () {
        var scroll = $(window).scrollTop();
        if (scroll >= 200) {
            $(".wrapper").addClass("stickyadd")
        } else {
            $(".wrapper").removeClass("stickyadd")
        }
    });
    $('.maindetails-nav .nav-link').on('click', function (event) {
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top - 0
        }, 1500, 'easeInOutExpo');
        event.preventDefault()
    });
    if ($.fn.onePageNav) {
        $('.scroll-nav').onePageNav({
            currentClass: 'active',
            scrollSpeed: 1500,
            easing: 'easeOutQuad'
        });
    }
    //--- Nav Smooth Scroll :: End ---
    //
    //--- Leave Rating :: Start ---
    $('.leave-rating input').change(function () {
        var $radio = $(this);
        $('.leave-rating .selected').removeClass('selected');
        $radio.closest('label').addClass('selected');

    });
    //--- Leave Rating :: End ---
});

