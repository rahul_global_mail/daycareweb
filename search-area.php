<?php
ob_start();
require_once 'user-includes/config.inc.php';
require_once USER_MODEL_PATH . 'searchdata-management.model.php';
$model_serach = new ModelSearchdata();
$servicedata = $model_serach->search_get_careservices();
require_once USER_MODEL_PATH . 'parent-management.model.php';
$model_user = new ModelParentmanage();
$location = $model_user->get_allcity();
if (!empty($_GET['type']) || !empty($_GET['location']) || !empty($_GET['radius']) || !empty($_GET['service']) || !empty($_GET['lat']) || !empty($_GET['lng'])) {
	$type = $_GET['type'];
	$get_location = $_GET['location'];
	$radius = $_GET['radius'];
	$service = $_GET['service']; //multiple
	$ex_location = explode("-", $get_location);
	$locationdata = $ex_location[1];
	$is_vacancy = $_GET['is_vacancy'];

	if ($type == 'nanny' || $type == 'babysitter') {
		$nannydata = $model_serach->search_get_allnannyusers($type, $locationdata, $_GET);
	}else {
		$providerdata = $model_serach->search_get_allprovider($type, $locationdata, $service, $radius, $is_vacancy, $_GET);
	}
} else {
	$providerdata = $model_serach->search_get_allprovider($type = "careprovider", $locationdata = "Vancouver", $service = "", $radius = "", $is_vacancy = "");
}

require_once USER_VIEW_PATH . 'search-data.view.php';
?>