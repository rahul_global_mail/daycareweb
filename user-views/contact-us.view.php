<?php
$pageName = 'contact';
require_once USER_VIEW_PATH . 'header.inc.php';?>
<!-- Inner Banner :: Start -->
<section class="inner-banner">
    <div class="inner-content text-center">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <h1 class="page-title">Contact Us</h1>
                </div>
                <div class="col-12 col-sm-12 col-md-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li class="active">Contact Us</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inner Banner :: End -->
<!-- Inner Body :: Start -->
<section class="innerbody-section pt-50 pb-50">
    <div class="container">
        <div class="row mb-50">
            <!-- <div class="col-md-4">
                <div class="contact-box">
                    <div class="contact-icon"><i class="fas fa-phone-alt"></i></div>
                    <div class="contact-details">
                        <div class="contact-header">Phones</div>
                        <div class="contact-cont">
                            <a href="tel:+00123456789">(+00) 123-456-789</a>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="col-md-6">
                <div class="contact-box">
                    <div class="contact-icon"><i class="fas fa-envelope"></i></div>
                    <div class="contact-details">
                        <div class="contact-header">Email</div>
                        <div class="contact-cont">
                            <a href="mailto:demo@example.com">demo@example.com</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="contact-box">
                    <div class="contact-icon"><i class="fas fa-map-marker-alt"></i></div>
                    <div class="contact-details">
                        <div class="contact-header">Address</div>
                        <div class="contact-cont">
                            <span>555 California str, Suite 100 San Francisco, CA 94107</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <form class="contact-form">
                    <div class="form-group">
                        <label>Enquiry Type<span class="required">*</span></label>
                        <div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="GeneralEnquiryRadio" name="EnquiryRadio" class="custom-control-input">
                                <label class="custom-control-label" for="GeneralEnquiryRadio">General Enquiry</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="TechnicalEnquiryRadio" name="EnquiryRadio" class="custom-control-input">
                                <label class="custom-control-label" for="TechnicalEnquiryRadio">Technical Enquiry</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="RegistrationEnquiryRadio" name="EnquiryRadio" class="custom-control-input">
                                <label class="custom-control-label" for="RegistrationEnquiryRadio">Registration Enquiry</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Full Name<span class="required">*</span></label>
                        <input type="text" class="form-control" name="fullname" placeholder="Full Name" required="">
                    </div>
                    <div class="form-group">
                        <label>Email<span class="required">*</span></label>
                        <input type="email" class="form-control" name="email" placeholder="Email Address" required="">
                    </div>
                    <div class="form-group">
                        <label>Phone Number<span class="required">*</span></label>
                        <input type="text" class="form-control" name="phonenumber" placeholder="Phone Number" required="">
                    </div>
                    <div class="form-group">
                        <label>Comment<span class="required">*</span></label>
                        <textarea class="form-control" rows="5" placeholder="Comment"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Send <i class="far fa-paper-plane pl-1"></i></button>
                </form>
            </div>
            <div class="col-md-6">
                <div class="contactmap-area">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d395.58677870284004!2d-113.48621743280164!3d53.542738028782715!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sbabycare%20canada!5e0!3m2!1sen!2sin!4v1590327230459!5m2!1sen!2sin"></iframe>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inner Body :: End -->
<?php require_once USER_VIEW_PATH . 'footer.inc.php';?>
<?php require_once USER_VIEW_PATH . 'frontouter.inc.php';?>