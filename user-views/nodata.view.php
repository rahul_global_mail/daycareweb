<?php
require_once USER_VIEW_PATH . 'header.inc.php';?>
<!--- Main Content :: Start --->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                   <center><h1 class="section-t1">Opps! Nothing to see here.</h1></center>
            </div>
        </div>
    </div>
</div>
<!--- Main Content :: End --->
<?php require_once USER_VIEW_PATH . 'footer.inc.php';?>