<?php
$pageName = 'home';
require_once USER_VIEW_PATH . 'header.inc.php';?>
<!-- Inner Banner :: Start -->
<section class="banners-section">
    <div id="masterslider1" class="master-slider ms-skin-default" >
        <div class="ms-slide" data-delay="5" data-fill-mode="fill" >
            <img src="assets/images/blank.gif" alt="" title="bride-bg-slide1" data-src="assets/images/banner/slider-1.jpg" />
        </div>
    </div>
    <div class="innerbanner-section">
        <div class="innerbanner-contextual-header">
            <div class="container">
                <div class="row">
                    <div class="col-xl-2 col-lg-2 col-md-3">
                        <div class="innerbanner-image">
                            <img class="img-fluid" src="assets/images/favicon.png" alt="Profile Image"/>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-7 col-md-9">
                        <div class="innerbanner-personaldetails">
                            <h1 class="innerbanner-title"><?php echo ucwords($providerData[0]->provider_name); ?></h1>
                            <div class="childcare-address"><i class="fas fa-map-marker-alt"></i><?php echo $providerData[0]->address . " , " . $providerData[0]->suburb . " , " . $providerData[0]->postcode . " - " . $providerData[0]->country; ?></div>
                            <div class="childcare-address"><i class="fas fa-user"></i>Contact name : <b><?php echo ucwords($providerData[0]->contact_name); ?> </b></div>
                            <div class="childcare-address"><i class="fas fa-envelope"></i>Email : <b><?php echo "Unavailable"; //$providerData[0]->provider_email;   ?></b></div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-9 offset-lg-0 offset-md-3">
                        <div class="userprofile-rightarea">
                        <div class="innerbanner-gettouch mt-2">
                            <ul>
                                <li>
                                    <a href="#" class="btn btn-gettouch"><i class="far fa-envelope"></i></a>
                                </li>
                                <li>
                                    <a href="#" class="btn btn-gettouch"><i class="far fa-calendar-alt"></i></a>
                                </li>
                                <li>
                                    <a href="#" class="btn btn-gettouch"><i class="fas fa-globe"></i></a>
                                </li>
                                <li>
                                    <a href="#" class="btn btn-gettouch"><i class="fas fa-certificate"></i></a>
                                </li>
                                <li>
                                    <a href="#" class="btn btn-gettouch"><i class="fab fa-facebook-f"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inner Banner :: End -->

<section id="info-scroll" class="pt-50 pb-50">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h4 class="info-title">About <?php echo ucwords($providerData[0]->provider_name); ?></h4>
                <div class="info-desc">
                    <?php echo $providerData[0]->about; ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sidebar-info">
                    <div class="sidebar-box">
                        <div class="sidebar-title">Services</div>
                        <div class="sidebar-body">
                            <ul class="services-list">
                                <?php
									$explode = explode(",", $providerData[0]->provider_service_id);
									foreach ($explode as $value) {
										$service_data = $model_provider->getsearch_get_careservices($value);
								?>
                                <li><?php echo $service_data[0]->title; ?></li>
								<?php } ?>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<section id="reviews-scroll" class="pt-50 pb-50 bg-grey">
    <div class="container">
        <div class="row">
            <div class="col-7 col-md-6 pr-0">
                <h4 class="info-title mb-20">Reviews <span class="count-review"></span></h4>
            </div>
            <div class="col-5 col-md-6 text-right pt-2 pl-0">
                <a href="javascript:void(0)" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#PostReviewModal">Post A Review</a>
            </div>
        </div>
        <!-- <div class="row mb-20">
            <div class="col-md-12">
                <div class="review-box">
                    <div class="review-header clearfix">
                        <div class="review-by">
                            <div class="review-author">Malou Galvez</div>
                            <div class="review-date">Aug 13, 2020 at 12:26 pm</div>
                        </div>
                        <div class="review-author-rating">
                            <div class="review-rating">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                            </div>
                        </div>
                    </div>
                    <div class="review-body">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="review-box">
                    <div class="review-header clearfix">
                        <div class="review-by">
                            <div class="review-author">Malou Galvez</div>
                            <div class="review-date">Aug 13, 2020 at 12:26 pm</div>
                        </div>
                        <div class="review-author-rating">
                            <div class="review-rating">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                            </div>
                        </div>
                    </div>
                    <div class="review-body">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="review-box">
                    <div class="review-header clearfix">
                        <div class="review-by">
                            <div class="review-author">Malou Galvez</div>
                            <div class="review-date">Aug 13, 2020 at 12:26 pm</div>
                        </div>
                        <div class="review-author-rating">
                            <div class="review-rating">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                            </div>
                        </div>
                    </div>
                    <div class="review-body">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="row">
            <div class="col-md-12">
                <p>We don't have any reviews just yet.</p>
                <a href="javascript:void(0)" class="btn btn-primary f-18" data-toggle="modal" data-target="#PostReviewModal">Post A Review</a>
            </div>
        </div>
    </div>
</section>
<!--- Inner Body :: End --->
<!-- Reviews Modal :: Start -->
<div class="modal custom-modal fade " id="PostReviewModal" tabindex="-1" role="dialog" aria-labelledby="PostReviewModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered md-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="PostReviewModal">Post A Review</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
            </div>
            <form>
                <div class="modal-body p-4">
                    <div class="form-group">
                        <label>Full Name<span class="required">*</span></label>
                        <input type="text" class="form-control" name="reviewfullname" placeholder="Full Name" required="">
                    </div>
                    <div class="form-group">
                        <label>Email Address<span class="required">*</span></label>
                        <input type="email" class="form-control" name="reviewemail" placeholder="Email Address" required="">
                    </div>
                    <div class="form-group">
                        <label>Post a Review<span class="required">*</span></label>
                        <textarea class="form-control" rows="5" placeholder="Post a Review"></textarea>
                    </div>
                    <div class="clearfix mb-3">
                        <div class="checkboxes">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="rememberme">
                                <label class="custom-control-label" for="rememberme">I here by confirm</label>
                            </div>
                        </div>
                    </div>
                    <ul class="list-disc pl-4">
                        <li>I have a child currently attending this service, or a child who has attended sometime in the last 12 months</li>
                        <li>Have not submitted a review previously</li>
                        <li>Read and agree to our Terms & Conditions and Privacy Policy.</li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Post Review</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Reviews Modal :: End -->

<?php require_once USER_VIEW_PATH . 'footer.inc.php';?>
<script>
    var masterslider1 = new MasterSlider();
    masterslider1.control('timebar', {autohide: false, overVideo: true, align: 'top', color: '#FFFFFF', width: 4});
    masterslider1.setup("masterslider1", {
        width: 1366,
        height: 600,
        minHeight: 400,
        space: 0,
        start: 1,
        grabCursor: true,
        swipe: false,
        mouse: true,
        keyboard: false,
        layout: "",
        wheel: false,
        autoplay: true,
        instantStartLayers: true,
        loop: true,
        shuffle: false,
        preload: 0,
        heightLimit: true,
        autoHeight: false,
        smoothHeight: true,
        endPause: false,
        overPause: false,
        fillMode: "fill",
        centerControls: false,
        startOnAppear: false,
        layersMode: "center",
        autofillTarget: "",
        hideLayers: false,
        fullscreenMargin: 80,
        speed: 20,
        dir: "h",
        parallaxMode: 'swipe',
        view: "parallaxMask"
    });
</script>
<?php require_once USER_VIEW_PATH . 'frontouter.inc.php';?>