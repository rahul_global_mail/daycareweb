<!-- Footer :: Start -->
<footer>
    <div class="outer-footer clearfix">
        <div class="container">
            <div class="copyright-area">
                <p>Copyright &copy; <?php echo date('Y'); ?>. All Rights Reserved By <a href="#">daycareseekers.com</a></p>
            </div>
            <div class="social-foot">
                <ul class="socialfoot-list">
                    <li>
                        <a href="#" class="social-link" target="_blank"><i class="fab fa-facebook-f"></i></a>
                    </li>
                    <li>
                        <a href="#" class="social-link" target="_blank"><i class="fab fa-google"></i></a>
                    </li>
                    <li>
                        <a href="#" class="social-link" target="_blank"><i class="fab fa-twitter"></i></a>
                    </li>
                    <li>
                        <a href="#" class="social-link" target="_blank"><i class="fab fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- Footer :: End -->
</div>
<!-- Scroll Top :: Start -->
<div id="scroll-up" class="footer-scroll" style=""><i class="fas fa-arrow-up"></i></div>
<!-- Scroll Top :: End -->

<!-- Login Modal :: Start -->
<div class="modal custom-modal fade " id="loginModal" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered sm-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="loginModalLabel">Welcome Back</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
            </div>
            <div class="modal-body p-4">
                <form id="userlogin" method="post">
                    <div class="form-group">
                        <label>I am ..</label>
                        <div class="input-group rm-bg mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="ti-user"></i></div>
                            </div>
                            <select class="form-control" name="user_type" required="">
                                <option value="">---</option>
                                <option value="parent">Parent</option>
                                <option value="care_provider">Care Provider</option>
                                <option value="nanny">Nanny / Baby Sitter</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <div class="input-group rm-bg mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="ti-email"></i></div>
                            </div>
                            <input type="email" class="form-control" name="email" placeholder="Email Address" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <div class="input-group rm-bg mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="ti-lock"></i></div>
                            </div>
                            <input type="password" class="form-control" name="password" placeholder="Password" required="">
                        </div>
                    </div>
                    <div class="clearfix mb-3">
                        <div class="checkboxes float-left">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="rememberme">
                                <label class="custom-control-label" for="rememberme">Remember me</label>
                            </div>
                        </div>
                        <div class="float-right"><a href="javascript:void(0)" class="leftlink" data-toggle="modal" data-target="#ForgotPasswordModal" data-dismiss="modal">Forgot Password?</a></div>
                    </div>
                    <input type="hidden" name="action" value="submituserdata">
                    <button class="btn btn-primary btn-block" type="submit">Login</button>
                    <div class="text-center mt-2">
                        Don’t have an account? <a href="sign-up.php" class="link">Sign up</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Login Modal :: End -->

<!-- Forgot Password Modal :: Start -->
<div class="modal custom-modal fade " id="ForgotPasswordModal" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="ForgotPasswordModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered sm-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ForgotPasswordModal">Forgot Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
            </div>
            <div class="modal-body p-4">
                <form>
                    <div class="form-group">
                        <label>Email</label>
                        <div class="input-group rm-bg mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="ti-email"></i></div>
                            </div>
                            <input type="email" class="form-control" name="email" placeholder="Email Address" required="">
                        </div>
                    </div>
                    <a href="account-Parent.php" class="btn btn-primary btn-block">Login </a>
                    <div class="text-center mt-2">
                        Back to <a href="javascript:void(0)" class="link" data-toggle="modal" data-target="#loginModal" data-dismiss="modal">Sign in</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Forgot Password Modal :: End -->

<!--- JavaScript :: Start --->
<script src="<?php echo HOME_URL; ?>assets/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo HOME_URL; ?>assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo HOME_URL; ?>assets/js/popper.min.js" type="text/javascript"></script>
<script src="<?php echo HOME_URL; ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo HOME_URL; ?>assets/js/moment.min.js" type="text/javascript"></script>
<script src="<?php echo HOME_URL; ?>assets/js/jquery.easing.min.js" type="text/javascript"></script>
<script src="<?php echo HOME_URL; ?>assets/js/masterslider.min.js" type="text/javascript"></script>
<script src="<?php echo HOME_URL; ?>assets/js/owl.carousel.min.js" type="text/javascript"></script>
<script src="<?php echo HOME_URL; ?>assets/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo HOME_URL; ?>assets/js/summernote.min.js" type="text/javascript"></script>
<script src="<?php echo HOME_URL; ?>assets/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo HOME_URL; ?>assets/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo HOME_URL; ?>assets/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<script src="<?php echo HOME_URL; ?>assets/js/bootstrap-datetimepicker-v4.min.js" type="text/javascript"></script>
<script src="<?php echo HOME_URL; ?>assets/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
<script src="<?php echo HOME_URL; ?>assets/js/bootstrap-select.min.js" type="text/javascript"></script>
<script src="<?php echo HOME_URL; ?>assets/js/tag-it.min.js" type="text/javascript"></script>
<script src="<?php echo HOME_URL; ?>assets/js/script.js" type="text/javascript"></script>
<script src="<?php echo HOME_URL; ?>DayCare-AdminPanel/assets/js/sweetalert2.min.js" type="text/javascript"></script>
<!--- JavaScript :: End --->
<script>
$(document).on('submit', '#userlogin', function(event){
event.preventDefault();
$.ajax({
    url:"ajax/loginUser.php",
    method:'POST',
    data:new FormData(this),
    contentType:false,
    processData:false,
    success:function(data)
    {
      $('#userlogin')[0].reset();
      $('#loginModal').modal('hide');
      if(data == 'success-parent'){
        window.location.replace("account-Parent.php");
      }else if(data == 'success-nanny'){
        window.location.replace("account-Nanny.php");
      }else if(data == 'success-provider'){
        window.location.replace("account-Provider.php");
      }else{
        Swal.fire(
          'Failed!','Somthing goes wrong.','error'
        ).then((result) => {
          // Reload the Page
          //window.location.replace("<?php echo $_SERVER['PHP_SELF']; ?>");
        });
      }

    }
});
});
</script>