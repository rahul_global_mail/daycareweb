<?php
$pageName = 'search';
require_once USER_VIEW_PATH . 'header.inc.php';?>
<!-- Inner Banner :: Start -->
<section class="inner-banner">
    <div class="inner-content text-center">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <h1 class="page-title">Search</h1>
                </div>
                <div class="col-12 col-sm-12 col-md-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li class="active">Search</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inner Banner :: End -->
<!-- Inner Body :: Start -->
<section class="innerbody-section pt-50 pb-50">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="filter mb-4">
                    <p>
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="ti-filter pr-1"></i> Filter</button>
                    </p>
                    <div class="collapse" id="collapseExample">
                        <div class="card card-body">
                            <form method="get">
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <label>Care Type</label>
                                        <select class="form-control" name="type">
                                            <option value="All">All Types</option>
                                            <option value="careprovider">Childcare Centre</option>
                                            <option value="2">3 Years to 5 Years Group Childcare Centre</option>
                                            <option value="3">Multi-age Childcare Centre</option>
                                            <option value="4">Family Day Care</option>
                                            <option value="5">Kindergartens</option>
                                            <option value="7">Before & After School Care</option>
                                            <option value="8">Occasional, Casual & Drop-in Centres</option>
                                            <option value="5">Pre-School</option>
                                            <option value="nanny">Nanny</option>
                                            <option value="babysitter">Babysitters</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label>Radius</label>
                                        <select class="form-control" name="radius">
                                            <option value="">-- Select Radius --</option>
                                            <option value="0-10">0km - 10km</option>
                                            <option value="11-20">11km - 20km</option>
                                            <option value="21-30">21km - 30km</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label>Location</label>
                                        <input id="search-box" name="location" type="text" class="form-control" placeholder="Location">
                                        <div id="suggesstion-box"></div>
                                    </div>
                                </div>
                                <div class="col-md-12 form-group">
                                        <label>Services</label>
                                            <div class="row">
                                                <?php
foreach ($servicedata as $service) {?>


                                                <div class="custom-control custom-checkbox col-md-6">
                                                    <input type="checkbox" name="service[]" value="<?php echo $service->provider_service_id; ?>" class="custom-control-input" id="customCheck<?php echo $service->provider_service_id; ?>">
                                                    <label class="custom-control-label" for="customCheck<?php echo $service->provider_service_id; ?>"><?php echo $service->title; ?></label>
                                                </div>
                                                <?php }?>

                                            </div>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Vacancy Availablity : </label>
                                        <div>
                                            <div class="row">
                                                <div class="custom-control custom-checkbox col-md-6">
                                                    <input type="radio" id="vac_Yes" name="is_vacancy" value="Yes">
                                                    <label for="vac_Yes">Yes</label>
                                                    <input type="radio" id="vac_No" name="is_vacancy" value="No">
                                                    <label for="vac_No">No</label><br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="lat" id="mylat" value="49.2768">
                                    <input type="hidden" name="lng" id="mylng" value="-123.0391">
                                <button class="btn btn-info" type="submit"><i class="ti-filter pr-1"></i> Search</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
          <?php
if (!empty($_GET['type']) || !empty($_GET['location']) || !empty($_GET['radius'])) {
	// nanny | babysitter
	if ($_GET['type'] == 'nanny' || $_GET['type'] == 'babysitter') {
		if ($nannydata) {
			foreach ($nannydata as $nanny) {
				?>
                <div class="col-lg-6 col-md-12">
                <div class="childcare-box">
                    <div class="childcare-info clearfix">
                        <div class="childcare-img">
                            <img class="img-fluid" src="upload_images/<?php echo $nanny->photo; ?>" alt="Childcare"/>
                            <a href="profileNanny.php?nanID=<?php echo base64_encode($nanny->nanny_id); ?>" class="btn btn-primary btn-view">View Profile</a>
                        </div>
                        <div class="childcare-details">
                            <h2 class="childcare-title">
                                <a href="profileNanny.php?nanID=<?php echo base64_encode($nanny->nanny_id); ?>"><?php echo $nanny->fullname; ?></a>
                            </h2>
                            <div class="childcare-rating">
                                <i class="far fa-star"></i>
                                <i class="far fa-star"></i>
                                <i class="far fa-star"></i>
                                <i class="far fa-star"></i>
                                <i class="far fa-star"></i>
                                <span class="givenrating">No Ratings</span>
                            </div>
                            <div class="childcare-address"><i class="fas fa-map-marker-alt"></i><?php echo $nanny->suburb . " , " . $nanny->city . ", " . $nanny->postcode . " - " . $nanny->country; ?></div>
                            <div class="childcare-phone">
                                <a class="childcare-phonelink" href="tel:<?php echo $nanny->contact; ?>"><i class="fas fa-phone-alt"></i><?php echo $nanny->contact; ?></a>
                            </div>
                            <div class="childcare-address"><i class="fas fa-clock"></i> <b>Availablity : </b><?php echo $nanny->availablity; ?>
                          </div>
                        </div>
                    </div>
                    <div class="childcare-foot clearfix">
                        <div class="row">
                            <div class="col">
                                <ul class="childcare-footlinks">
                                    <li>
                                        <a href="#" class="childcare-sortlink" data-toggle="tooltip" data-placement="bottom" title="Show on map"><i class="fas fa-map-marker-alt"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" class="childcare-sortlink" data-toggle="tooltip" data-placement="bottom" title="<?php echo $nanny->salary . " / " . $nanny->salary_type; ?>"><i class="fas fa-dollar-sign"></i></a>
                                    </li>
                                    <li>
                                        <a href="profileNanny.php?nanID=<?php echo base64_encode($nanny->nanny_id); ?>" class="childcare-sortlink" data-toggle="tooltip" data-placement="bottom" title="Contact service"><i class="fas fa-envelope"></i></a>
                                    </li>
                                    <li>
                                        <a href="profileNanny.php?nanID=<?php echo base64_encode($nanny->nanny_id); ?>" class="childcare-sortlink" data-toggle="tooltip" data-placement="bottom" title="Confort! Book Now"><i class="fas fa-bell"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" class="childcare-sortlink" data-toggle="tooltip" data-placement="bottom" title="<?php echo $nanny->availablity; ?>"><i class="fas fa-calendar-alt"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col">
                                <select class="form-control">
                                    <option value="">More Options</option>
                                    <?php
$explode = explode(",", $nanny->services);
				foreach ($explode as $value) {
					$servicedata = $model_serach->get_service($value);
					?>
                                      <option value="option1"><?php echo $servicedata[0]->service_name; ?></option>
                                    <?php }?>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
}} else {?>
			<div class="card-body">
                   <center><h3 class="section-t1">Opps! No data found.</h3></center>
            </div>
		<?php }
	} else {
		if ($providerdata) {
			foreach ($providerdata as $providerVal) {
				?>

              <div class="col-lg-6 col-md-12">
                <div class="childcare-box">
                    <div class="childcare-info clearfix">
                        <div class="childcare-img">
                            <img class="img-fluid" src="upload_images/<?php echo $providerVal->photo; ?>" alt="Childcare"/>
                            <a href="provider-details.php?careID=<?php echo base64_encode($providerVal->provider_id); ?>" class="btn btn-primary btn-view">View Profile</a>
                        </div>
                        <div class="childcare-details">
                            <h2 class="childcare-title">
                                <a href="provider-details.php?careID=<?php echo base64_encode($providerVal->provider_id); ?>"><?php echo ucwords($providerVal->provider_name); ?></a>
                            </h2>
                            <div class="childcare-rating">
                                <i class="far fa-star"></i>
                                <i class="far fa-star"></i>
                                <i class="far fa-star"></i>
                                <i class="far fa-star"></i>
                                <i class="far fa-star"></i>
                                <span class="givenrating">No Ratings</span>
                            </div>
                            <div class="childcare-address"><i class="fas fa-map-marker-alt"></i><?php echo $providerVal->suburb . " , " . $providerVal->postcode . " , " . $providerVal->city . " - " . $providerVal->country; ?></div>
                            <?php
                            if($providerVal->subscription == 'free'){}else{?>
                            <div class="childcare-phone">
                                <a class="childcare-phonelink" href="tel:<?php echo $providerVal->phone; ?>"><i class="fas fa-phone-alt"></i><?php echo $providerVal->phone; ?></a>
                            </div><?php } ?>
                        </div>
                    </div>
                    <div class="childcare-foot clearfix">
                        <div class="row">
                            <div class="col">
                                <ul class="childcare-footlinks">
                                    <li>
                                        <a href="provider-details.php?careID=<?php echo base64_encode($providerVal->provider_id); ?>" class="childcare-sortlink" data-toggle="tooltip" data-placement="bottom" title="Show on map"><i class="fas fa-map-marker-alt"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" class="childcare-sortlink" data-toggle="tooltip" data-placement="bottom" title="Show vacancies and fees"><i class="fas fa-dollar-sign"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" class="childcare-sortlink" data-toggle="tooltip" data-placement="bottom" title="Contact service"><i class="fas fa-envelope"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" class="childcare-sortlink" data-toggle="tooltip" data-placement="bottom" title="Set up Spot alert"><i class="fas fa-bell"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" class="childcare-sortlink" data-toggle="tooltip" data-placement="bottom" title="Waitlist"><i class="fas fa-calendar-alt"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col">
                                <select class="form-control">
                                    <option value="">More Options</option>
                                    <?php
$explode = explode(",", $providerVal->provider_service_id);
				foreach ($explode as $value) {
					$service_data = $model_serach->getsearch_get_careservices($value);
					?>
                                      <option value="option1"><?php echo $service_data[0]->title; ?></option>
                                    <?php }?>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php	}} else {?>

            <div class="card-body">
                   <center><h3 class="section-t1">Opps! No data found.</h3></center>
            </div>

			<?php
}
	}} else {?>

<div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                   <center><h1 class="section-t1">Opps! Input data not match.</h1>
                    <p>
                      Please search somthing related & nearest to view results.
                    </p>
                    <h4>Thanks</h4>
                  </center>
            </div>
        </div>
    </div>
            <?php }
?>


            <!-- <div class="col-lg-6 col-md-12">
                <div class="childcare-box">
                    <div class="childcare-info clearfix">
                        <div class="childcare-img">
                            <img class="img-fluid" src="assets/images/childcare-1.jpg" alt="Childcare"/>
                            <a href="provider-details.php" class="btn btn-primary btn-view">View Profile</a>
                        </div>
                        <div class="childcare-details">
                            <h2 class="childcare-title">
                                <a href="provider-details.php">Kids Club Clarence St Early Childhood Learning Centre</a>
                            </h2>
                            <div class="childcare-rating">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                                <span class="givenrating">5 Ratings</span>
                            </div>
                            <div class="childcare-address"><i class="fas fa-map-marker-alt"></i>Ground Floor, 83 Clarence Street Sydney, NSW 2000</div>
                            <div class="childcare-phone">
                                <a class="childcare-phonelink" href="tel:9090 909 909"><i class="fas fa-phone-alt"></i>9090 909 909</a>
                            </div>
                        </div>
                    </div>
                    <div class="childcare-foot clearfix">
                        <div class="row">
                            <div class="col">
                                <ul class="childcare-footlinks">
                                    <li>
                                        <a href="#" class="childcare-sortlink" data-toggle="tooltip" data-placement="bottom" title="Show on map"><i class="fas fa-map-marker-alt"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" class="childcare-sortlink" data-toggle="tooltip" data-placement="bottom" title="Show vacancies and fees"><i class="fas fa-dollar-sign"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" class="childcare-sortlink" data-toggle="tooltip" data-placement="bottom" title="Contact service"><i class="fas fa-envelope"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" class="childcare-sortlink" data-toggle="tooltip" data-placement="bottom" title="Set up vacancy alert"><i class="fas fa-bell"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" class="childcare-sortlink" data-toggle="tooltip" data-placement="bottom" title="Waitlist"><i class="fas fa-calendar-alt"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col">
                                <select class="form-control">
                                    <option value="more">More Options</option>
                                    <option value="option1">Options 1</option>
                                    <option value="option2">Options 2</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->

        </div>
    </div>
</section>
<!-- Inner Body :: End -->
<?php require_once USER_VIEW_PATH . 'footer.inc.php';?>
<script>
// AJAX call for autocomplete
$(document).ready(function(){
    $("#search-box").keyup(function(){
        $.ajax({
        type: "POST",
        url: "ajax/readLocation.php",
        data:'keyword='+$(this).val(),
        beforeSend: function(){
            $("#search-box").css("background","#FFF url(assets/images/LoaderIcon.gif) no-repeat 165px");
        },
        success: function(data){
            $("#suggesstion-box").show();
            $("#suggesstion-box").html(data);
            $("#search-box").css("background","#FFF");
        }
        });
    });
});
//To select country name
function selectCountry(val) {
    $("#search-box").val(val);
    $("#suggesstion-box").hide();
}
</script>

<script type="text/javascript">
	function getLocation() {
	  	if (navigator.geolocation) {
	    	navigator.geolocation.getCurrentPosition(showPosition);
	  	} else { 
	    	alert("Geolocation is not supported by this browser.");
	  	}
	}

	function showPosition(position) {
		$("#mylat").val(position.coords.latitude);
		$("#mylng").val(position.coords.longitude);
		/*fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.coords.latitude},${position.coords.longitude}&key=AIzaSyAZPcppwWN179bx8Asxh8Xsd-ZiLIvZNMM`)
	    .then(response => response.json())
	    .then(result => {
	    	getTypeadd(result);
	    });*/

	}
	window.addEventListener('load', (event) => {
	  getLocation();
	});


	/*function getTypeadd(new_cords){
		var res = new Array();
		var components = new_cords.results[0].address_components;
		for(var k=0; k < components.length; k++){
			if(components[k].types.indexOf('postal_code') >= 0){
				res['postal_code'] = components[k].long_name;
			}else if(components[k].types.indexOf('sublocality_level_2') >= 0){
				res['address_line1'] = components[k].long_name;
			}else if(components[k].types.indexOf('sublocality_level_1') >= 0){
				res['address_line2'] = components[k].long_name;
			}else if(components[k].types.indexOf('administrative_area_level_1') >= 0){
				res['state'] = components[k].long_name;
			}else if(components[k].types.indexOf('administrative_area_level_2') >= 0){
				res['city'] = components[k].long_name;
			}else if(components[k].types.indexOf('country') >= 0){
				res['country'] = components[k].long_name;
			}

		}
		$("#search-box").val(res['postal_code']);
		return res;
	}*/

	
</script>
<?php require_once USER_VIEW_PATH . 'frontouter.inc.php';?>