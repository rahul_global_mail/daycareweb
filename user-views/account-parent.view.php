<?php
$pageName = 'home';
require_once USER_VIEW_PATH . 'header.inc.php';

?>
<!-- Inner Banner :: Start -->
<section class="inner-banner">
<div class="inner-content">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="profileimg-area">
                    <img class="img-fluid" src="assets/images/favicon.png" alt="Avtar"/>
                </div>
            </div>
            <div class="col-md-9">
                <div class="myaccount-name">
                    <h1>Hello, <?php echo $parentData[0]->fname . " " . $parentData[0]->lname; ?></h1>
                    <h3><?php echo $parentData[0]->about; ?></h3>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!-- Inner Banner :: End -->
<!-- Inner Body :: Start -->
<section class="myaccount-section">
<div class="myaccount-menu">
    <div class="container scroll-container">
        <ul class="myaccount-nav scroll-nav nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="dashboard-tab" data-toggle="tab" href="#dashboard" role="tab">Dashboard</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="updateprofile-tab" data-toggle="tab" href="#updateprofile" role="tab">Update Profile</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="modal" data-target="#ChangeEmailModal">Change Email</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="modal" data-target="#ChangePasswordModal">Change Password</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="modal" data-target="#DeleteProfileModal">Delete Profile</a>
            </li>
        </ul>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content" id="myTabContent">

                <div class="tab-pane fade show active" id="dashboard" aria-labelledby="dashboard-tab">
                    <div class="myaccount-area pt-50 pb-50">
                    <?php if ($_SESSION['add_msg'] == 'success') {?>
                    <div class="alert alert-success alert-dismissible" id="success-alert">
                      <strong>Success!</strong> Data Submitted successfully.
                    </div>
                    <?php }?>
                    <?php if ($_SESSION['add_msg'] == 'error') {?>
                    <div class="alert alert-danger alert-dismissible" id="success-alert">
                      <strong>Failed!</strong> Somthing goes wrong.Please try again.
                    </div>
                    <?php }?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="section-title mb-5">
                                    <h2>My Dashboard</h2>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="tabscustom-tabs">
                                    <ul class="nav nav-tabs custom-tabs" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="dytab1-tab" data-toggle="tab" href="#dytab1" role="tab" aria-controls="dytab1" aria-selected="true">Babysitting Bookings</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="dytab2-tab" data-toggle="tab" href="#dytab2" role="tab" aria-controls="dytab2" aria-selected="false">Waitlist Applications</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="dytab3-tab" data-toggle="tab" href="#dytab3" role="tab" aria-controls="dytab3" aria-selected="false">Spot Alert</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="dytab1" role="tabpanel" aria-labelledby="dytab1-tab">
                                            <div class="table-responsive">
                                                <table class="table table-striped-odd table-bordered mb-0 text-nowrap DataTables">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center no-sort">#</th>
                                                            <th class="text-center no-sort">Image</th>
                                                            <th>Name</th>
                                                            <th>Email Address</th>
                                                            <th>Contact</th>
                                                            <th>Requested Date</th>
                                                            <th class="text-center">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
$bookNannyi = 1;
foreach ($bookNanny as $bookdata) {?>


                                                            <tr>
                                                                <td class="text-center"><?php echo $bookNannyi++; ?></td>
                                                                <td class="text-center">
                                                                    <img class="tb-img" src="upload_images/<?php echo $bookdata->photo; ?>" alt="Image"/>
                                                                </td>
                                                                <td><?php echo $bookdata->fname . " " . $bookdata->lname; ?></td>
                                                                <td><?php echo $bookdata->email_id; ?></td>
                                                                <td><?php echo $bookdata->contact; ?></td>
                                                                <td><?php echo $bookdata->from_date; ?></td>
                                                                <td class="text-center align-middle">
                                                                    <a target="_blank" href="profileNanny.php?nanID=<?php echo base64_encode($bookdata->nanny_id); ?>" class="btn btn-success btn-sm"><i class="fa fa-eye"></i> View Profile</a>
                                                                    <!-- <a href="#" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a> -->
                                                                </td>
                                                            </tr>
                                                            <?php }
?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="dytab2" role="tabpanel" aria-labelledby="dytab2-tab">
                                             <div class="table-responsive">
                                                <table class="table table-striped-odd table-bordered mb-0 text-nowrap DataTables">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center no-sort">#</th>
                                                            <th>Image</th>
                                                            <th>Care Provider Name</th>
                                                            <th>Email</th>
                                                            <th>Child Name</th>
                                                            <th>Requested Date</th>
                                                            <th>Days</th>
                                                            <th class="text-center">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
$waitlisti = 1;
foreach ($waitlist as $waitlistdata) {?>


                                                            <tr>
                                                                <td class="text-center"><?php echo $waitlisti++; ?></td>
                                                                <td class="text-center">
                                                                    <img class="tb-img" src="upload_images/<?php echo $waitlistdata->photo; ?>" alt="Image"/>
                                                                </td>
                                                                <td><?php echo $waitlistdata->provider_name; ?></td>
                                                                <td><?php echo $waitlistdata->provider_email; ?></td>
                                                                <td><?php echo $waitlistdata->child_fname . " " . $waitlist->child_lname; ?></td>
                                                                <td><?php echo $waitlistdata->req_date; ?></td>
                                                                <td><?php echo $waitlistdata->req_days; ?></td>
                                                                <td class="text-center align-middle">
                                                                    <a href="provider-details.php?careID=<?php echo base64_encode($waitlistdata->provider_id); ?>" class="btn btn-success btn-sm"><i class="fa fa-eye"></i> View Provider Profile</a>
                                                                </td>
                                                            </tr>
                                                            <?php }
?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="dytab3" role="tabpanel" aria-labelledby="dytab3-tab">
                                             <div class="table-responsive">
                                                <table class="table table-striped-odd table-bordered mb-0 text-nowrap DataTables">
                                                   <thead>
                                                        <tr>
                                                            <th class="text-center no-sort">#</th>
                                                            <th>Image</th>
                                                            <th>Care Provider Name</th>
                                                            <th>Email</th>
                                                            <th>Created Date</th>
                                                            <th class="text-center">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
$vacancyi = 1;
foreach ($vacancydata as $vacancy) {?>


                                                            <tr>
                                                                <td class="text-center"><?php echo $vacancyi++; ?></td>
                                                                <td class="text-center">
                                                                    <img class="tb-img" src="upload_images/<?php echo $vacancy->photo; ?>" alt="Image"/>
                                                                </td>
                                                                <td><?php echo $vacancy->provider_name; ?></td>
                                                                <td><?php echo $vacancy->provider_email; ?></td>
                                                                <td><?php echo $vacancy->created_date; ?></td>
                                                                <td class="text-center align-middle">
                                                                    <a href="provider-details.php?careID=<?php echo base64_encode($vacancy->provider_id); ?>" class="btn btn-success btn-sm"><i class="fa fa-eye"></i> View Provider Profile</a>
                                                                </td>
                                                            </tr>
                                                            <?php }
?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="package-subscribe">
                                    <div class="package-title">
                                        <h2>Premium<?php if($subscriptions): ?> <?php echo ' - '. $subscriptions[0]->plan_interval; ?><?php endif; ?></h2>
                                        <h3><sup><?php echo $subscriptions[0]->plan_amount_currency; ?></sup><?php echo $subscriptions[0]->plan_amount; ?></h3>
                                    </div>
                                    <div class="package-purchasedate">
                                        <?php if($subscriptions): ?>
                                        <span class="package-date-title">Purchase Date</span>
                                        <span class="package-date-cal"><?php echo date("F j, Y", strtotime($subscriptions[0]->created)) ; ?></span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="package-expiredate">
                                        <?php if($subscriptions): ?>
                                        <span class="package-date-title">Expire Date</span>
                                        <span class="package-date-cal"><?php echo date("F j, Y", strtotime($subscriptions[0]->enddate)) ; ?></span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="package-action">
                                        <?php if($subscriptions): ?>
                                        <a href="#" class="btn btn-primary">Renew Plan</a>
                                        <?php else: ?>
                                        <a class="btn btn-primary" data-toggle="modal" data-target="#subPlan">Subscribe Plan</a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="tab-pane fade" id="updateprofile" aria-labelledby="updateprofile-tab">
                    <div class="myaccount-area pt-50 pb-50">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="section-title mb-3">
                                    <h2>Edit Profile</h2>
                                </div>
                            </div>
                        </div>
                        <form method="post">
                        	
                            <input type="hidden" name="parent_id" value="<?php echo $parentData[0]->parent_id; ?>">
                            <div class="row mb-30">
                                <div class="col-md-6 form-group">
                                    <label>First Name <i class="required">*</i></label>
                                    <input type="text" class="form-control" placeholder="First Name" name="fname" required="" value="<?php echo $parentData[0]->fname; ?>">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Last Name <i class="required">*</i></label>
                                    <input type="text" name="lname" class="form-control" placeholder="Last Name" required="" value="<?php echo $parentData[0]->lname; ?>">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Address <i class="required">*</i></label>
                                    <input onFocus="geolocate();" type="text" class="form-control" placeholder="Address" id="address" name="address" required="" value="<?php echo $parentData[0]->address; ?>">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Town/Suburb <i class="required">*</i></label>
                                    <input type="text" id="suburb" class="form-control" placeholder="Town/Suburb" name="suburb" required="" value="<?php echo $parentData[0]->suburb; ?>">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Postcode <i class="required">*</i></label>
                                    <input id="postcode" type="text" class="form-control" placeholder="Postcode" name="postcode" required="" value="<?php echo $parentData[0]->postcode; ?>">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>City <i class="required">*</i></label>
                                    <input class="form-control" placeholder="City" required="" type="text" name="city" id="city" value="<?php echo $parentData[0]->city; ?>">
                                    <?php /* ?>
                                    <select class="form-control" name="city" id="city">
                                                <option value="">Select City</option>
                                                <?php
foreach ($cityData as $city) {?>
                                                <option value="<?php echo $city->city; ?>" <?php if ($parentData[0]->city == $city->city) {echo "selected";}?>><?php echo $city->city; ?></option>
                                                <?php }?>


                                            </select>
                                            <?php */ ?>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Phone Number <i class="required">*</i></label>
                                    <input type="text" name="phone" class="form-control" placeholder="Phone Number" value="<?php echo $parentData[0]->phone; ?>">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Contact Preference</label>
                                    <div class="pt-8">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="contactpreference-email" name="contact_preference" class="custom-control-input" value="Email" <?php if ($parentData[0]->contact_preference == 'Email') {echo 'checked';}?>>
                                            <label class="custom-control-label" for="contactpreference-email">Email</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="contactpreference-mobile" name="contact_preference" class="custom-control-input" value="Mobile" <?php if ($parentData[0]->contact_preference == 'Mobile') {echo 'checked';}?>>
                                            <label class="custom-control-label" for="contactpreference-mobile">Mobile</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="contactpreference-phone" name="contact_preference" class="custom-control-input" value="Phone" <?php if ($parentData[0]->contact_preference == 'Phone') {echo 'checked';}?>>
                                            <label class="custom-control-label" for="contactpreference-phone">Phone (daytime/evening)</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Occupation(s)</label>
                                    <input type="text" class="form-control" placeholder="Occupation(s)" name="occupation" value="<?php echo $parentData[0]->occupation; ?>">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Additional Languages</label>
                                    <select class="form-control multiselect-lang" multiple="multiple" name="language[]">
                                        <option>-- Select Language --</option>
                                        <?php $language = $parentData[0]->language;?>
                                        <option value="English" <?php if (preg_match('/\bEnglish\b/', $language)) {echo "selected";}?>>English</option>
                                        <option value="Hindi" <?php if (preg_match('/\bHindi\b/', $language)) {echo "selected";}?>>Hindi</option>
                                        <option value="Urdu" <?php if (preg_match('/\bUrdu\b/', $language)) {echo "selected";}?>>Urdu</option>
                                        <option value="Punjabi" <?php if (preg_match('/\bPunjabi\b/', $language)) {echo "selected";}?>>Punjabi</option>
                                    </select>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label>Profile</label>
                                    <textarea name="about" class="form-control summernote" required><?php echo $parentData[0]->about; ?></textarea>
                                </div>
                            </div>
                            <div class="row mb-30">
                                <div class="col-md-12">

                                    <h3 class="inner-title">Your Children</h3>
                                    <?php
$count = 1;
if ($chilData) {
	foreach ($chilData as $data) {
		?>
                                    <div class="children-form dchild-<?php echo $data->child_id; ?>">
                                        <h5 class="children-header">Child <span class="child-index">Information</span>- <?php echo $count++; ?></h5>
                                        <div class="children-body row">
                                            <div class="col-md-5 form-group">
                                                <label>Child's Name</label>
                                                <input type="text" class="form-control" value="<?php echo $data->child_name; ?>" disabled>
                                            </div>
                                            <div class="col-md-5 form-group">
                                                <label>Date of birth (child is not born yet please indicate due date)</label>
                                                <input type="text" class="form-control" value="<?php
$date = date_create($data->child_dob);
		echo $get_date = date_format($date, "j F, Y");?>" disabled>
                                            </div>
                                            <div class="col-md-2 form-group">
                                                <label>&nbsp;</label>
                                                <a href="#" class="btn btn-danger delete-child deleteChild" id="<?php echo $data->child_id; ?>">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php }} else {?>
                                        <tr>
                                            <td colspan="8" class="text-center">
                                                No Child Found.
                                            </td>
                                        </tr>
                                    <?php }?>

                                    <div id="NewContainerBox"></div>

                                </div>
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-secondary add-child" id="addmoreblock"><i class="fas fa-plus pr-1 f-13"></i> Add Child</button>
                                </div>
                            </div>
                            <input type="submit" value="Update Profile" name="updateParent" class="btn btn-primary">
                        </form>
                    </div>
                </div>
                <div class="tab-pane fade" id="changeemail" aria-labelledby="changeemail-tab">..3.</div>
                <div class="tab-pane fade" id="changepassword" aria-labelledby="changepassword-tab">..4.</div>
                <div class="tab-pane fade" id="deleteprofile" aria-labelledby="deleteprofile-tab">..5.</div>
            </div>
        </div>
    </div>
</div>
</section>


<!-- Inner Body :: End -->
<?php require_once USER_VIEW_PATH . 'footer-dashboard.inc.php';?>
<!-- Change Email Address :: Start -->
<div class="modal custom-modal fade " id="ChangeEmailModal" tabindex="-1" role="dialog" aria-labelledby="ChangeEmailModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered sm-modal" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="ChangeEmailModalLabel">Change Email Address</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
        </div>
        <div class="modal-body p-4">
            <p>Please complete the form below to change your <b>DayCare.com</b> login email address.</p>

            <form method="post" id="parentEmail">
                <div class="form-group">
                    <label>Current e-mail address</label>
                    <input type="email" class="form-control" placeholder="Email Address" value="<?php echo $parentData[0]->parent_email; ?>" disabled="">
                </div>
                <div class="form-group">
                    <label>New e-mail address</label>
                    <input type="email" class="form-control" name="newemail" placeholder="New Email Address" required="">
                </div>
                <input type="hidden" name="id" value="<?php echo $parentData[0]->parent_id; ?>">
                <input type="hidden" name="user_type" value="parent">
                <input type="hidden" name="action" value="emailchange">
                <button type="submit" class="btn btn-primary">Change</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </form>
        </div>
    </div>
</div>
</div>
<!-- Change Email Address :: End -->


<div class="modal custom-modal fade" id="subPlan" tabindex="-1" role="dialog" aria-labelledby="subPlanLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered sm-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="subPlanLabel">Add Subcriptions</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
            </div>
            <div class="modal-body p-4">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" id="subscribePlan">
                        <div class="col-md-12 form-group">
                            <label>Select Premium Plan</label>
                            <select class="form-control" name="plan" id="plan">
                                <option value="">Select Plan</option>
                                <option value="0">$<?php echo PARENT_MONTH_PRICE; ?> / Month</option>
                                <option value="1">$<?php echo PARENT_YEAR_PRICE; ?> / Year</option>
                            </select>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>Card Number</label>
                            <input type="text" class="form-control" id="card_number" name="card_number" placeholder="Card Number" required />
                            <div id="card-suggesstion-box"></div>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>Expiry Month / Year</label>
                            <input type="text" class="form-control" id="card_exp" name="card_exp" placeholder="Month / Year" required />
                        </div>
                        <div class="col-md-12 form-group">
                            <label>CVC Code</label>
                            <input type="text" class="form-control" id="card_cvc" name="card_cvc" placeholder="CVC Code" required />
                        </div>

                        <div class="col-md-12 form-group">
                            <label>Card Holder Name</label>
                            <input type="text" class="form-control" id="c_name" name="c_name" placeholder="Card Holder Name" required />
                        </div>

                        <div class="col-md-12 form-group">
                            <label>Postal Code</label>
                            <input type="text" class="form-control" id="post_code" name="post_code" placeholder="Postal Code" required />
                        </div>  
                        <input type="hidden" name="subscribe_plan" id="subscribe_plan" value="subscribe_plan">
                        <div class="col-md-12 form-group">
                            <input type="submit" class="btn btn-primary" id="updatePlan" name="updatePlan" value="Subscribe" />
                        </div> 
                    </form>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>



<!-- Delete Profile :: Start -->
<div class="modal custom-modal fade " id="DeleteProfileModal" tabindex="-1" role="dialog" aria-labelledby="DeleteProfileModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered sm-modal" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="DeleteProfileModalLabel">Delete Profile</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
        </div>
        <div class="modal-body p-4">
            <form method="post" id="parentRemove">
                <h5>SORRY TO SEE YOU GO...</h5>
                <p>Are you sure you want to delete your account?</p>
                <input type="hidden" name="id" value="<?php echo $parentData[0]->parent_id; ?>">
                <input type="hidden" name="user_type" value="parent">
                <input type="hidden" name="action" value="submituserdata">
                <button type="submit" class="btn btn-primary">Yes</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </form>
        </div>
    </div>
</div>
</div>
<!-- Delete Profile :: End -->
<!-- Change Password :: Start -->
<div class="modal custom-modal fade " id="ChangePasswordModal" tabindex="-1" role="dialog" aria-labelledby="ChangePasswordModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered sm-modal" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="ChangePasswordModalLabel">Change Password</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
        </div>
        <div class="modal-body p-4">
            <p>Please complete the form below to change your <b>DayCare.com</b> Password.</p>
            <form method="post" id="parentChangepassword">
                <div class="form-group">
                    <label>Current Password</label>
                    <input type="password" class="form-control" name="currentpassword" placeholder="Current Password" required="">
                </div>
                <div class="form-group">
                    <label>New Password</label>
                    <input type="password" id="password" class="form-control" name="newpassword" placeholder="New Password" required="">
                </div>
                <div class="form-group">
                    <label>Confirm Password</label>
                    <input type="password" class="form-control" name="confirmpassword" id="confirmpassword" placeholder="Confirm Password" required="">
                </div>

                <input type="hidden" name="id" value="<?php echo $parentData[0]->parent_id; ?>">
                <input type="hidden" name="user_type" value="parent">
                <input type="hidden" name="action" value="userpasswordchange">
                <button type="submit" class="btn btn-primary">Change</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <div style="color: red;" id="divCheckPasswordMatch">
            </form>

        </div>
    </div>
</div>
</div>
<!-- Change Password :: End -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw==" crossorigin="anonymous"></script>

<script>

	$(document).on('submit', '#subscribePlan', function(event){
        event.preventDefault();
        /*$(document).on('click', '#updatePlan', function (event) {*/
        var postData = {
                plan: $("#plan").val(),
                card_number: $("#card_number").val(),
                card_exp: $("#card_exp").val(),
                card_exp: $("#card_exp").val(),
                card_cvc: $("#card_cvc").val(),
                c_name: $("#c_name").val(),
                post_code: $("#post_code").val(),
                parent_id: '<?php echo $parentData[0]->parent_id; ?>',
                subscribe_plan: $("#subscribe_plan").val(),
            };
        $.ajax({
            type: "POST",
            url: "ajax/validCard.php",
            data:postData,
            success: function(data){
                var res = $.parseJSON(data);
                $("#subPlan").modal('hide');
                if(res.success == 0){
                    Swal.fire(
                        'Failed!',
                        res.token,
                        'error'
                    ).then((result) => {
                      //location.reload();
                    });
                }
                if(res.success == 1){
                    Swal.fire(
                        'Success!',
                        res.token,
                        'success'
                    ).then((result) => {
                      location.reload();
                    });
                }
            }
        });
        /*});*/
    });
function checkPasswordMatch() {
    var password = $("#password").val();
    var confirmPassword = $("#confirmpassword").val();
    if (password != confirmPassword){
        $("#divCheckPasswordMatch").text("Passwords do not match!");
    }
    else{
        $("#divCheckPasswordMatch").text("Passwords match.");
    }
}
</script>
<script>
    $(document).ready(function () {
		$('#card_number').mask('0000-0000-0000-0000');
    	$('#card_exp').mask('00/00');
   		$("#confirmpassword").keyup(checkPasswordMatch);
    });
</script>
<script type="text/javascript">
$(document).ready(function () {
    var countfieldbox = 1;
    $("#addmoreblock").bind("click", function () {
        var div = $("<div />");
        div.html(GetDynamicTextBox(countfieldbox++));
        $("#NewContainerBox").append(div);
    });
    $("#btnGet").bind("click", function () {
        var values = "";
        $("input[name=DynamicTextBox]").each(function () {
            values += $(this).val() + "\n";
        });
        alert(values);
    });
    $("body").on("click", "#removeContainerBox", function () {
        $(this).closest(".new-row-add").remove();
    });
    function GetDynamicTextBox(value) {
        return '<div class="new-row-add"><div class="children-form"><h5 class="children-header">Child <span class="child-index">Information</span></h5><div class="children-body row"><div class="col-md-5 form-group"><label>Childs Name</label><input type="text" name="child_name[]" class="form-control" placeholder="Childs Name"></div><div class="col-md-5 form-group"><label>Date of birth (child is not born yet please indicate due date)</label><input type="text" class="form-control datetimepicker-input valid" name="child_dob[]" id="datetimepickerdob' + value + '" data-target="#datetimepickerdob' + value + '" data-toggle="datetimepicker" placeholder="Date of Birth" name="child_dob[]"></div><div class="col-md-2 form-group"><label>&nbsp;</label><button type="button" class="btn btn-danger btn-sm btn-rounded" id="removeContainerBox"><i class="fa fa-close"></i> Remove</button></div></div></div></div>';
        $("#datetimepickerdob" + value).datetimepicker({
            format: 'L',
            maxDate: new Date()
        });
    }
});


</script>
<script>
$(document).on('submit', '#parentRemove', function(event){
event.preventDefault();
$.ajax({
    url:"ajax/deactiveAccount.php",
    method:'POST',
    data:new FormData(this),
    contentType:false,
    processData:false,
    success:function(data)
    {
      $('#parentRemove')[0].reset();
      if(data == 'success-parent'){
        window.location.replace("index.php");
      }else{
        Swal.fire(
          'Failed!','Somthing goes wrong.','error'
        ).then((result) => {
          // Reload the Page
          //window.location.replace("<?php echo $_SERVER['PHP_SELF']; ?>");
        });
      }

    }
});
});
</script>
<script>
$(document).on('submit', '#parentEmail', function(event){
event.preventDefault();
$.ajax({
    url:"ajax/userEmailupdate.php",
    method:'POST',
    data:new FormData(this),
    contentType:false,
    processData:false,
    success:function(data)
    {
      $('#parentEmail')[0].reset();
      $('#ChangeEmailModal').modal('hide');
      if(data == 'success-parent'){
        Swal.fire(
            'Success!',
            'Your Email has been changed.',
            'success'
        )
      }else{
        Swal.fire(
          'Failed!','Somthing goes wrong.','error'
        ).then((result) => {
          // Reload the Page
          //window.location.replace("<?php echo $_SERVER['PHP_SELF']; ?>");
        });
      }

    }
});
});
</script>
<script>
$(document).on('submit', '#parentChangepassword', function(event){
event.preventDefault();
$.ajax({
    url:"ajax/userPasswordupdate.php",
    method:'POST',
    data:new FormData(this),
    contentType:false,
    processData:false,
    success:function(data)
    {
      $('#parentChangepassword')[0].reset();
      $('#ChangePasswordModal').modal('hide');
      if(data == 'success-parent'){
        Swal.fire(
            'Success!',
            'Your Password has been changed.',
            'success'
        )
      }else{
        Swal.fire(
          'Failed!','Somthing goes wrong.','error'
        ).then((result) => {
          // Reload the Page
          //window.location.replace("<?php echo $_SERVER['PHP_SELF']; ?>");
        });
      }

    }
});
});
</script>
<script type="text/javascript">
    $(document).on('click', '.deleteChild', function(){
        var child_id = $(this).attr("id");
        //alert(user_id);
        Swal.fire({
            title: 'Are you sure?',
            text: "Once deleted, you will not be able to recover this data!",
            icon: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
        if (result.value) {
            $.ajax({
                url:"ajax/removeChild.php",
                method:"POST",
                data:{child_id:child_id,action:'removeChild'},
                success:function(data)
                {
                  //alert(data);
                  Swal.fire(
                    'Deleted!',
                    'Child Data has been removed.',
                    'success'
                    )
                  $(".dchild-"+child_id).hide();
                  //window.location.replace("<?php echo ADMINROOT; ?>spinningmills-users.php");
                }
              });
        }
    })
});
</script>
<script>
    $("#success-alert").fadeTo(2000, 900).slideUp(500, function(){
    $("#success-alert").slideUp(900);
    <?php $_SESSION['add_msg'] = '';?>
});
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZPcppwWN179bx8Asxh8Xsd-ZiLIvZNMM&libraries=places"></script>

<script type="text/javascript">
    var placeSearch, autocomplete;

    function initialize() {
        autocomplete = new google.maps.places.Autocomplete(
        (document.getElementById('address')), {
            types: ['geocode']
        });
        google.maps.event.addDomListener(document.getElementById('address'), 'focus', geolocate);

        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            console.log(place.address_components);
            var res = new Array();
            for(var k=0; k < place.address_components.length; k++){
                if(place.address_components[k].types.indexOf('postal_code') >= 0){
                    res['postal_code'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('locality') >= 0){
                    res['suberb'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('sublocality_level_2') >= 0){
                    res['address_line1'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('sublocality_level_1') >= 0){
                    res['address_line2'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('administrative_area_level_1') >= 0){
                    res['state'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('administrative_area_level_2') >= 0){
                    res['city'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('country') >= 0){
                    res['country'] = place.address_components[k].short_name;
                }

            }
            $("#postcode").val(res['postal_code']);
            $("#suburb").val(res['suberb']);
            $("#city").val(res['city']);
            $("#country").val(res['country']);
        });
    }

    function geolocate() {

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                
                $("#lat").val(position.coords.latitude);
                $("#long").val(position.coords.longitude);

                var geolocation = new google.maps.LatLng(
                position.coords.latitude, position.coords.longitude);
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }

    window.addEventListener('load', (event) => {
        initialize();
    });
</script>
<?php require_once USER_VIEW_PATH . 'frontouter.inc.php';?>