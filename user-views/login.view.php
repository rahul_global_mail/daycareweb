<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="DayCare - AdminPanel" />
        <meta name="author" content="DayCare - AdminPanel" />
        <meta name="robots" content="DayCare - AdminPanel" />
        <meta name="description" content="DayCare - AdminPanel" />
        <link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon" />
        <title><?php echo PROJECTTITLE; ?></title>

        <!-- google font -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800,900&display=swap" rel="stylesheet">

        <!-- icons -->
        <link href="<?php echo SITE_URL; ?>assets/fonts/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

        <!-- css -->
        <link href="<?php echo SITE_URL; ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo SITE_URL; ?>assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo SITE_URL; ?>assets/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo SITE_URL; ?>assets/css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo SITE_URL; ?>assets/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo SITE_URL; ?>assets/css/responsive.css" rel="stylesheet" type="text/css"/>
    </head>

    <body class="login_page">
        <div class="main-wrapper">
            <div class="account-page">
                <div class="container">
                    <div class="login-container row d-flex justify-content-between">
                        <div class="col-md-6 align-self-md-center login-content">
                            <div class="newlogin-logo">
                                <img class="login-logo img-fluid" src="assets/images/logo-white.png" alt="<?php echo PROJECTTITLE; ?>"/>
                            </div>
                            <hr>
                            <h3 class="inner-text text-white">The Bootstrap 4 Admin Theme by Propeller is a full featured premium HRMS Admin theme built using Propeller Pro framework.</h3>
                            <p class="lead">The theme suitable for building CMS, CRM, ERP, Admin Panel, or a web application.</p>
                        </div>
                        <div class="col-md-6 align-self-md-center">
                            <div class="new-logincard card">
                                <div class="login-card">
                                    <div class="card-header">
                                        <h2 class="card-title">Welcome to <span>Admin</span></h2>
                                    </div>
                                    <?php if ($_SESSION['login_error'] == 'incorrect') {?>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                          <strong>Opps!</strong> Login Failed.
                                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                          </button>
                                        </div>
                                    <?php $_SESSION['login_error'] = '';}?>
                                    <form method="post">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <div class="input-group mb-2">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text"><i class="fa fa-envelope-o"></i></div>
                                                    </div>
                                                    <input type="email" class="form-control" placeholder="Email Address" name="username">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group mb-2">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text"><i class="fa fa-key"></i></div>
                                                    </div>
                                                    <input type="password" class="form-control" placeholder="Password" name="password">
                                                </div>
                                            </div>
                                            <div class="form-group mb-0 clearfix">
                                                <div class="float-left">
                                                    <div class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" value="" id="rememberme" checked/>
                                                        <label class="custom-control-label" for="rememberme">Remember Me</label>
                                                    </div>
                                                </div>
                                                <span class="float-right">
                                                    <a href="forgot-password.php" class="forgot-password text-primary">Forgot password?</a>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="card-footer text-center">
                                            <button type="submit" name="submit" class="btn btn-primary btn-block btn-lg m-0">Sign In</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery -->
        <script src="<?php echo SITE_URL; ?>assets/js/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo SITE_URL; ?>assets/js/popper.js" type="text/javascript"></script>
        <script src="<?php echo SITE_URL; ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo SITE_URL; ?>assets/js/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
        <script src="<?php echo SITE_URL; ?>assets/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="<?php echo SITE_URL; ?>assets/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
        <script src="<?php echo SITE_URL; ?>assets/js/bootstrap-select.min.js" type="text/javascript"></script>
        <script src="<?php echo SITE_URL; ?>assets/js/scripts.js" type="text/javascript"></script>
    </body>
</html>