<?php
$pageName = 'home';
require_once USER_VIEW_PATH . 'header.inc.php';?>
<!-- Inner Banner :: Start -->
<section class="banners-section">
    <div id="masterslider1" class="master-slider ms-skin-default" >
        <div class="ms-overlay-layers">
            <img class="ms-layer" src="assets/images/blank.gif" data-src="assets/images/arrow-nxt.png" alt="" style="" data-ease="easeOutQuint" data-type="image" data-action="next" data-offset-x="50" data-offset-y="0" data-origin="mr" data-position="normal" />
            <img class="ms-layer" src="assets/images/blank.gif" data-src="assets/images/arrow-prv.png" alt="" style="" data-ease="easeOutQuint" data-type="image" data-action="previous" data-offset-x="104" data-offset-y="0" data-origin="mr" data-position="normal" />
        </div>
        <?php
if ($galleryData) {
	foreach ($galleryData as $banner) {?>
        <div class="ms-slide" data-delay="5.2" data-fill-mode="fill" >
            <img src="assets/images/blank.gif" alt="" title="bride-bg-slide1" data-src="upload_images/<?php echo $banner->banner_path; ?>" />
        </div>
        <?php }} else {?>
        <div class="ms-slide" data-delay="5" data-fill-mode="fill" >
            <img src="assets/images/blank.gif" alt="" title="bride-bg-slide1" data-src="assets/images/banner/slider-2.jpg" />-->
        </div>
        <?php }?>
        <!--<div class="ms-slide" data-delay="5" data-fill-mode="fill" >-->
        <!--    <img src="assets/images/blank.gif" alt="" title="bride-bg-slide1" data-src="assets/images/banner/slider-2.jpg" />-->
        <!--</div>-->
        <!--<div class="ms-slide" data-delay="5" data-fill-mode="fill" >-->
        <!--    <img src="assets/images/blank.gif" alt="" title="bride-bg-slide1" data-src="assets/images/banner/slider-1.jpg" />-->
        <!--</div>-->
    </div>
    <div class="innerbanner-section">
        <div class="innerbanner-contextual-header">
            <div class="container">
                <div class="row">
                    <div class="col-xl-2 col-lg-2 col-md-3">
                        <div class="innerbanner-image">
                            <img class="img-fluid" src="upload_images/<?php echo $providerData[0]->photo; ?>" alt="Profile Image"/>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-7 col-md-9">
                        <div class="innerbanner-personaldetails">
                            <h1 class="innerbanner-title"><?php echo ucwords($providerData[0]->provider_name); ?></h1>
                            <div class="childcare-address"><i class="fas fa-map-marker-alt"></i><?php echo $providerData[0]->address . " , " . $providerData[0]->suburb . " , " . $providerData[0]->postcode . " - " . $providerData[0]->country; ?></div>
                            <div class="childcare-address"><i class="fas fa-user"></i>Contact name : <b><?php echo ucwords($providerData[0]->contact_name); ?> </b></div>
                            <div class="childcare-address"><i class="fas fa-envelope"></i>Email : <b><?php echo $providerData[0]->provider_email; ?></b></div>
                            <div class="childcare-address"><i class="fas fa-check"></i>Approved Places : <b><?php echo $providerData[0]->approved_places; ?></b></div>
                            <div class="childcare-phone">
                                <a class="childcare-phonelink" href="tel:<?php echo $providerData[0]->phone; ?>"><i class="fas fa-phone-alt"></i><?php echo $providerData[0]->phone; ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-9 offset-lg-0 offset-md-3">
                        <div class="userprofile-rightarea">
                        <?php
if ($providerData[0]->is_vacancy == 'Yes') {?>

                        <button type="submit" class="btn btn-primary btn-childsm"><i class="fas fa-check pr-1"></i> Vacancies Available</button>
                        <?php }?>
                        <div class="innerbanner-gettouch mt-2">
                            <ul>
                                <li>
                                    <a href="javascript:void(0)" class="btn btn-gettouch" data-toggle="tooltip" data-placement="top" title="Send E-Mail"><i class="far fa-envelope"></i></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" class="btn btn-gettouch" data-toggle="tooltip" data-placement="top" title="WaitList"><i class="far fa-calendar-alt"></i></a>
                                </li>
                                <?php
if ($providerData[0]->web_app_link != "") {?>
    <li>
                                    <a href="<?php echo $providerData[0]->web_app_link; ?>" class="btn btn-gettouch" data-toggle="tooltip" data-placement="top" title="Website" target="_blank"><i class="fas fa-globe"></i></a>
                                </li>
<?php }?>
<?php
if ($providerData[0]->inspection_link != "") {?>

                                <li>
                                    <a href="<?php echo $providerData[0]->inspection_link; ?>" class="btn btn-gettouch" data-toggle="tooltip" data-placement="top" title="Inspection Website Link" target="_blank"><i class="fas fa-certificate"></i></a>
                                </li>
                                <?php }?>

                                <?php
if ($providerData[0]->fb_link != "") {?>
    <li>
    <a href="<?php echo $fb_link; ?>" class="btn btn-gettouch" data-toggle="tooltip" data-placement="top" title="Facebook Link" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                </li>
<?php }?>

                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inner Banner :: End -->
<!-- Inner Body :: Start -->
<section id="services-menu" class="maindetailsmenu-section innermenu">
    <div class="maindetails-menu">
        <div class="container scroll-container">
            <ul class="maindetails-nav scroll-nav nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#info-scroll">Info</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#vacancies-scroll">Vacancies</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#fees-scroll">Fees</a>
                </li>
                <?php
if (isset($_SESSION['parentData']) && !empty($_SESSION['parentData'])) {?>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="modal" data-target="#WaitlistModal">Waitlist</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  data-toggle="modal" data-target="#VacancyalertModal">Spot Alert</a>
                </li>
            <?php } else {?>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="modal" data-target="#loginModal">Waitlist</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="modal" data-target="#loginModal">Spot Alert</a>
                </li>
            <?php }?>
                <li class="nav-item">
                    <a class="nav-link" href="#reviews-scroll">Reviews</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="#contact-scroll">Contact</a>
                </li>
            </ul>
        </div>
    </div>
</section>

<section id="info-scroll" class="pt-50 pb-50">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h4 class="info-title">About <?php echo ucwords($providerData[0]->provider_name); ?></h4>
                <div class="childcare-deatilsrating">
                    <i class="fas fa-star"></i>
                    <i class="far fa-star"></i>
                    <i class="far fa-star"></i>
                    <i class="far fa-star"></i>
                    <i class="far fa-star"></i>
                    <span class="givenrating">0 Ratings</span>
                </div>
                <div class="info-desc">
                    <!-- <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    <p class="text-bold">Our centre offers the following features:</p>
                    <ul class="list-dott mb-20">
                        <li>The standard Lorem Ipsum passage, used since the 1500s</li>
                        <li>Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</li>
                        <li>1914 translation by H. Rackham</li>
                        <li>Section 1.10.33 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</li>
                        <li>1914 translation by H. Rackham</li>
                        <li>Ut enim ad minima veniam</li>
                    </ul>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <p class="text-bold">Only $157 per day for Nursery and Toddlers</p>
                    <p class="text-bold">Only $147 per day for Preschoolers</p>
                    <p>Check out our <span class="text-bold">website www.lighthouseelc.com.au</span> or give us a call on <span class="text-bold">8937 4433</span></p>
                    <p>National quality standard rating: Meeting National Quality Standard.</p> -->
                    <?php echo $providerData[0]->about; ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sidebar-info">
                    <div class="sidebar-box">
                        <div class="sidebar-title">Services</div>
                        <div class="sidebar-body">
                            <ul class="services-list">
                                <?php
$explode = explode(",", $providerData[0]->provider_service_id);
foreach ($explode as $value) {
	$service_data = $model_provider->getsearch_get_careservices($value);
	?>
                                <li><?php echo $service_data[0]->title; ?></li>

                            <?php }?>
                            </ul>
                            <p>What are the different types of child care?</p>
                        </div>
                    </div>
                    <div class="sidebar-box">
                        <div class="sidebar-title">Service Extras</div>
                        <div class="sidebar-body">
                            <ul class="serviceextra-list">
                                <li><span class="child-care-indicator active" data-toggle="tooltip" data-placement="bottom" title="Allergy Aware"><i class="fas fa-syringe"></i></span></li>
                                <li><span class="child-care-indicator" data-toggle="tooltip" data-placement="bottom" title="Bottles Supplied"><i class="fas fa-prescription-bottle"></i></span></li>
                                <li><span class="child-care-indicator active" data-toggle="tooltip" data-placement="bottom" title="Education & Developmental programs"><i class="fas fa-graduation-cap"></i></span></li>
                                <li><span class="child-care-indicator active" data-toggle="tooltip" data-placement="bottom" title="Meals Provided"><i class="fas fa-concierge-bell"></i></span></li>
                                <li><span class="child-care-indicator active" data-toggle="tooltip" data-placement="bottom" title="Nappies Supplied"><i class="fas fa-baby"></i></span></li>
                                <li><span class="child-care-indicator active" data-toggle="tooltip" data-placement="bottom" title="NQS Rating"><i class="fas fa-star"></i></span></li>
                                <li><span class="child-care-indicator active" data-toggle="tooltip" data-placement="bottom" title="Sun Safe Aware"><i class="fas fa-sun"></i></span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="sidebar-box">
                        <div class="sidebar-title">Location</div>
                        <div class="sidebar-body">
                            <!-- <div class="map-area">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d395.58677870284004!2d-113.48621743280164!3d53.542738028782715!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sbabycare%20canada!5e0!3m2!1sen!2sin!4v1590327230459!5m2!1sen!2sin"></iframe>
                            </div> -->
                            <div class="map-area">
                                <iframe src="https://maps.google.com/maps?q=<?php echo $locationData[0]->city . "," . $locationData[0]->state . "," . $locationData[0]->postcode; ?> V5X 2C4, Canada&z=15&output=embed" width="250" height="270" frameborder="0" style="border:0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="vacancies-scroll" class="pt-50 pb-50 bg-grey">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="info-title mb-20">Vacancies</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mb-20">
                <!-- <h4 class="inner-title-t1">Permanent</h4> -->
                <div class="availability-area">
                    <div class="row">
                        <?php
$vc_count = 1;
foreach ($vacancyData as $vacancy) {
	?>
                        <div class="col-md-6">
                            <div class="availability-box">
                                <div class="availability-header">
                                    <div class="availability-date">
                                        <span class="availabilitydate-title">Start From</span>
                                        <span class="availabilitydate-label"><?php echo $vacancy->start_from; ?></span>
                                    </div>
                                    <div class="availability-months"><?php echo $vacancy->from_age . " " . $vacancy->from_age_type . " TO " . $vacancy->to_age . " " . $vacancy->to_age_type; ?></div>
                                </div>
                                <div class="availability-week">
                                    <?php
$arrDay = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
	foreach ($arrDay as $day) {
		$day_data = $model_provider->day_get_vacancy($vacancy->vacancy_id, $day);?>
                                    <span class="availabilityweek-label <?php if ($day_data[0]->day > 0) {echo "availability";}?>"><?php echo $day; ?></span>
                                    <?php }?>
                                    <!-- <span class="availabilityweek-label">Monday</span>
                                    <span class="availabilityweek-label">Tuesday</span>
                                    <span class="availabilityweek-label availability">Wednesday</span>
                                    <span class="availabilityweek-label">Thursday</span>
                                    <span class="availabilityweek-label">Friday</span>
                                    <span class="availabilityweek-label availability">Saturday</span> -->
                                </div>
                            </div>
                        </div>
                        <?php }?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="fees-scroll" class="pt-50 pb-50">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="info-title mb-20">Fees</h4>
            </div>
        </div>
        <div class="row mb-4">
            <?php
if ($feesData) {
	foreach ($feesData as $fees) {
		?>
            <div class="col-md-4">
                <div class="fees-price-block">
                    <div class="feesprice-header">
                        <h2><?php echo $fees->title; ?></h2>
                    </div>
                    <div class="feesprice-price"><?php echo "$" . $fees->price . " / " . $fees->type; ?></div>
                    <div class="feesprice-list">
                        <ul>
                            <li><?php echo $fees->ages; ?></li>
                            <li><?php if (isset($fees->notes) && !empty($fees->notes)) {echo $fees->notes;} else {echo "-";}
		;?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <?php }} else {?>
                <div class="col-md-8">

                        <div class="feesprice-header">
                            <h3>No Fees Data Available.</h3>
                        </div>
                </div>
            <?php }?>
            <!-- <div class="col-md-4">
                <div class="fees-price-block">
                    <div class="feesprice-header">
                        <h2>Long Day Care</h2>
                    </div>
                    <div class="feesprice-price">$168 / Day</div>
                    <div class="feesprice-list">
                        <ul>
                            <li>for ages 2yrs to 3yrs</li>
                            <li>-</li>
                        </ul>
                    </div>
                </div>
            </div> -->
        </div>
        <!-- <div class="row">
            <div class="col-md-12">
                <p>We don't have any fee details right now. To find out please <b>contact</b> Only About Children Market St directly.</p>
                <p>Use our Child Care Subsidy Calculator to learn how much government support your family may be eligible for.</p>
            </div>
        </div> -->
    </div>
</section>

<!-- <section id="rating" class="pt-50 pb-50 bg-grey">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="info-title mb-20">Ratings</h4>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-md-12">
                <div class="aggregate-rating">
                    <div class="aggregate-ratinglist">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="far fa-star"></i>
                    </div>
                    <div class="aggregate-title">Excellent Quality</div>
                </div>
            </div>
        </div>
    </div>
</section> -->

<section id="reviews-scroll" class="pt-50 pb-50 bg-grey">
    <div class="container">
        <div class="row">
            <div class="col-7 col-md-6 pr-0">
                <h4 class="info-title mb-20">Reviews <span class="count-review">(0 Reviews)</span></h4>
            </div>
            <div class="col-5 col-md-6 text-right pt-2 pl-0">
                <a href="javascript:void(0)" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#PostReviewModal">Post A Review</a>
            </div>
        </div>
        <!-- <div class="row mb-20">
            <div class="col-md-12">
                <div class="review-box">
                    <div class="review-header clearfix">
                        <div class="review-by">
                            <div class="review-author">Malou Galvez</div>
                            <div class="review-date">Aug 13, 2020 at 12:26 pm</div>
                        </div>
                        <div class="review-author-rating">
                            <div class="review-rating">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                            </div>
                        </div>
                    </div>
                    <div class="review-body">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="row">
            <div class="col-md-12">
                <p>We don't have any reviews just yet.</p>
                <!-- <a href="javascript:void(0)" class="btn btn-primary f-18" data-toggle="modal" data-target="#PostReviewModal">Post A Review</a> -->
            </div>
        </div>
    </div>
</section>

<section id="specialoffer" class="pt-50 pb-50">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="info-title mb-20">Special Offer</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h5>Be referred by a friend and receive $500 credit!</h5>
                <p>When you are referred to Oac by an enrolled family, both you and your referring friend can receive $500 credit* each. T&Cs apply.</p>
                <p>Call 138 622 or visit our website.</p>
                <a href="https://www.oac.edu.au/refer/" class="btn btn-primary f-18" target="_blank">Find Out More</a>
            </div>
        </div>
    </div>
</section>

<section id="contact-scroll" class="pt-50 pb-50 bg-grey">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="info-title mb-20">Contact <?php echo ucwords($providerData[0]->contact_name); ?> </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <ul class="contact-area-list">
                    <li class="contact-icon mb-3 d-flex align-items-center">
                        <span class="fas fa-map-marker-alt mr-3"></span>
                        <p><?php echo $providerData[0]->suburb . " , " . $providerData[0]->city . " , " . $providerData[0]->postcode . " - " . $providerData[0]->country; ?></p>
                    </li>
                    <li class="contact-icon mb-3 d-flex align-items-center">
                        <span class="fas fa-user mr-3"></span>
                        <p>Contact name : <b><?php echo ucwords($providerData[0]->contact_name); ?></b></p>
                    </li>

                </ul>
            </div>
            <div class="col-md-6">
                <ul class="contact-area-list">
                    <!-- <li class="contact-icon mb-3 d-flex align-items-center">
                        <span class="fas fa-clock mr-3"></span>
                        <p>Best contact time between : <b>Anytime</b></p>
                    </li> -->
                    <li class="contact-icon mb-3 d-flex align-items-center">
                        <span class="fas fa-phone mr-3"></span>
                        <p><?php echo $providerData[0]->phone; ?></p>
                    </li>
                    <li class="contact-icon mb-3 d-flex align-items-center">
                        <span class="fas fa-globe mr-3"></span>
                        <p><?php if ($providerData[0]->website) {echo $providerData[0]->website;} else {echo "#";}?></p>
                    </li>
                </ul>
            </div>
            <!--<div class="col-md-12">-->
            <!--    <div class="contact-area">-->
            <!--        <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d395.58677870284004!2d-113.48621743280164!3d53.542738028782715!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sbabycare%20canada!5e0!3m2!1sen!2sin!4v1590327230459!5m2!1sen!2sin"></iframe>-->
            <!--    </div>-->
            <!--</div>-->
        </div>
    </div>
</section>
<!--- Inner Body :: End --->
<!-- Reviews Modal :: Start -->
<div class="modal custom-modal fade " id="PostReviewModal" tabindex="-1" role="dialog" aria-labelledby="PostReviewModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered md-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="PostReviewModal">Post A Review</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
            </div>
            <form>
                <div class="modal-body p-4">
                    <div class="form-group">
                        <label>Full Name<span class="required">*</span></label>
                        <input type="text" class="form-control" name="reviewfullname" placeholder="Full Name" required="">
                    </div>
                    <div class="form-group">
                        <label>Email Address<span class="required">*</span></label>
                        <input type="email" class="form-control" name="reviewemail" placeholder="Email Address" required="">
                    </div>
                    <div class="form-group">
                        <label>Post a Review<span class="required">*</span></label>
                        <textarea class="form-control" rows="5" placeholder="Post a Review"></textarea>
                    </div>
                    <div class="clearfix mb-3">
                        <div class="checkboxes">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="rememberme">
                                <label class="custom-control-label" for="rememberme">I here by confirm</label>
                            </div>
                        </div>
                    </div>
                    <ul class="list-disc pl-4">
                        <li>I have a child currently attending this service, or a child who has attended sometime in the last 12 months</li>
                        <li>Have not submitted a review previously</li>
                        <li>Read and agree to our Terms & Conditions and Privacy Policy.</li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Post Review</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Reviews Modal :: End -->
<!-- Vaccany alert Modal :: Start -->
<div class="modal custom-modal fade " id="VacancyalertModal" tabindex="-1" role="dialog" aria-labelledby="VacancyalertModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered md-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="VacancyalertModal">Post Vancay Alert</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
            </div>
            <form method="post" id="savevacancydata">
                <input type="hidden" name="vacancy_parent_id" value="<?php echo $_SESSION['parentData'][0]->parent_id; ?>">
                <input type="hidden" name="provider_id" value="<?php echo $providerData[0]->provider_id; ?>">
                <input type="hidden" name="action" value="savevacancyalertdetail">
                <div class="modal-body p-4">
                    <div class="form-group">
                        <label>Full Name<span class="required">*</span></label>
                        <input type="text" class="form-control" name="parent_name" placeholder="Full Name" value="<?php echo $_SESSION['parentData'][0]->fname . " " . $_SESSION['parentData'][0]->lname; ?>" required="">
                    </div>
                    <div class="form-group">
                        <label>Email Address<span class="required">*</span></label>
                        <input type="email" class="form-control" name="parent_email" placeholder="Email Address" value="<?php echo $_SESSION['parentData'][0]->parent_email; ?>" required="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Vaccany alert Modal :: End -->
<!-- Waitlist Modal :: Start -->
<div class="modal custom-modal fade " id="WaitlistModal" tabindex="-1" role="dialog" aria-labelledby="WaitlistModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered md-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="WaitlistModalLabel">Waitlist</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
            </div>
            <div class="modal-body p-4">
                <form method="post" id="waitlistprovider">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Parent Name<span class="required">*</span></label>
                                <input type="text" class="form-control" name="carer_fullname" placeholder="Full Name" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email<span class="required">*</span></label>
                                <input type="email" class="form-control" name="email" placeholder="Email" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Mobile Number<span class="required">*</span></label>
                                <input type="text" class="form-control" name="mobile" placeholder="Mobile Number" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Child First Name<span class="required">*</span></label>
                                <input type="text" class="form-control" name="child_fname" placeholder="Child First Name" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Child Last Name<span class="required">*</span></label>
                                <input type="text" class="form-control" name="child_lname" placeholder="Child Last Name" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>DOB/Expected DOB<span class="required">*</span></label>
                                <input type="text" name="child_dob" class="form-control" id="ChildDOBDatePicker" data-toggle="datetimepicker" data-target="#ChildDOBDatePicker"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Care Required Date<span class="required">*</span></label>
                                <input type="text" name="req_date" class="form-control" id="CareRequiredDateDatePicker" data-toggle="datetimepicker" data-target="#CareRequiredDateDatePicker"/>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Days Required<span class="required">*</span></label>
                                <br>
                                <div class="btn-group btn-group-toggle" id="DaysRequired" data-toggle="buttons">
                                    <label class="btn btn-toggle">
                                        <input type="checkbox" name="req_days[]" value="Monday" autocomplete="off"> Mon
                                    </label>
                                    <label class="btn btn-toggle">
                                        <input type="checkbox" name="req_days[]" value="Tuesday" autocomplete="off"> Tue
                                    </label>
                                    <label class="btn btn-toggle">
                                        <input type="checkbox" name="req_days[]" value="Wednesday" autocomplete="off"> Wed
                                    </label>
                                    <label class="btn btn-toggle">
                                        <input type="checkbox" name="req_days[]" value="Thursday" autocomplete="off"> Thu
                                    </label>
                                    <label class="btn btn-toggle">
                                        <input type="checkbox" name="req_days[]" value="Friday" autocomplete="off"> Fri
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>I am interested in the following centre(s)<span class="required">*</span></label>
                                <select name="interest[]" class="form-control selectinterested" multiple data-live-search="true" title="Select Interested" data-selected-text-format="count > 5">
                                    <option value="all" class="select-all">Select All interested</option>
                                    <?php
foreach ($serviceData as $service) {?>
                                    <option value="<?php echo $service->title; ?>"><?php echo $service->title; ?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Referral Source<span class="required">*</span></label>
                                <select class="form-control" name="referal_from">
                                    <option value="Google">Google</option>
                                    <option value="CareForKids.com.au">CareForKids.com.au</option>
                                    <option value="MyChild Website">MyChild Website</option>
                                    <option value="Our Website">Our Website</option>
                                    <option value="Word of Mouth Referral">Word of Mouth Referral</option>
                                    <option value="Drive Walk Past">Drive Walk Past</option>
                                    <option value="Other">Other</option>
                                    <option value="Imported">Imported</option>
                                    <option value="My Waitlist">My Waitlist</option>
                                    <option value="Facebook">Facebook</option>
                                    <option value="Newspaper">Newspaper</option>
                                    <option value="Flyer">Flyer</option>
                                    <option value="Employer">Employer</option>
                                    <option value="Event">Event</option>
                                    <option value="Outdoor Advertising">Outdoor Advertising</option>
                                    <option value="Coffee Cup Advertising">Coffee Cup Advertising</option>
                                    <option value="Vacancy Care">Vacancy Care</option>
                                    <option value="ChildcareNearMe.com.au">ChildcareNearMe.com.au</option>
                                    <option value="Centre Transfer">Centre Transfer</option>
                                    <option value="Returning Family">Returning Family</option>
                                    <option value="Expo">Expo</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Comments<span class="required">*</span></label>
                                <textarea rows="3" class="form-control" name="comments"></textarea>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="parent_id" value="<?php echo $_SESSION['parentData'][0]->parent_id; ?>">
                    <input type="hidden" name="provider_id" value="<?php echo $providerData[0]->provider_id; ?>">
                    <input type="hidden" name="action" value="savewaitlist">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Waitlist Modal :: End -->
<?php require_once USER_VIEW_PATH . 'footer.inc.php';?>
<script>
    $('#ChildDOBDatePicker').datetimepicker({
        format: 'L',
        format: 'DD/MM/YYYY'
    });
    $('#CareRequiredDateDatePicker').datetimepicker({
        format: 'L',
        format: 'DD/MM/YYYY'
    });
</script>
<script>
    var masterslider1 = new MasterSlider();
    masterslider1.control('timebar', {autohide: false, overVideo: true, align: 'top', color: '#FFFFFF', width: 4});
    masterslider1.setup("masterslider1", {
        width: 1366,
        height: 600,
        minHeight: 400,
        space: 0,
        start: 1,
        grabCursor: true,
        swipe: false,
        mouse: true,
        keyboard: false,
        layout: "",
        wheel: false,
        autoplay: true,
        instantStartLayers: true,
        loop: true,
        shuffle: false,
        preload: 0,
        heightLimit: true,
        autoHeight: false,
        smoothHeight: true,
        endPause: false,
        overPause: false,
        fillMode: "fill",
        centerControls: false,
        startOnAppear: false,
        layersMode: "center",
        autofillTarget: "",
        hideLayers: false,
        fullscreenMargin: 80,
        speed: 20,
        dir: "h",
        parallaxMode: 'swipe',
        view: "parallaxMask"
    });

</script>
<script>
$(document).on('submit', '#waitlistprovider', function(event){
event.preventDefault();
$.ajax({
    url:"ajax/savewaitlist.php",
    method:'POST',
    data:new FormData(this),
    contentType:false,
    processData:false,
    success:function(data)
    {
      $('#waitlistprovider')[0].reset();
      $('#WaitlistModal').modal('hide');
      if(data == 'success-provider'){
        Swal.fire(
            'Success!',
            'Your WaitList has been submitted.',
            'success'
        )
      }else{
        Swal.fire(
          'Failed!','Somthing goes wrong.','error'
        ).then((result) => {
          // Reload the Page
          //window.location.replace("<?php echo $_SERVER['PHP_SELF']; ?>");
        });
      }

    }
});
});
</script>
<script>
$(document).on('submit', '#savevacancydata', function(event){
event.preventDefault();
$.ajax({
    url:"ajax/savevacancyalert.php",
    method:'POST',
    data:new FormData(this),
    contentType:false,
    processData:false,
    success:function(data)
    {
      $('#savevacancydata')[0].reset();
      $('#VacancyalertModal').modal('hide');
      if(data == 'success-alert'){
        Swal.fire(
            'Success!',
            'Your Spot alert request has been submitted.',
            'success'
        )
      }else{
        Swal.fire(
          'Exist!','You have already requested for Spot Alert.','warning'
        ).then((result) => {
          // Reload the Page
          //window.location.replace("<?php echo $_SERVER['PHP_SELF']; ?>");
        });
      }

    }
});
});
</script>
<?php require_once USER_VIEW_PATH . 'frontouter.inc.php';?>