<?php
$pageName = 'home';
require_once USER_VIEW_PATH . 'header.inc.php';?>
<!-- Inner Banner :: Start -->
<section class="inner-banner">
    <div class="inner-content text-center">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <h1 class="page-title">Register</h1>
                </div>
                <div class="col-12 col-sm-12 col-md-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li class="active">Register</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inner Banner :: End -->
<!-- Inner Body :: Start -->
<section class="innerbody-section pt-50 pb-50">
    <div class="register-wizard-area pb-50">
        <div class="container">
            <div class="row">
                <div class="col-4">
                    <a href="javascript:void(0);" class="register-wizard-list">
                        <span class="register-wizard-icon"><i class="fas fa-check"></i><span class="register-wizard-num">1</span></span>
                        <span class="register-wizard-title">1. Check In</span>
                    </a>
                </div>
                <div class="col-4">
                    <a href="javascript:void(0);" class="register-wizard-list disabled">
                        <span class="register-wizard-icon"><i class="fas fa-check"></i><span class="register-wizard-num">2</span></span>
                        <span class="register-wizard-title">2. Sign Up</span>
                    </a>
                </div>
                <div class="col-4">
                    <a href="javascript:void(0);" class="register-wizard-list disabled">
                        <span class="register-wizard-icon"><i class="fas fa-check"></i><span class="register-wizard-num">3</span></span>
                        <span class="register-wizard-title">3. Verify</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6">
                <div class="signup-area">
                    <div class="section-title mb-30">
                        <h2 class="mb-10">Thanks for visiting us!</h2>
                        <h6 class="mb-30">We need to get you to the right place so please select from the following:</h6>
                    </div>
                    <ul class="signupinner-boxes">
                        <li class="">
                            <a href="addParent.php" class="register-box">
                                <div class="register-icon"><i class="fas fa-user"></i></div>
                                <div class="register-title">I am a Parent</div>
                            </a>
                        </li>
                        <li>
                            <a href="addProvider.php" class="register-box">
                                <div class="register-icon"><i class="fas fa-heart"></i></div>
                                <div class="register-title">I am Care Provider</div>
                            </a>
                        </li>
                        <li>
                            <a href="addNanny.php" class="register-box">
                                <div class="register-icon"><i class="fas fa-female"></i></div>
                                <div class="register-title">I am Nanny / Babysitter</div>
                            </a>
                        </li>
                        <!-- <li>
                            <a href="addNanny.php?type=babysitter" class="register-box">
                                <div class="register-icon"><i class="fas fa-child"></i></div>
                                <div class="register-title">I am Babysitter</div>
                            </a>
                        </li> -->
                    </ul>
                </div>
            </div>
            <div class="col-md-6">
                <div class="adv-banner pt-50">
                    <div class="owl-carousel owl-theme">
                        <div class="adv-box">
                            <div class="adv-image">
                                <img class="img-fluid" src="assets/images/adv-1.jpg" alt="Adv"/>
                            </div>
                            <div class="adv-content">
                                <p>Finding a brilliant babysitter</p>
                            </div>
                        </div>
                        <div class="adv-box">
                            <div class="adv-image">
                                <img class="img-fluid" src="assets/images/adv-2.jpg" alt="Adv"/>
                            </div>
                            <div class="adv-content">
                                <p>Securing high quality childcare</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inner Body :: End -->
<?php require_once USER_VIEW_PATH . 'footer.inc.php';?>
<?php require_once USER_VIEW_PATH . 'frontouter.inc.php';?>