<?php
$pageName = 'home';
require_once USER_VIEW_PATH . 'header.inc.php';?>
<!-- Inner Banner :: Start -->
<section class="inner-banner">
    <div class="inner-content text-center">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <h1 class="page-title">About Us</h1>
                </div>
                <div class="col-12 col-sm-12 col-md-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li class="active">About Us</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inner Banner :: End -->
<!-- Inner Body :: Start -->
<section class="innerbody-section pt-50 pb-50">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-lg-5 col-md-12 mb-md-30">
                <img class="img-fluid" src="assets/images/about-us.jpg" alt="About Us"/>
            </div>
            <div class="col-xl-8 col-lg-7 col-md-12">
                <div class="section-title mb-30">
                    <h2 class="mb-10">About Us</h2>
                    <h6 class="mb-30">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h6>
                </div>
                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
            </div>
        </div>
    </div>
</section>
<section class="innerbody-section pt-50 pb-50 bg-grey">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-lg-5 col-md-12 order-lg-2 order-xl-2 mb-md-30">
                <img class="img-fluid" src="assets/images/about-us.jpg" alt="About Us"/>
            </div>
            <div class="col-xl-8 col-lg-7 col-md-12 order-lg-1 order-xl-1">
                <div class="section-title mb-30">
                    <h2 class="mb-10">The Corporate Program</h2>
                    <h6 class="mb-30">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h6>
                </div>
                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
            </div>
        </div>
    </div>
</section>
<!-- Inner Body :: End -->
<?php require_once USER_VIEW_PATH . 'footer.inc.php';?>
<?php require_once USER_VIEW_PATH . 'frontouter.inc.php';?>