 <?php
$pageName = 'home';
require_once USER_VIEW_PATH . 'header.inc.php';?>
<!-- Inner Banner :: Start -->
<section class="inner-banner">
    <div class="inner-content">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="profileimg-area">
                        <img class="img-fluid" src="upload_images/<?php echo $nannyData[0]->photo; ?>" alt="Avtar" />
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="myaccount-name">
                        <h1>Hello, <?php echo ucwords($nannyData[0]->fullname); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inner Banner :: End -->
<!-- Inner Body :: Start -->
<section class="myaccount-section">
    <div class="myaccount-menu">
        <div class="container scroll-container">
            <ul class="myaccount-nav scroll-nav nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="dashboard-tab" data-toggle="tab" href="#dashboard" role="tab">Dashboard</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="updateprofile-tab" data-toggle="tab" href="#updateprofile" role="tab">Update Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="jobdetails-tab" data-toggle="tab" href="#jobdetails" role="tab">Job Details</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="modal" data-target="#ChangePasswordModal">Change Password</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="modal" data-target="#DeleteProfileModal">Delete Profile</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="dashboard" aria-labelledby="dashboard-tab">
                        <div class="myaccount-area pt-50 pb-50">
                        <?php if ($_SESSION['add_msg'] == 'success') {?>
                        <div class="alert alert-success alert-dismissible" id="success-alert">
                          <strong>Success!</strong> Data Submitted successfully.
                        </div>
                        <?php }?>
                        <?php if ($_SESSION['add_msg'] == 'error') {?>
                        <div class="alert alert-danger alert-dismissible" id="success-alert">
                          <strong>Failed!</strong> Somthing goes wrong.Please try again.
                        </div>
                        <?php }?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="section-title mb-3">
                                        <h2>My Booking</h2>
                                        <div class="table-responsive">
                                            <table class="table table-striped-odd table-bordered mb-0 text-nowrap DataTables">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center no-sort">#</th>
                                                        <th class="text-center no-sort">Contact Name</th>
                                                        <th>Email</th>
                                                        <th>Phone</th>
                                                        <th>Requested Date</th>
                                                        <th>Comments</th>
                                                        <th class="text-center">Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
$bookNannyi = 1;
foreach ($bookingdata as $bookdata) {?>


                                                        <tr>
                                                            <td class="text-center"><?php echo $bookNannyi++; ?></td>
                                                            <td><?php echo $bookdata->contact_name; ?></td>
                                                            <td><?php echo $bookdata->contact_email; ?></td>
                                                            <td><?php echo $bookdata->contact_phone; ?></td>
                                                            <td><?php echo $bookdata->from_date; ?></td>
                                                            <td><?php echo $bookdata->comment; ?></td>
                                                            <td class="text-center align-middle">
                                                                <?php
if ($bookdata->booking_status == 'accept') {?>
    <span class="btn btn-info btn-sm">Accepted</i></span>
                        <?php } elseif ($bookdata->booking_status == 'decline') {?>
<span class="btn btn-warning btn-sm">Rejected</i></span>
                        <?php } else {?>
<a href="#" class="btn btn-success btn-sm acceptdata" id="<?php echo $bookdata->book_nanny_id; ?>">Accept</i></a>
<a href="#" class="btn btn-danger btn-sm declinedata" id="<?php echo $bookdata->book_nanny_id; ?>">Decline</i></a>
                        <?php }?>
                                                            </td>
                                                        </tr>
                                                        <?php }
?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="package-subscribe">
                                    <div class="package-title">
                                        <h2>Premium<?php if($subscriptions): ?> <?php echo ' - '. $subscriptions[0]->plan_interval; ?><?php endif; ?></h2>
                                        <h3><sup><?php echo $subscriptions[0]->plan_amount_currency; ?></sup><?php echo $subscriptions[0]->plan_amount; ?></h3>
                                    </div>
                                    <div class="package-purchasedate">
                                        <?php if($subscriptions): ?>
                                        <span class="package-date-title">Purchase Date</span>
                                        <span class="package-date-cal"><?php echo date("F j, Y", strtotime($subscriptions[0]->created)) ; ?></span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="package-expiredate">
                                        <?php if($subscriptions): ?>
                                        <span class="package-date-title">Expire Date</span>
                                        <span class="package-date-cal"><?php echo date("F j, Y", strtotime($subscriptions[0]->enddate)) ; ?></span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="package-action">
                                        <?php if($subscriptions): ?>
                                        <a href="#" class="btn btn-primary">Renew Plan</a>
                                        <?php else: ?>
                                        <a class="btn btn-primary" data-toggle="modal" data-target="#subPlan">Subscribe Plan</a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="updateprofile" aria-labelledby="updateprofile-tab">
                        <div class="myaccount-area pt-50 pb-50">
                            <section>
                                <form method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="nanny_id" value="<?php echo $nannyData[0]->nanny_id; ?>">
                                    <input type="hidden" name="get_photo" value="<?php echo $nannyData[0]->photo; ?>">
                                <fieldset class="mb-20">
                                    <h2 class="form-title">Tell us about your self?</h2>
                                    <div class="row">
                                        <div class="col-md-2 form-group">
                                            <label class="bold-label">Profile Avatar <i class="required">*</i></label>
                                            <div class="uploadimage-block">
                                                <div class="col-md-12 form-group">
                                                    <input type="file" name="photo" class="btn btn-primary btn-md" accept="image/*">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <label>Your Full Name<i class="required">*</i></label>
                                            <input type="text" class="form-control" name="fullname" placeholder="e.g : Akash Sathvara" required="" value="<?php echo $nannyData[0]->fullname; ?>">
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label>Short Description<i class="required">*</i></label>
                                            <textarea class="form-control" name="description" required=""><?php echo $nannyData[0]->description; ?></textarea>
                                        </div>

                                        <div class="col-md-12 form-group">
                                            <label>About your self</label>
                                            <textarea name="about" class="form-control summernote" required><?php echo $nannyData[0]->about; ?></textarea>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label>Facebook Link</label>
                                            <input type="text" class="form-control" name="website" placeholder="Facebook Link" value="<?php echo $nannyData[0]->website; ?>">
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="mb-20">
                                    <h2 class="form-title">Your Classification</h2>
                                    <div class="row">
                                        <!--<div class="col-md-12 form-group">-->
                                        <!--    <label>Classification</label>-->
                                        <!--    <input type="text" class="form-control" name="classification" value="<?php echo $nannyData[0]->classification; ?>">-->
                                        <!--</div>-->
                                        <div class="col-md-6 form-group">
                                            <label>Graduation</label>
                                            <input type="text" class="form-control" name="graduation" placeholder="B.com / Master in .." value="<?php echo $nannyData[0]->graduation; ?>">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Institute Name</label>
                                            <input type="text" class="form-control" name="institute" value="<?php echo $nannyData[0]->institute; ?>">
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="mb-20">
                                    <h2 class="form-title">Contact Details</h2>
                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <label>Email Address<i class="required">*</i></label>
                                            <input type="email" class="form-control" name="email_id" placeholder="Email Address" required="" value="<?php echo $nannyData[0]->email_id; ?>">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Phone Number<i class="required">*</i></label>
                                            <input type="text" class="form-control" name="contact" placeholder="Phone Number" required="" value="<?php echo $nannyData[0]->contact; ?>">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>First Name<i class="required">*</i></label>
                                            <input type="text" class="form-control" name="fname" placeholder="First Name" required="" value="<?php echo $nannyData[0]->fname; ?>">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>last Name<i class="required">*</i></label>
                                            <input type="text" class="form-control" name="lname" placeholder="Last Name" required="" value="<?php echo $nannyData[0]->lname; ?>">
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label>Address<i class="required">*</i></label>
                                            <input type="text" class="form-control" name="address" placeholder="Address" id="address" onFocus="geolocate();" required="" value="<?php echo $nannyData[0]->address; ?>">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Suburb<i class="required">*</i></label>
                                            <input type="text" id="suburb" class="form-control" name="suburb" placeholder="Suburb" required="" value="<?php echo $nannyData[0]->suburb; ?>">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Postcode<i class="required">*</i></label>
                                            <input type="text" id="postcode" class="form-control" name="postcode" placeholder="Postcode" required="" value="<?php echo $nannyData[0]->postcode; ?>">
                                            <div id="suggesstion-box"></div>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>City<i class="required">*</i></label>
                                            <input type="text" id="city" class="form-control" name="city" placeholder="City" required="" value="<?php echo $nannyData[0]->city; ?>">
                                            <div id="suggesstion-box1"></div>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Country<i class="required">*</i></label>
                                            <select class="form-control" required="" name="country">
                                                <option value="">-- Select Country --</option>
                                                <option value="CA" <?php if ($nannyData[0]->country == 'CA') {echo "selected";}?>>Canada</option>
                                                <option value="US" <?php if ($nannyData[0]->country == 'US') {echo "selected";}?>>US</option>
                                            </select>
                                        </div>
                                    </div>
                                </fieldset>
                                <button type="submit" name="nanny_profile_update" class="btn btn-primary">Update Profile</button>
                            </form>
                            </section>

                        </div>
                    </div>
                    <div class="tab-pane fade" id="jobdetails" aria-labelledby="jobdetails-tab">
                        <div class="myaccount-area pt-50 pb-50">
                            <form method="post">
                                <input type="hidden" name="job_nanny_id" value="<?php echo $nannyData[0]->nanny_id; ?>">
                            <fieldset class="mb-20">
                                <h2 class="form-title">Profile details</h2>
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <label>Your Availablity</label>
                                        <div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="CasualRadio" name="availablity" class="custom-control-input" value="casual" <?php if ($nannyData[0]->availablity == 'casual') {echo "checked";}?>>
                                                <label class="custom-control-label" for="CasualRadio">Casual</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="ContractRadio" name="availablity" class="custom-control-input" value="contract" <?php if ($nannyData[0]->availablity == 'contract') {echo "checked";}?>>
                                                <label class="custom-control-label" for="ContractRadio">Contract</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="FullTimeRadio" name="availablity" class="custom-control-input" value="fulltime" <?php if ($nannyData[0]->availablity == 'fulltime') {echo "checked";}?>>
                                                <label class="custom-control-label" for="FullTimeRadio">Full Time</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="PartTimeRadio" name="availablity" class="custom-control-input" value="parttime" <?php if ($nannyData[0]->availablity == 'parttime') {echo "checked";}?>>
                                                <label class="custom-control-label" for="PartTimeRadio">Part Time</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="TemporaryRadio" name="availablity" class="custom-control-input" value="temporary" <?php if ($nannyData[0]->availablity == 'temporary') {echo "checked";}?>>
                                                <label class="custom-control-label" for="TemporaryRadio">Temporary</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <h2 class="form-title">What are the benefits & salary on offer?</h2>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label>Salary</label>
                                        <input type="text" class="form-control" name="salary" placeholder="Salary" value="<?php echo $nannyData[0]->salary; ?>">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label>Select Type</label>
                                        <select class="form-control" name="salary_type">
                                            <option value="Hours" <?php if ($nannyData[0]->salary_type == 'Hours') {echo "selected";}?>>Hours</option>
                                            <option value="Weekly" <?php if ($nannyData[0]->salary_type == 'Weekly') {echo "selected";}?>>Weekly</option>
                                            <option value="Monthly" <?php if ($nannyData[0]->salary_type == 'Monthly') {echo "selected";}?>>Monthly</option>
                                        </select>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Selected Services</label>
                                        <div class="row">
                                        <?php
foreach ($benefitData as $benefit) {
	?>
                                            <div class="col-md-6">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="ben<?php echo $benefit->service_id; ?>" name="services[]" value="<?php echo $benefit->service_id; ?>" <?php
$dataArr = explode(",", $nannyData[0]->services);
	foreach ($dataArr as $arr) {
		if ($benefit->service_id == $arr) {echo "checked";}
	}
	?>>
                                                    <label class="custom-control-label" for="ben<?php echo $benefit->service_id; ?>"><i class="fas fa-briefcase-medical pr-1"></i><?php echo $benefit->service_name; ?></label>
                                                </div>
                                            </div>
                                        <?php }?>


                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group">
                                    <label>Interests</label>
                                   <input type="hidden" class="variantFieldTags" name="skills" value="<?php echo $nannyData[0]->skills; ?>">
                                </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <h2 class="form-title">Where is the job located?</h2>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label>Location<i class="required">*</i></label>
                                        <!-- <select class="form-control" name="job_location">
                                                <option value="">Select City</option>
                                                <?php
foreach ($cityData as $city) {?>
                                                <option value="<?php echo $city->city; ?>" <?php if ($nannyData[0]->job_location == $city->city) {echo "selected";}?>><?php echo $city->city; ?></option>
                                                <?php }?>


                                            </select> -->
                                            <select name="job_location" class="form-control" required="">
                                                <option value="">-- Select Country --</option>
                                                <option value="Canada" <?php if ($nannyData[0]->job_location == 'Canada') {echo "selected";}?>>Canada</option>
                                                <option value="US" <?php if ($nannyData[0]->job_location == 'US') {echo "selected";}?>>US</option>
                                            </select>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label>Suburb/Town<i class="required">*</i></label>
                                        <input type="text" class="form-control" name="job_suburb" value="<?php echo $nannyData[0]->job_suburb; ?>" placeholder="Suburb/Town" required="">
                                    </div>
                                </div>
                            </fieldset>
                            <button type="submit" name="updateJobdetail" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inner Body :: End -->
<?php require_once USER_VIEW_PATH . 'footer-dashboard.inc.php';?>
<!-- Delete Profile :: Start -->
<div class="modal custom-modal fade" id="DeleteProfileModal" tabindex="-1" role="dialog" aria-labelledby="DeleteProfileModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered sm-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="DeleteProfileModalLabel">Delete Profile</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
            </div>
            <div class="modal-body p-4">
            <form method="post" id="nannyRemove">
                <h5>SORRY TO SEE YOU GO...</h5>
                <p>Are you sure you want to delete your account?</p>
                <input type="hidden" name="id" value="<?php echo $nannyData[0]->nanny_id; ?>">
                <input type="hidden" name="user_type" value="nanny">
                <input type="hidden" name="action" value="submituserdata">
                <button type="submit" class="btn btn-primary">Yes</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </form>
        </div>
        </div>
    </div>
</div>
<!-- Delete Profile :: End -->

<div class="modal custom-modal fade" id="subPlan" tabindex="-1" role="dialog" aria-labelledby="subPlanLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered sm-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="subPlanLabel">Add Subcriptions</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
            </div>
            <div class="modal-body p-4">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" id="subscribePlan">
                        <div class="col-md-12 form-group">
                            <label>Select Premium Plan</label>
                            <select class="form-control" name="plan" id="plan">
                                <option value="">Select Plan</option>
                                <option value="0">$<?php echo PRONAN_MONTH_PRICE; ?> / Month</option>
                                <option value="1">$<?php echo PRONAN_YEAR_PRICE; ?> / Year</option>
                            </select>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>Card Number</label>
                            <input type="text" class="form-control" id="card_number" name="card_number" placeholder="Card Number" required />
                            <div id="card-suggesstion-box"></div>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>Expiry Month / Year</label>
                            <input type="text" class="form-control" id="card_exp" name="card_exp" placeholder="Month / Year" required />
                        </div>
                        <div class="col-md-12 form-group">
                            <label>CVC Code</label>
                            <input type="text" class="form-control" id="card_cvc" name="card_cvc" placeholder="CVC Code" required />
                        </div> 

                        <div class="col-md-12 form-group">
                            <label>Card Holder Name</label>
                            <input type="text" class="form-control" id="c_name" name="c_name" placeholder="Card Holder Name" required />
                        </div>

                        <div class="col-md-12 form-group">
                            <label>Postal Code</label>
                            <input type="text" class="form-control" id="post_code" name="post_code" placeholder="Postal Code" required />
                        </div>  
                        
                        <input type="hidden" name="subscribe_plan" id="subscribe_plan" value="subscribe_plan">
                        <div class="col-md-12 form-group">
                            <input type="submit" class="btn btn-primary" id="updatePlan" name="updatePlan" value="Subscribe" />
                        </div> 
                    </form>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>

<!-- Change Password :: Start -->
<div class="modal custom-modal fade " id="ChangePasswordModal" tabindex="-1" role="dialog" aria-labelledby="ChangePasswordModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered sm-modal" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="ChangePasswordModalLabel">Change Password</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
        </div>
        <div class="modal-body p-4">
            <p>Please complete the form below to change your <b>DayCare.com</b> Password.</p>
            <form method="post" id="nannyChangepassword">
                <div class="form-group">
                    <label>Current Password</label>
                    <input type="password" class="form-control" name="currentpassword" placeholder="Current Password" required="">
                </div>
                <div class="form-group">
                    <label>New Password</label>
                    <input type="password" id="password" class="form-control" name="newpassword" placeholder="New Password" required="">
                </div>
                <div class="form-group">
                    <label>Confirm Password</label>
                    <input type="password" class="form-control" name="confirmpassword" id="confirmpassword" placeholder="Confirm Password" required="">
                </div>

                <input type="hidden" name="id" value="<?php echo $nannyData[0]->nanny_id; ?>">
                <input type="hidden" name="user_type" value="nanny">
                <input type="hidden" name="action" value="userpasswordchange">
                <button type="submit" class="btn btn-primary">Change</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <div style="color: red;" id="divCheckPasswordMatch">
            </form>

        </div>
    </div>
</div>
</div>
<!-- Change Password :: End -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw==" crossorigin="anonymous"></script>
<script>

	$(document).on('submit', '#subscribePlan', function(event){
        event.preventDefault();
        /*$(document).on('click', '#updatePlan', function (event) {*/
        var postData = {
                plan: $("#plan").val(),
                card_number: $("#card_number").val(),
                card_exp: $("#card_exp").val(),
                card_exp: $("#card_exp").val(),
                card_cvc: $("#card_cvc").val(),
                c_name: $("#c_name").val(),
                post_code: $("#post_code").val(),
                nanny_id: '<?php echo $nannyData[0]->nanny_id; ?>',
                subscribe_plan: $("#subscribe_plan").val(),
            };
        $.ajax({
            type: "POST",
            url: "ajax/validCard.php",
            data:postData,
            success: function(data){
                var res = $.parseJSON(data);
                $("#subPlan").modal('hide');
                if(res.success == 0){
                    Swal.fire(
                        'Failed!',
                        res.token,
                        'error'
                    ).then((result) => {
                      location.reload();
                    });
                }
                if(res.success == 1){
                    Swal.fire(
                        'Success!',
                        res.token,
                        'success'
                    ).then((result) => {
                      location.reload();
                    });
                }
            }
        });
        /*});*/
    });

    $(function () {
            $('.variantFieldTags').tagit({
                singleField: true,
                placeholderText: "e.g. Reading , Humble with baby"
            });
        });
function checkPasswordMatch() {
    var password = $("#password").val();
    var confirmPassword = $("#confirmpassword").val();
    if (password != confirmPassword){
        $("#divCheckPasswordMatch").text("Passwords do not match!");
    }
    else{
        $("#divCheckPasswordMatch").text("Passwords match.");
    }
}
</script>
<script>
    $(document).ready(function () {
    	$('#card_number').mask('0000-0000-0000-0000');
        $('#card_exp').mask('00/00');
       	$("#confirmpassword").keyup(checkPasswordMatch);
    });
</script>
<script>
$(document).on('submit', '#nannyRemove', function(event){
event.preventDefault();
$.ajax({
    url:"ajax/deactiveAccount.php",
    method:'POST',
    data:new FormData(this),
    contentType:false,
    processData:false,
    success:function(data)
    {
      $('#nannyRemove')[0].reset();
      if(data == 'success-nanny'){
        window.location.replace("index.php");
      }else{
        Swal.fire(
          'Failed!','Somthing goes wrong.','error'
        ).then((result) => {
          // Reload the Page
        });
      }

    }
});
});
</script>
<script type="text/javascript">
    $(document).ready(function () {
        var countfieldbox = 1;
        $("#addmoreblock").bind("click", function () {
            var div = $("<div />");
            div.html(GetDynamicTextBox(countfieldbox++));
            $("#NewContainerBox").append(div);
        });
        $("#btnGet").bind("click", function () {
            var values = "";
            $("input[name=DynamicTextBox]").each(function () {
                values += $(this).val() + "\n";
            });
            alert(values);
        });
        $("body").on("click", "#removeContainerBox", function () {
            $(this).closest(".new-row-add").remove();
        });
        function GetDynamicTextBox(value) {
            return '<div class="new-row-add"><div class="children-form"><h5 class="children-header">Child <span class="child-index">' + value + '</span></h5><div class="children-body row"><div class="col-md-5 form-group"><label>Childs Name</label><input type="text" class="form-control" placeholder="Childs Name"></div><div class="col-md-5 form-group"><label>Date of birth (child is not born yet please indicate due date)</label><input type="text" class="form-control" placeholder="Childs Name"></div><div class="col-md-2 form-group"><label>&nbsp;</label><button type="button" class="btn btn-danger btn-sm btn-rounded" id="removeContainerBox"><i class="fa fa-close"></i> Remove</button></div></div></div></div>'
        }
    });

    $("#datetimepickerdob").datetimepicker({
        format: 'L',
        maxDate: new Date()
    });
</script>
<script>
$(document).on('submit', '#nannyChangepassword', function(event){
event.preventDefault();
$.ajax({
    url:"ajax/userPasswordupdate.php",
    method:'POST',
    data:new FormData(this),
    contentType:false,
    processData:false,
    success:function(data)
    {
      $('#nannyChangepassword')[0].reset();
      $('#ChangePasswordModal').modal('hide');
      if(data == 'success-nanny'){
        Swal.fire(
            'Success!',
            'Your Password has been changed.',
            'success'
        )
      }else{
        Swal.fire(
          'Failed!','Somthing goes wrong.','error'
        ).then((result) => {
          // Reload the Page
          //window.location.replace("<?php echo $_SERVER['PHP_SELF']; ?>");
        });
      }

    }
});
});
</script>
<script type="text/javascript">
    $(document).on('click', '.acceptdata', function(){
        var book_nanny_id = $(this).attr("id");
        var status = 'accept';
        //alert(user_id);
        Swal.fire({
            title: 'Are you sure?',
            text: "Once Accepted, you will not be able to recover this user data!",
            icon: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Yes, Change it!'
        }).then((result) => {
        if (result.value) {
            $.ajax({
                url:"ajax/bookRequestdata.php",
                method:"POST",
                data:{book_nanny_id:book_nanny_id,status:status,action:'bookingreqaccept'},
                success:function(data)
                {
                  window.location.replace("account-Nanny.php");
                }
            });
        }
    })
});
</script>
<script type="text/javascript">
    $(document).on('click', '.declinedata', function(){
        var book_nanny_id = $(this).attr("id");
        var status = 'decline';
        //alert(user_id);
        Swal.fire({
            title: 'Are you sure?',
            text: "Once Accepted, you will not be able to recover this user data!",
            icon: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Yes, Change it!'
        }).then((result) => {
        if (result.value) {
            $.ajax({
                url:"ajax/bookRequestdata.php",
                method:"POST",
                data:{book_nanny_id:book_nanny_id,status:status,action:'bookingreqaccept'},
                success:function(data)
                {
                  window.location.replace("account-Nanny.php");
                }
            });
        }
    })
});
</script>
<script>
    $("#success-alert").fadeTo(2000, 900).slideUp(500, function(){
    $("#success-alert").slideUp(900);
    <?php $_SESSION['add_msg'] = '';?>
});
</script>
<script>
// AJAX call for autocomplete
$(document).ready(function(){
    $("#search-box").keyup(function(){
        $.ajax({
        type: "POST",
        url: "ajax/readPostcode.php",
        data:'keyword='+$(this).val(),
        beforeSend: function(){
            $("#search-box").css("background","#FFF url(assets/images/LoaderIcon.gif) no-repeat 165px");
        },
        success: function(data){
            $("#suggesstion-box").show();
            $("#suggesstion-box").html(data);
            $("#search-box").css("background","#FFF");
        }
        });
    });
});
//To select country name
function selectCountry(val) {
    $("#search-box").val(val);
    $("#suggesstion-box").hide();
}
</script>
<script>
// AJAX call for autocomplete
$(document).ready(function(){
    $("#search-box1").keyup(function(){
        $.ajax({
        type: "POST",
        url: "ajax/readCity.php",
        data:'keyword='+$(this).val(),
        beforeSend: function(){
            $("#search-box1").css("background","#FFF url(assets/images/LoaderIcon.gif) no-repeat 165px");
        },
        success: function(data){
            $("#suggesstion-box1").show();
            $("#suggesstion-box1").html(data);
            $("#search-box1").css("background","#FFF");
        }
        });
    });
});
//To select country name
function selectCountry1(val) {
    $("#search-box1").val(val);
    $("#suggesstion-box1").hide();
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZPcppwWN179bx8Asxh8Xsd-ZiLIvZNMM&libraries=places"></script>

<script type="text/javascript">
    var placeSearch, autocomplete;

    function initialize() {
        autocomplete = new google.maps.places.Autocomplete(
        (document.getElementById('address')), {
            types: ['geocode']
        });
        google.maps.event.addDomListener(document.getElementById('address'), 'focus', geolocate);
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            console.log(place.address_components);
            var res = new Array();
            for(var k=0; k < place.address_components.length; k++){
                if(place.address_components[k].types.indexOf('postal_code') >= 0){
                    res['postal_code'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('locality') >= 0){
                    res['suberb'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('sublocality_level_2') >= 0){
                    res['address_line1'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('sublocality_level_1') >= 0){
                    res['address_line2'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('administrative_area_level_1') >= 0){
                    res['state'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('administrative_area_level_2') >= 0){
                    res['city'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('country') >= 0){
                    res['country'] = place.address_components[k].short_name;
                }
            }
            $("#postcode").val(res['postal_code']);
            $("#suburb").val(res['suberb']);
            $("#city").val(res['city']);
            $("#country").val(res['country']);
        });
    }

    function geolocate() {

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                
                $("#lat").val(position.coords.latitude);
                $("#long").val(position.coords.longitude);

                var geolocation = new google.maps.LatLng(
                position.coords.latitude, position.coords.longitude);
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }

    window.addEventListener('load', (event) => {
        initialize();
    });
</script>
<?php require_once USER_VIEW_PATH . 'frontouter.inc.php';?>