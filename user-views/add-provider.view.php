<?php
$pageName = 'home';
require_once USER_VIEW_PATH . 'header.inc.php';
?>
<!-- Inner Banner :: Start -->
<section class="inner-banner">
    <div class="inner-content text-center">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <h1 class="page-title">Register</h1>
                </div>
                <div class="col-12 col-sm-12 col-md-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li class="active">Register</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inner Banner :: End -->
<!-- Inner Body :: Start -->
<section class="innerbody-section pt-50 pb-50">
    <div class="container">
        <div class="signup-area">
            <div class="signup-form">
                <form id="signup-form" method="post">
                	<input type="hidden" name="lat" value="" id="lat">
        			<input type="hidden" name="long" value="" id="long">
                    <h3>Service Details</h3>
                    <section>
                        <fieldset class="mb-20">
                    		<h2 class="form-title">Name & Type of service</h2>
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label>Name of Service<i class="required">*</i></label>
                                    <input type="text" class="form-control" name="provider_name" placeholder="Name of Service" required="">
                                </div>
                                <div class="col-md-12 form-group">
                                    <div class="checkbox-list">
                                        <ul>
                                        <?php foreach ($serviceData as $service) {?>
											<li>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="longdaycarefees<?php echo $service->provider_service_id; ?>" name="provider_service_id[]" value="<?php echo $service->provider_service_id; ?>" />
                                                    <label class="custom-control-label" for="longdaycarefees<?php echo $service->provider_service_id; ?>"><?php echo $service->title; ?></label>
                                                </div>
                                            </li>
                                        <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="mb-20">
                            <h2 class="form-title">Contact Details</h2>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label>Contact Name<i class="required">*</i></label>
                                    <input type="text" class="form-control" name="contact_name" placeholder="Contact Name" required="">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Email Address<i class="required">*</i></label>
                                    <input type="email" class="form-control" name="provider_email" placeholder="Email Address" required="">
                                    <small class="form-text text-muted">Please nominate the primary email address that you can be contacted on at all times</small>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Address<i class="required">*</i></label>
                                    <input type="text" class="form-control" id="address" onFocus="geolocate();" name="address" placeholder="Enter a full address" required="">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Town/Suburb<i class="required">*</i></label>
                                    <input type="text" class="form-control" id="superb" name="suburb" placeholder="Town/Suburb" required="">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Country<i class="required">*</i></label>
                                    <select class="form-control" id="country" name="country">
                                        <option value="selectstate">Select Country</option>
                                        <option value="CA">Canada</option>
                                        <option value="US">US</option>
                                        <option value="IN">India</option>
                                    </select>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>City<i class="required">*</i></label>
                                    <input type="text" id="search-box1" class="form-control" name="city" placeholder="City" required="">
                                    <div id="suggesstion-box1"></div>
                                </div>
                                <div  class="col-md-6 form-group">
                                    <label>Postcode<i class="required">*</i></label>
                                    <div class="input-group">
                                        <input type="text" id="postcode"  class="form-control" name="postcode" autocomplete="off" placeholder="Postcode" required="">
                                        <div class="input-group-append">
                                            <a class="btn btn-primary btn-gup" onclick="getLocation();"><i class="fas fa-map-marker-alt pr-2"></i> Locate Me</a>
                                        </div>
                                    </div>
                                    <div id="suggesstion-box"></div>
                                </div>
                                
                                <div class="col-md-6 form-group">
                                    <label>Phone Number<i class="required">*</i></label>
                                    <input type="text" class="form-control" name="phone" placeholder="Phone Number" required="">
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="mb-20">
                            <h2 class="form-title">Vacancies - Permanent</h2>
                            <div class="form-row d-flex align-items-center vacancies-box mb-20">
                                <div class="col-auto padt-3 pr-2">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="is_vacancy" class="custom-control-input" id="novacancies" value="No" onchange="valueChanged()">
                                        <label class="custom-control-label" for="novacancies">No Vacancies</label>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <span class="pr-2">- Waiting List Period</span>
                                    <select class="form-control inline-form w-70">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                    <select class="form-control inline-form w-110">
                                        <option value="select">---</option>
                                        <option value="week">Week(s)</option>
                                        <option value="month">Month(s)</option>
                                        <option value="year">Year(s)</option>
                                    </select>
                                </div>
                                <div class="col-auto pl-3">
                                    <a href="javascript:void(0);" class="moonselect-area pr-2">
                                        <input type="checkbox" class="MoonOvernightmode" id="MoonOvernightCare" />
                                        <label for="MoonOvernightCare" data-toggle="tooltip" data-placement="top" title="Weekend care available">
                                            <i class="icon-qualifications far fa-moon"></i>
                                        </label>
                                    </a>
                                    <span>Weekend care available</span>
                                </div>
                            </div>
                            <div class="row" id="vacancyinfo">
                                <div class="col-md-12">
                                    <div class="add-additional-box mb-3">
                                        <a href="javascript:void(0)">
                                            <div class="add-additional-icon"><i class="far fa-plus-square"></i></div>
                                            <div class="add-additional-title" id="btnAdd">Add Vacancy Information</div>
                                        </a>
                                    </div>
                                    <div class="agegroup-area">
                                        <div class="no-more-tables">
                                            <table class="table table-bordered table-alignmiddle">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">#</th>
                                                        <th>Age Group</th>
                                                        <th>Days</th>
                                                        <th class="text-center">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="TextBoxContainer"></tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="hint mb-2">
                                    	Click on the blue icon to add a row to remove a row click on the red icon. Ideally you should have a row for each age group you service (e.g., 0-2 years, 2-3 years, 3-5 years).
                                    </div>
                                    <div class="hint mb-4">
                                    	<b>IMPORTANT:</b> you should enter the age groups you service whether you have vacancies for those age groups or not. Please only include vacancies that are available now not for wait listing. Purple means you have a vacancy.
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="mb-20">
                            <h2 class="form-title">The Vacancies Posted Are Available</h2>
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="VacanciesPostNow" name="vacancy_start_on" value="Now" class="custom-control-input">
                                            <label class="custom-control-label" for="VacanciesPostNow">Now</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="VacanciesPostSelectDate" name="vacancy_start_on" class="custom-control-input">
                                            <label class="custom-control-label" for="VacanciesPostSelectDate">MM/YYYY</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6" id="VacanciesPostSelectDate-box">
                                    <div class="form-group">
                                        <label>Select Month & Year</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="VacanciesPostedDatePicker" name="vacancy_start_value" data-toggle="datetimepicker" data-target="#VacanciesPostedDatePicker"/>
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i class="fas fa-calendar-alt"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="mb-20">
                            <!-- <h2 class="form-title">Occasional, Casual or Flexible Care</h2> -->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>For more info about your web page or app link</label>
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <div class="input-group-text">http://</div>
                                            </div>
                                            <input type="text" class="form-control" name="web_app_link">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="mb-20">
                            <h2 class="form-title mb-0">Waitlisting</h2>
                            <p>Our waitlisting service allows families to conveniently waitlist with you and best of all its free or you can use your own waitlist form.</p>
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label>Add waitlist</label>
                                    <div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="addwaitlistyes" name="add_waitlist" value="Yes" class="custom-control-input" checked="">
                                            <label class="custom-control-label" for="addwaitlistyes">Yes please</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="addwaitlistno" name="add_waitlist" value="No" class="custom-control-input">
                                            <label class="custom-control-label" for="addwaitlistno">No thanks</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="addwaitlistown" name="add_waitlist" value="Own" class="custom-control-input">
                                            <label class="custom-control-label" for="addwaitlistown">Add my own waitlist link</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" id="waitlistdiv">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Waitlist Link (external)</label>
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <div class="input-group-text">http://</div>
                                                    </div>
                                                    <input type="text" class="form-control" name="waitlist_link" placeholder="Waitlist Link">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Waitlist Fee</label>
                                                <input type="text" class="form-control" name="waitlist_fee" placeholder="Waitlist Fee">
                                                <small class="form-text text-muted">$ Numeric Value</small>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-12 form-group">
                                                    <label>Waitlist Policy</label>
                                                    <textarea class="form-control" name="waitlist_policy" rows="5"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="mb-20">
                            <h2 class="form-title">Benefits</h2>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="firstaid" name="benifits[]">
                                        <label class="custom-control-label" for="firstaid"><i class="fas fa-briefcase-medical pr-1"></i>First Aid/CPR Training</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="firstaid1">
                                        <label class="custom-control-label" for="firstaid1"><i class="fas fa-clock pr-1"></i>Flexible work hours</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="employment">
                                        <label class="custom-control-label" for="employment"><i class="fas fa-plane pr-1"></i>Opportunity for employment overseas</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="professional">
                                        <label class="custom-control-label" for="professional"><i class="fas fa-book pr-1"></i>Professional development/education/training</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="superannuation">
                                        <label class="custom-control-label" for="superannuation"><i class="fas fa-coins pr-1"></i>Superannuation</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="uniforms">
                                        <label class="custom-control-label" for="uniforms"><i class="fas fa-universal-access pr-1"></i>Uniforms supplied</label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </section>
                    <h3>Select The Plan</h3>
                    <section>
                        <fieldset>
                            <div class="section-title text-center mb-30">
                                <h2 class="mb-10">Choose the best plan for your early childhood services</h2>
                                <h6 class="mb-30"><b>Need help?</b> Click here to <a href="contact-us.php" class="link">contact us</a> for more information</h6>
                            </div>
                            <div class="priceing-table pt-50 pb-50">
                                <!-- <div class="row">
                                    <div class="col-md-4 offset-md-4">
                                        <div class="selectplan-box">
                                            <label>Select Your Plan</label>
                                            <select class="form-control">
                                                <option value="0">-- Select Plan --</option>
                                                <option value="1">1 Month</option>
                                                <option value="2">6 Months</option>
                                                <option value="3">12 Months</option>
                                            </select>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="row">
                                    <div class="col-lg-2 col-md-2"></div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="price-block price-clr1">
                                            <div class="price-block-header">
                                                <h4>Basic</h4>
                                                <h3><sup>$</sup>0.00</h3>
                                                <h5>Free</h5>
                                            </div>
                                            <div class="price-block-body">
                                                <div class="price-description">This <b>free</b> community service enables you to be seen by families searching for child care</div>
                                                <div class="price-list">
                                                    <ul>
                                                        <li>Lists your service name and basic contact details</li>
                                                        <li>Details your fee and vacancy information: a red map pin indicates current vacancies and location</li>
                                                        <li>Parents can contact you via email</li>
                                                        <li>Parents attending your service can post a rating and review</li>
                                                        <li>Parents can conveniently waitlist with you</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="price-block-foot">
                                                <input type="radio" id="selectbasicplan" name="subscription" class="custom-control-input" value="free">
                                                <label class="btn btn-primary pricebtn-clr1" for="selectbasicplan">Select Basic</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="price-block price-clr2">
                                            <div class="price-block-header">
                                                <h4>Premium</h4>
                                                <h3 id="sub_price"><sup>$</sup>15.00</h3>
                                                <h5 id="sub_year">Month</h5>
                                            </div>
                                            <div class="price-block-area">
                                                <h5>Select Your Plan</h5>
                                                <div class="selectpricearea">
                                                    <div class="form-check form-check-inline">
                                                      <input class="form-check-input" type="radio" name="price_id" id="PremiumRadio5" value="0">
                                                      <label class="form-check-label" for="PremiumRadio5">$15 Month</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                      <input class="form-check-input" type="radio" name="price_id" id="PremiumRadio50" value="1">
                                                      <label class="form-check-label" for="PremiumRadio50">$150 Year</label>
                                                    </div>
                                                </div>
                                                <!--<select class="form-control">-->
                                                <!--    <option value="10-1">1 Month</option>-->
                                                <!--    <option value="45-5">6 Months</option>-->
                                                <!--    <option value="100-12">12 Months</option>-->
                                                <!--</select>-->
                                            </div>
                                            <div class="price-block-body">
                                                <div class="price-description">Stand out from the crowd by highlighting the unique selling points of your service and make it easier for families to contact you. <b>Includes all the features of a basic listing plus:</b></div>
                                                <div class="price-list">
                                                    <ul>
                                                        <li>Priority ranking in search results</li>
                                                        <li>An enhanced listing displaying your logo</li>
                                                        <li><b>A detailed web page</b> where you can showcase inclusions, display photos, logo and link to your website</li>
                                                        <li>Advanced features including ‘request a tour’</li>
                                                        <li>Showcase your video content and social media by adding your Facebook page</li>
                                                        <li>Live Chat – speak to parents directly and in real time on your page</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="price-block-foot">
                                                <input type="radio" id="selectbasicpremium" name="subscription" class="custom-control-input" value="paid">
                                                <label class="btn btn-primary pricebtn-clr2" for="selectbasicpremium">Select Premium</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2"></div>
                                </div>
                            </div>
                        </fieldset>
                    </section>
                    <h3>Post</h3>
                    <!-- Inner Body :: Start -->
                    <section class="innerbody-section pt-50 pb-50">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 offset-md-2">
                                    <div class="section-title text-center mb-30">
                                        <h2 class="mb-0">ONE LAST Final Step</h2>
                                    </div>
                                    <!-- <div class="signupconfirm-area">
                                        <h6>Welcome, Care Provider</h6>
                                        <p>We’ve just sent you a message to verify your email address.</p>
                                        <p>We have sent a Verification email to <b>loremipsum@gmail.com</b> and follow the instructions.</p>
                                    </div> -->
                                    <div class="row" id="card_details">
			                            <div class="col-md-4 form-group">
			                                <label>Card Number</label>
			                                <input type="text" class="form-control" id="card_number" name="card_number" placeholder="Card Number" required />
			                                <div id="card-suggesstion-box"></div>
			                            </div>
			                            <div class="col-md-4 form-group">
			                                <label>Expiry Month / Year</label>
			                                <input type="text" class="form-control" id="card_exp" name="card_exp" placeholder="Month / Year" required />
			                            </div>
			                            <div class="col-md-4 form-group">
			                                <label>CVC Code</label>
			                                <input type="text" class="form-control" id="card_cvc" name="card_cvc" placeholder="CVC Code" required />
			                            </div> 

                                        <div class="col-md-6 form-group">
                                            <label>Card Holder Name</label>
                                            <input type="text" class="form-control" id="c_name" name="c_name" placeholder="Card Holder Name" required />
                                        </div>

                                        <div class="col-md-6 form-group">
                                            <label>Postal Code</label>
                                            <input type="text" class="form-control" id="post_code" name="post_code" placeholder="Postal Code" required />
                                        </div> 
			                            <input type="hidden" name="token" id="token" data-rule-checkvalidcard="true" value=""> 
			                        </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- Inner Body :: End -->
                </form>
            </div>
        </div>
    </div>
</section>
<!-- Inner Body :: End -->
<?php require_once USER_VIEW_PATH . 'footer.inc.php';?>

<script src="<?php echo HOME_URL; ?>assets/js/jquery.steps.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw==" crossorigin="anonymous"></script>

<script>
    $(document).on('change', '#card_number, #card_exp, #card_cvc', function (event) {
        $.ajax({
            type: "POST",
            url: "ajax/validCard.php",
            data: {
                card_number: $("#card_number").val(),
                card_exp: $("#card_exp").val(),
                card_cvc: $("#card_cvc").val(),
                c_name: $("#c_name").val(),
                post_code: $("#post_code").val(),
            },
            success: function(data){
                var res = $.parseJSON(data);
                if(res.success == 0){
                	$("#card-suggesstion-box").html(res.token).css('color', 'red').show();
                    $("#signupprovider").attr('disabled', true);
                }
                if(res.success == 1){
                    $("#token").val(res.token);
                    $("#card-suggesstion-box").hide();
                    $("#signupprovider").attr('disabled', false);
                }
            }
        });
    });

    $(document).ready(function () {

        var form = $('#signup-form').show();
        form.steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            enableFinishButton: false,

            labels: {
                finish: "Submit",
                next: "Next",
                previous: "Back",

            },
            onStepChanging: function (event, currentIndex, newIndex)
            {
            	form.validate().settings.ignore = ':disabled,:hidden';
                return form.valid();
            },
            onStepChanged: function (event, currentIndex, priorIndex) {
                if (currentIndex == 2) {
                    var $input = $('<input id="signupprovider" type="submit" name="signupprovider" class="btn btn-primary" value="Submit"/>');
                    $input.appendTo($('ul[aria-label=Pagination]'));
                } else {
                    $('ul[aria-label=Pagination] input[value="Submit"]').remove();
                }
            },
            onFinishing: function (event, currentIndex)
            {
        		form.validate().settings.ignore = ":disabled";
				return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                alert("Good job!", "Submitted!", "success");
            }
        });

        $('input[name="price_id"]').on('change', function() {
          var radioValue = $('input[name="price_id"]:checked').val();        
          if(radioValue == 0){
              $("#sub_price").html('<sup>$</sup><?php echo number_format(PRONAN_MONTH_PRICE,2); ?>');
              $("#sub_year").html('MONTH');
          }
          if(radioValue == 1){
              $("#sub_price").html('<sup>$</sup><?php echo number_format(PRONAN_YEAR_PRICE,2); ?>');
              $("#sub_year").html('YEAR');
          }
        });
        
        $('#VacanciesPostedDatePicker').datetimepicker({
            format: 'L',
            viewMode: 'years',
            format: 'MM/YYYY'
        });

        $('#card_number').mask('0000-0000-0000-0000');
        $('#card_exp').mask('00/00');
        $('#schemeDetails').css('display', 'none');
        $('#VacanciesPostSelectDate-box').css('display', 'none');
        $('#ChildcareCentreFees-area').css('display', 'none');
        $('#ChildcareCentreYearFees-area').css('display', 'none');
        $('#OccasionalCareCentreFees-area').css('display', 'none');
        $('#FamiltDayCareFees-area').css('display', 'none');
        $('#PreschoolFees-area').css('display', 'none');
        $('#NotRequiredFamilyDaycareFees-area').css('display', 'none');
        $('#BeforeAfterSchoolFees-area').css('display', 'none');
        $('#OccasionalCasualCentresFees-area').css('display', 'none');

        $("input[name='familydaycare']").click(function () {
            if ($("#familydaycare").is(":checked")) {
                $("#schemeDetails").show();
                $("#longdaycare").attr('disabled', true);
                $("#occasionalcare").attr('disabled', true);
                $("#preschool").attr('disabled', true);
            } else {
                $("#schemeDetails").hide();
            }
        });

        $("input[name='vacancy_start_on']").click(function () {
            if ($("#VacanciesPostSelectDate").is(":checked")) {
                $("#VacanciesPostSelectDate-box").show();
            } else {
                $("#VacanciesPostSelectDate-box").hide();
            }
        });

        $("input[name='ChildcareCentreFees']").click(function () {
            if ($("#ChildcareCentreFees").is(":checked")) {
                $("#ChildcareCentreFees-area").show();
            } else {
                $("#ChildcareCentreFees-area").hide();
            }
        });

        $("input[name='ChildcareCentreYearFees']").click(function () {
            if ($("#ChildcareCentreYearFees").is(":checked")) {
                $("#ChildcareCentreYearFees-area").show();
            } else {
                $("#ChildcareCentreYearFees-area").hide();
            }
        });

        $("input[name='OccasionalCareCentreFees']").click(function () {
            if ($("#OccasionalCareCentreFees").is(":checked")) {
                $("#OccasionalCareCentreFees-area").show();
            } else {
                $("#OccasionalCareCentreFees-area").hide();
            }
        });

        $("input[name='FamiltDayCareFees']").click(function () {
            if ($("#FamiltDayCareFees").is(":checked")) {
                $("#FamiltDayCareFees-area").show();
            } else {
                $("#FamiltDayCareFees-area").hide();
            }
        });

        $("input[name='PreschoolFees']").click(function () {
            if ($("#PreschoolFees").is(":checked")) {
                $("#PreschoolFees-area").show();
            } else {
                $("#PreschoolFees-area").hide();
            }
        });

        $("input[name='BeforeAfterSchoolFees']").click(function () {
            if ($("#BeforeAfterSchoolFees").is(":checked")) {
                $("#BeforeAfterSchoolFees-area").show();
            } else {
                $("#BeforeAfterSchoolFees-area").hide();
            }
        });

        $("input[name='NotRequiredFamilyDaycareFees']").click(function () {
            if ($("#NotRequiredFamilyDaycareFees").is(":checked")) {
                $("#NotRequiredFamilyDaycareFees-area").show();
            } else {
                $("#NotRequiredFamilyDaycareFees-area").hide();
            }
        });

        $("input[name='OccasionalCasualCentresFees']").click(function () {
            if ($("#OccasionalCasualCentresFees").is(":checked")) {
                $("#OccasionalCasualCentresFees-area").show();
            } else {
                $("#OccasionalCasualCentresFees-area").hide();
            }
        });
        $("input[name='subscription']").on("change", function () {
            $('.price-block').removeClass('active');
            $(this).parent().parent().toggleClass('active', this.checked);
            if($(this).val() == 'free'){
                $("#card_details").hide();
                $("#card_details").find(':input').removeAttr('required');
            }else{
                $("#card_details").show();
                $("#card_details").find(':input').attr('required', true);
            }
        });
    });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZPcppwWN179bx8Asxh8Xsd-ZiLIvZNMM&libraries=places"></script>
<script type="text/javascript">
    var placeSearch, autocomplete;

    function initialize() {
        autocomplete = new google.maps.places.Autocomplete(
        (document.getElementById('address')), {
            types: ['geocode']
        });
        google.maps.event.addDomListener(document.getElementById('address'), 'focus', geolocate);

        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            console.log(place.address_components);
            var res = new Array();
            for(var k=0; k < place.address_components.length; k++){
                if(place.address_components[k].types.indexOf('postal_code') >= 0){
                    res['postal_code'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('locality') >= 0){
                    res['suberb'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('sublocality_level_2') >= 0){
                    res['address_line1'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('sublocality_level_1') >= 0){
                    res['address_line2'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('administrative_area_level_1') >= 0){
                    res['state'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('administrative_area_level_2') >= 0){
                    res['city'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('country') >= 0){
                    res['country'] = place.address_components[k].short_name;
                }

            }
            $("#postcode").val(res['postal_code']);
            $("#superb").val(res['suberb']);
            $("#search-box1").val(res['city']);
            $("#country").val(res['country']);
        });
    }

    function geolocate() {

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                
                $("#lat").val(position.coords.latitude);
                $("#long").val(position.coords.longitude);

                var geolocation = new google.maps.LatLng(
                position.coords.latitude, position.coords.longitude);
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }

    window.addEventListener('load', (event) => {
        initialize();
    });
</script>

<script type="text/javascript">
// AIzaSyAZPcppwWN179bx8Asxh8Xsd-ZiLIvZNMM
    function getLocation() {
        if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(showPosition);
        }else{
            alert("Geolocation is not supported by this browser.");
        }
    }

    function showPosition(position) {
        $("#lat").val(position.coords.latitude);
        $("#long").val(position.coords.longitude);
        fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.coords.latitude},${position.coords.longitude}&key=AIzaSyAZPcppwWN179bx8Asxh8Xsd-ZiLIvZNMM`)
        .then(response => response.json())
        .then(result => {
            getTypeadd(result);
        });
    }
    window.addEventListener('load', (event) => {
        //getLocation();
    });

    function getTypeadd(new_cords){
        var res = new Array();
        var components = new_cords.results[0].address_components;

        for(var k=0; k < components.length; k++){
            if(components[k].types.indexOf('postal_code') >= 0){
                res['postal_code'] = components[k].long_name;
            }else if(components[k].types.indexOf('sublocality_level_2') >= 0){
                res['address_line1'] = components[k].long_name;
            }else if(components[k].types.indexOf('sublocality_level_1') >= 0){
                res['address_line2'] = components[k].long_name;
            }else if(components[k].types.indexOf('administrative_area_level_1') >= 0){
                res['state'] = components[k].long_name;
            }else if(components[k].types.indexOf('administrative_area_level_2') >= 0){
                res['city'] = components[k].long_name;
            }else if(components[k].types.indexOf('country') >= 0){
                res['country'] = components[k].short_name;
            }

        }
        $("#postcode").val(res['postal_code']);
        $("#address").val(new_cords.results[0].formatted_address);
        $("#superb").val(res['address_line2']);
        $("#search-box1").val(res['city']);
        $("#country").val(res['country']);
        return res;
    }
</script>
<script type="text/javascript">
    function valueChanged()
    {
        if($('#novacancies').is(":checked"))
            $("#vacancyinfo").hide();
        else
            $("#vacancyinfo").show();
    }
</script>
<script>
$(function() {
   $("input[name='add_waitlist']").click(function() {
     if ($("#addwaitlistno").is(":checked")) {
       $("#waitlistdiv").hide();
     } else {
       $("#waitlistdiv").show();
     }
   });
 });
</script>
<script type="text/javascript">
    $(function () {
        var countt = 1;
        $("#btnAdd").bind("click", function () {
            var div = $("<tr />");
            div.html(GetDynamicTextBox(countt++));
            $("#TextBoxContainer").append(div);
        });
        $("#btnGet").bind("click", function () {
            var values = "";
            $("input[name=DynamicTextBox]").each(function () {
                values += $(this).val() + "\n";
            });
            //alert(values);
        });
        $("body").on("click", "#removefield", function () {
            $(this).closest("#custom-field").remove();
            countt--;
        });
    });
    function GetDynamicTextBox(values) {
        return '<tr class="dynamic-tr" id="custom-field"><td data-title="#" class="text-center">' + values + '</td><td data-title="Age Group"><div class="agegroup-td d-flex"><div class="tb-inline"><select class="form-control mr-2 w-70" name="from_age[]" required><option value="">--</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select><select class="form-control w-110" name="from_age_type[]" required><option value="">--</option><option value="weeks">Weeks</option><option value="months">Months</option><option value="years">Years</option></select></div><span class="td-divider">TO</span><div class="tb-inline"><select class="form-control mr-2 w-70" name="to_age[]" required><option value="">--</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select><select class="form-control w-110" name="to_age_type[]" required><option value="">--</option><option value="weeks">Weeks</option><option value="months">Months</option><option value="years">Years</option></select></div></div></td><td data-title="Days"><div class="selectday-area"><ul><li><a href="javascript:void(0)" class="SelectDaysGroup"><input type="checkbox" class="selectdays" name="days[]" id="sunday-day' + values + '" /><label for="sunday-day' + values + '">S</label></a></li><li><a href="javascript:void(0)" class="SelectDaysGroup"><input type="checkbox" class="selectdays" name="days[]" id="monday-day' + values + '" checked="" /><label for="monday-day' + values + '">M</label></a></li><li><a href="javascript:void(0)" class="SelectDaysGroup"><input type="checkbox" class="selectdays" name="days[]" id="tuesday-day' + values + '" /><label for="tuesday-day' + values + '">T</label></a></li><li><a href="javascript:void(0)" class="SelectDaysGroup"><input type="checkbox" class="selectdays" name="days[]" id="wednesday-day' + values + '" /><label for="wednesday-day' + values + '">W</label></a></li><li><a href="javascript:void(0)" class="SelectDaysGroup"><input type="checkbox" class="selectdays" name="days[]" id="thursday-day' + values + '" /><label for="thursday-day' + values + '">T</label></a></li><li><a href="javascript:void(0)" class="SelectDaysGroup"><input type="checkbox" class="selectdays" name="days[]" id="friday-day' + values + '" /><label for="friday-day' + values + '">F</label></a></li><li><a href="javascript:void(0)" class="SelectDaysGroup"><input type="checkbox" class="selectdays" name="days[]" id="saturday-day' + values + '" /><label for="saturday-day' + values + '">S</label></a></li></ul></div></td><td class="text-center" data-title="Action"><a id="removefield" class="remove-agegroup"><i class="fa fa-minus-square"></i></a></td></tr>';
    }
</script>
<script>
// AJAX call for autocomplete
$(document).ready(function(){
    /*$("#search-box").keyup(function(){
        $.ajax({
        type: "POST",
        url: "ajax/readPostcode.php",
        data:'keyword='+$(this).val(),
        beforeSend: function(){
            $("#search-box").css("background","#FFF url(assets/images/LoaderIcon.gif) no-repeat 165px");
        },
        success: function(data){
            $("#suggesstion-box").show();
            $("#suggesstion-box").html(data);
            $("#search-box").css("background","#FFF");
        }
        });
    });*/
});
//To select country name
function selectCountry(val) {
   // $("#search-box").val(val);
    $("#suggesstion-box").hide();
}


</script>
<script>
// AJAX call for autocomplete
$(document).ready(function(){
    /*$("#search-box1").keyup(function(){
        $.ajax({
        type: "POST",
        url: "ajax/readCity.php",
        data:'keyword='+$(this).val(),
        beforeSend: function(){
            $("#search-box1").css("background","#FFF url(assets/images/LoaderIcon.gif) no-repeat 165px");
        },
        success: function(data){
            $("#suggesstion-box1").show();
            $("#suggesstion-box1").html(data);
            $("#search-box1").css("background","#FFF");
        }
        });
    });*/
});
//To select country name
function selectCountry1(val) {
    $("#search-box1").val(val);
    $("#suggesstion-box1").hide();
}
</script>
<style type="text/css">
	.error{
		color: red;
    	padding-left: 15px;
	}
</style>


<?php require_once USER_VIEW_PATH . 'frontouter.inc.php';?>