<?php
$pageName = 'home';
require_once USER_VIEW_PATH . 'header.inc.php';?>
<!-- Main Search :: Start -->
<section class="mainsearch-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form class="mainsearch-form row no-gutters" method="get" action="search-area.php">
                    <div class="form-group col-xl-3 col-lg-3 col-md-12 mb-0 bg-white">
                        <select class="form-control select2-show-search" name="type">
                            <option value="All">All Types</option>
                            <option value="careprovider">Childcare Centre</option>
                            <option value="2">3 Years to 5 Years Group Childcare Centre</option>
                            <option value="3">Multi-age Childcare Centre</option>
                            <option value="4">Family Day Care</option>
                            <option value="5">Kindergartens</option>
                            <option value="7">Before & After School Care</option>
                            <option value="8">Occasional, Casual & Drop-in Centres</option>
                            <option value="5">Pre-School</option>
                            <option value="nanny">Nanny</option>
                            <option value="babysitter">Babysitters</option>
                        </select>
                    </div>
                    <div class="form-group col-xl-7 col-lg-7 col-md-12 mb-0 bg-white autocomplete">
                        <input type="text" id="search-box" name="location" class="form-control" placeholder="Postcode or City" onclick="getLocation()">
                        <div id="suggesstion-box"></div>
                    </div>
                    <input type="hidden" name="lat" id="mylat" value="49.2768">
                    <input type="hidden" name="lng" id="mylng" value="-123.0391">
                    <div class="col-xl-2 col-lg-2 col-md-12 mb-0">
                        <button type="submit" class="btn btn-secondary btn-block">Search</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- Main Search :: End -->
<!-- Banner :: Start -->
<section class="banner-section">
    <div id="mainslider" class="master-slider ms-skin-default" >
        <div class="ms-slide" data-delay="5" data-fill-mode="fill" >
            <div class="ms-overlay-layers"></div>
            <img src="<?php echo HOME_URL; ?>assets/images/blank.gif" alt="" title="" data-src="<?php echo HOME_URL; ?>assets/images/banner/slider-1.jpg" />
            <div class="ms-layer banner-title" style="" data-effect="t(true,n,200,n,n,n,n,n,n,n,n,n,n,n,n)" data-duration="2000" data-delay="200" data-ease="easeOutQuart" data-offset-x="38" data-offset-y="-80" data-origin="ml" data-position="normal">
                <span class="col-white">Big Search Engine</span>
                <span class="col-white">for small people</span>
            </div>
            <div class="ms-layer banner-content" style="" data-effect="t(true,n,200,n,n,n,n,n,n,n,n,n,n,n,n)" data-duration="1600" data-delay="1600" data-ease="easeOutQuart" data-offset-x="38" data-offset-y="30" data-origin="ml" data-position="normal" >
                You can watch Ostburn VS. Highgriffin games on TV. Check out the list of TV
                <br />
                Channels showing the Elfdale matches in your country. Many sites broadcast
                <br />
                the game live on the internet.
            </div>
            <a class="ms-layer banner-btn-line" href="about-us.php" data-effect="t(true,n,30,n,n,n,n,n,n,n,n,n,n,n,n)" data-duration="1412" data-delay="2100" data-ease="easeOutQuart" data-offset-x="38" data-offset-y="120" data-origin="ml" data-position="normal" >Learn More</a>
        </div>
        <div class="ms-slide" data-delay="5" data-fill-mode="fill" >
            <div class="ms-overlay-layers"></div>
            <img src="<?php echo HOME_URL; ?>assets/images/blank.gif" alt="" title="" data-src="<?php echo HOME_URL; ?>assets/images/banner/slider-2.jpg" />
            <div class="ms-layer banner-title" style="" data-effect="t(true,n,200,n,n,n,n,n,n,n,n,n,n,n,n)" data-duration="2000" data-delay="200" data-ease="easeOutQuart" data-offset-x="38" data-offset-y="-80" data-origin="ml" data-position="normal">
                <span class="col-white">Big Search Engine</span>
                <span class="col-white">for small people</span>
            </div>
            <div class="ms-layer banner-content" style="" data-effect="t(true,n,200,n,n,n,n,n,n,n,n,n,n,n,n)" data-duration="1600" data-delay="1600" data-ease="easeOutQuart" data-offset-x="38" data-offset-y="30" data-origin="ml" data-position="normal" >
                You can watch Ostburn VS. Highgriffin games on TV. Check out the list of TV
                <br />
                Channels showing the Elfdale matches in your country. Many sites broadcast
                <br />
                the game live on the internet.
            </div>
            <a class="ms-layer banner-btn-line" href="about-us.php" data-effect="t(true,n,30,n,n,n,n,n,n,n,n,n,n,n,n)" data-duration="1412" data-delay="2100" data-ease="easeOutQuart" data-offset-x="38" data-offset-y="120" data-origin="ml" data-position="normal" >Learn More</a>
        </div>
        <div class="ms-slide" data-delay="5" data-fill-mode="fill" >
            <div class="ms-overlay-layers"></div>
            <img src="<?php echo HOME_URL; ?>assets/images/blank.gif" alt="" title="" data-src="<?php echo HOME_URL; ?>assets/images/banner/slider-1.jpg" />
            <div class="ms-layer banner-title" style="" data-effect="t(true,n,200,n,n,n,n,n,n,n,n,n,n,n,n)" data-duration="2000" data-delay="200" data-ease="easeOutQuart" data-offset-x="38" data-offset-y="-80" data-origin="ml" data-position="normal">
                <span class="col-white">Big Search Engine</span>
                <span class="col-white">for small people</span>
            </div>
            <div class="ms-layer banner-content" style="" data-effect="t(true,n,200,n,n,n,n,n,n,n,n,n,n,n,n)" data-duration="1600" data-delay="1600" data-ease="easeOutQuart" data-offset-x="38" data-offset-y="30" data-origin="ml" data-position="normal" >
                You can watch Ostburn VS. Highgriffin games on TV. Check out the list of TV
                <br />
                Channels showing the Elfdale matches in your country. Many sites broadcast
                <br />
                the game live on the internet.
            </div>
            <a class="ms-layer banner-btn-line" href="about-us.php" data-effect="t(true,n,30,n,n,n,n,n,n,n,n,n,n,n,n)" data-duration="1412" data-delay="2100" data-ease="easeOutQuart" data-offset-x="38" data-offset-y="120" data-origin="ml" data-position="normal" >Learn More</a>
        </div>
    </div>
</section>
<!-- Banner :: End -->
<!-- About us :: Start -->
<section class="aboutus-section pt-100 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-lg-5">
                <div class="aboutus-img">
                    <div class="inner-img">
                        <img class="img-fluid" src="<?php echo HOME_URL; ?>assets/images/about-2.jpg" alt="About Us" />
                    </div>
                </div>
            </div>
            <div class="col-xl-8 col-lg-7 pl-5">
                <div class="section-title mb-30">
                    <h2 class="mb-30">Unlock a Network of Reliable Daycares, Nannies and Babysitters in Canada</h2>
                    <h6 class="mb-30">Connect with Reliable Daycares, Nannies and Babysitters near you and find the one that’s right for your child.</h6>
                </div>
                <div class="">
                    <p class="f-16">Wondering who will take care of the young ones for you when you are away? Worry no more! At DaycareSeekers, we got you covered! We bring you all the trusted daycare facilities, nannies, and babysitters in one place so that you can find the best within the shortest time.</p>
                </div>
                <!--<a href="about-us.html" class="btn btn-primary">View More <i class="fas fa-long-arrow-alt-right pl-1"></i></a>-->
            </div>
        </div>
    </div>
</section>
<!-- About us :: End -->
<!-- Our Services :: Start -->
<section class="services-section bg-grey pt-100 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="section-title text-center mb-30">
                    <h2 class="mb-2">Our Best Services</h2>
                    <p>We are providing a best services</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="services-box">
                    <div class="services-image">
                        <img class="img-fluid" src="<?php echo HOME_URL; ?>assets/images/trusted-community.jpg" alt="A Trusted Community"/>
                    </div>
                    <div class="services-details">
                        <div class="services-title">A Trusted Community</div>
                        <p class="services-desc">Whether you are looking for a daycare, babysitter, or nanny, we have detailed profiles of the ideal candidates, which are complemented by honest reviews and ratings from local families. We also provide reference contact information to verify your preferred individuals' qualifications before hiring them.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="services-box">
                    <div class="services-image">
                        <img class="img-fluid" src="<?php echo HOME_URL; ?>assets/images/real-time-communication.jpg" alt="Real-Time Communication"/>
                    </div>
                    <div class="services-details">
                        <div class="services-title">Real-Time Communication</div>
                        <p class="services-desc">Our foundation is built on honesty and speedy 2-way communication. We provide parents with verified spot alerts, which notifies them when there is a slot available in their preferred daycare facilities.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="services-box">
                    <div class="services-image">
                        <img class="img-fluid" src="<?php echo HOME_URL; ?>assets/images/engrossed-trust-safety.jpg" alt="Engrossed In Trust & Safety"/>
                    </div>
                    <div class="services-details">
                        <div class="services-title">Engrossed In Trust & Safety</div>
                        <p class="services-desc">Our highly experienced team champions safety procedures across our website every day. We do an ongoing site content monitoring, ID authentication, family watchdog screening, and scam prevention to give you peace of mind when hiring your childcare team.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="services-box">
                    <div class="services-image">
                        <img class="img-fluid" src="<?php echo HOME_URL; ?>assets/images/daycare.jpg" alt="Daycare"/>
                    </div>
                    <div class="services-details">
                        <div class="services-title">Daycare</div>
                        <p class="services-desc">Find a daycare facility that will provide your child with the best care as you focus on other things that matter to your personal or professional life. Find a trusted daycare facility now!</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="services-box">
                    <div class="services-image">
                        <img class="img-fluid" src="<?php echo HOME_URL; ?>assets/images/nannies.jpg" alt="Nannies"/>
                    </div>
                    <div class="services-details">
                        <div class="services-title">Nannies</div>
                        <p class="services-desc">Find a professional nanny who can give your young one(s) as a part-time, daytime, overnight, full-time, live-in, and more! Find a nanny now!</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="services-box">
                    <div class="services-image">
                        <img class="img-fluid" src="<?php echo HOME_URL; ?>assets/images/babysitters.jpg" alt="Babysitters"/>
                    </div>
                    <div class="services-details">
                        <div class="services-title">Babysitters</div>
                        <p class="services-desc">Find a wide range of local options for virtual or in-home sitting to help with date nights, last-minute needs, strict schedules, and more. Find a babysitter now!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Our Services :: End -->
<!-- How it works :: Start -->
<section class="howsitwork-section bg-dark pt-100 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="section-title text-white text-center mb-80">
                    <h2 class="mb-2">Find A Child Care Partner In Three Steps!</h2>
                    <p>There have only few steps for find you task.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="steps-box mb-sm-50">
                    <div class="steps-icon"><i class="ti-location-pin"></i></div>
                    <div class="steps-desc">
                        <h2 class="step-title">Search</h2>
                        <p class="step-content">Browse through a broad range of childcare options available in your local area.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="steps-box mb-sm-50">
                    <div class="steps-icon"><i class="ti-direction-alt"></i></div>
                    <div class="steps-desc">
                        <h2 class="step-title">Compare</h2>
                        <p class="step-content">Use trusted reviews, ratings, and price ranges to identify the best childcare partner you can afford.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="steps-box">
                    <div class="steps-icon"><i class="ti-mobile"></i></div>
                    <div class="steps-desc">
                        <h2 class="step-title">Contact</h2>
                        <p class="step-content">Email your shortlisted candidates for an interview and final enrolment.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- How it works :: End -->
<!-- World Wide :: Start -->
<!--<section class="worldwide-section pt-100 pb-100">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-5 col-lg-6 pl-5">
                <div class="section-title mb-30">
                    <h2 class="mb-30">Lorem Ipsum is simply printing industry.</h2>
                    <h6 class="mb-30">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin from 45 BC</h6>
                </div>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores</p>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores</p>
            </div>
            <div class="col-xl-7 col-lg-6">
                <div class="counter-section">
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-6">
                            <div class="counter">
                                <div class="counter-icon">
                                    <i class="ti-world"></i>
                                </div>
                                <div class="counter-content">
                                    <span class="timer" data-to="650" data-speed="1000">650</span>
                                    <label>Worldwide Branches</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-6">
                            <div class="counter">
                                <div class="counter-icon">
                                    <i class="ti-heart"></i>
                                </div>
                                <div class="counter-content">
                                    <span class="timer" data-to="100" data-speed="1000">100</span>
                                    <label>Verify Sitters</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-6">
                            <div class="counter">
                                <div class="counter-icon">
                                    <i class="ti-thumb-up"></i>
                                </div>
                                <div class="counter-content">
                                    <span class="timer" data-to="30" data-speed="1000">30</span>
                                    <label>Local Partners</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-6">
                            <div class="counter">
                                <div class="counter-icon">
                                    <i class="ti-face-smile"></i>
                                </div>
                                <div class="counter-content">
                                    <span class="timer" data-to="1500" data-speed="1000">1500</span>
                                    <label>Happy Client</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>-->
<!-- World Wide :: End -->
<!-- Testimonials :: Start -->
<section class="testimonials-section pt-100 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="section-title text-center mb-30">
                    <h2 class="mb-2">Testimonials</h2>
                    <p>Check what our client say about us!</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-8 col-lg-12 offset-xl-2">
                <div class="owl-carousel owl-theme">
                    <div class="testimonials-box">
                        <div class="testimonials-content">"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages."</div>
                        <div class="testimonials-author">- Sara Lisbon</div>
                    </div>
                    <div class="testimonials-box">
                        <div class="testimonials-content">"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages."</div>
                        <div class="testimonials-author">- Akash</div>
                    </div>
                    <div class="testimonials-box">
                        <div class="testimonials-content">"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages."</div>
                        <div class="testimonials-author">- Sara Lisbon</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Testimonials :: End -->
<!-- Advertise :: Start -->
<section class="advertise-section pt-50 pb-50">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-8">
                <div class="advertise-box">
                    <div class="advertise-title mb-20">Let us help you find the child care you need</div>
                    <div class="advertise-content mb-20">
                        <p>We understand what you are going through. This is why we make it easy for you to find nannies and babysitters who will change your life. With us, you will have less stress, improved flexibility, and more peace of mind. We will give you immediate access to thousands of active child care experts who will never let you down. Let’s walk on this journey together!
</p><p>Register your needs, set your vacancy alert level, and leave the rest to us! </p>
                    </div>
                    <a href="sign-up.php" class="btn btn-primary">Register Now <i class="fas fa-long-arrow-alt-right pl-1"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Advertise :: End -->
<?php require_once USER_VIEW_PATH . 'footer.inc.php';?>
<script>
// AJAX call for autocomplete
$(document).ready(function(){
    $("#search-box").keyup(function(){
        $.ajax({
        type: "POST",
        url: "ajax/readLocation.php",
        data:'keyword='+$(this).val(),
        beforeSend: function(){
            $("#search-box").css("background","#FFF url(<?php echo HOME_URL; ?>assets/images/LoaderIcon.gif) no-repeat 165px");
        },
        success: function(data){
            $("#suggesstion-box").show();
            $("#suggesstion-box").html(data);
            $("#search-box").css("background","#FFF");
        }
        });
    });
});
//To select country name
function selectCountry(val) {
    $("#search-box").val(val);
    $("#suggesstion-box").hide();
}
</script>
<?php require_once USER_VIEW_PATH . 'frontouter.inc.php';?>