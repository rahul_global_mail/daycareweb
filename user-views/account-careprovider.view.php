<?php
$pageName = 'home';
require_once USER_VIEW_PATH . 'header.inc.php';
?>
<!-- Inner Banner :: Start -->
<section class="inner-banner">
    <div class="inner-content">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="profileimg-area">
                        <img class="img-fluid" src="upload_images/<?php echo $providerData[0]->photo; ?>" alt="Avtar" />
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="myaccount-name">
                        <h1>Hello, <?php echo ucwords($providerData[0]->provider_name); ?></h1>
                        <h3>Contact Person : [ <?php echo ucwords($providerData[0]->contact_name); ?> ]</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inner Banner :: End -->
<!-- Inner Body :: Start -->
<section class="myaccount-section">
    <div class="myaccount-menu">
        <div class="container scroll-container">
            <ul class="myaccount-nav scroll-nav nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="dashboard-tab" data-toggle="tab" href="#dashboard" role="tab">Dashboard</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab">Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="servicedetails-tab" data-toggle="tab" href="#servicedetails" role="tab">Service Details</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="carefees-tab" data-toggle="tab" href="#carefees" role="tab">Care Fees</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="vacancies-tab" data-toggle="tab" href="#vacancies " role="tab">Vacancies</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="waitlisting-tab" data-toggle="tab" href="#waitlisting " role="tab">Waitlisting</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="gallery-tab" data-toggle="tab" href="#gallery " role="tab">Gallery</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="modal" data-target="#ChangePasswordModal">Change Password</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="modal" data-target="#DeleteProfileModal">Delete Profile</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="dashboard" aria-labelledby="dashboard-tab">
                        <div class="myaccount-area pt-50 pb-50">
                            <?php if ($_SESSION['add_msg'] == 'success') {?>
                                <div class="alert alert-success alert-dismissible" id="success-alert">
                                  <strong>Success!</strong> Data Submitted successfully.
                                </div>
                                <?php }?>
                                <?php if ($_SESSION['add_msg'] == 'error') {?>
                                <div class="alert alert-danger alert-dismissible" id="success-alert">
                                  <strong>Failed!</strong> Somthing goes wrong.Please try again.
                                </div>
                                <?php }?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="section-title mb-5">
                                        <h2>My Dashboard</h2>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tabscustom-tabs">
                                        <ul class="nav nav-tabs custom-tabs" id="myTab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="dytab1-tab" data-toggle="tab" href="#dytab1" role="tab" aria-controls="dytab1" aria-selected="true">Waitlist Applications</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="dytab2-tab" data-toggle="tab" href="#dytab2" role="tab" aria-controls="dytab2" aria-selected="false">Spot Alert</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane fade show active" id="dytab1" role="tabpanel" aria-labelledby="dytab1-tab">
                                                <div class="table-responsive">
                                                    <table id="table" class="table table-striped-odd table-bordered mb-0 text-nowrap">
                                                        <thead>
                                    <tr>
                                        <th class="no-sort text-center">#</th>
                                        <th class="no-sort text-center">Parent Name</th>
                                        <th class="no-sort text-center">Email</th>
                                        <th class="no-sort text-center">Mobile</th>
                                        <th class="no-sort text-center">Child Name</th>
                                        <th class="no-sort text-center">DOB</th>
                                        <th class="no-sort text-center">Requested Date</th>
                                        <th class="no-sort text-center">Days</th>
                                        <th class="no-sort text-center">Intrest</th>
                                        <th class="no-sort text-center">Referal</th>
                                        <th class="no-sort text-center">Comments</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="htmldata">
                                    <?php
$countwait = 1;
if ($waitlistData) {
	foreach ($waitlistData as $datawait) {
		?>
                                    <tr>
                                        <td class="text-center"><?php echo $countwait++; ?></td>
                                        <td class="text-center"><?php echo $datawait->carer_fullname; ?></td>
                                        <td class="text-center"><?php echo $datawait->email; ?></td>
                                        <td class="text-center"><?php echo $datawait->mobile; ?></td>
                                        <td class="text-center"><?php echo $datawait->child_fname . " " . $datawait->child_lname; ?></td>
                                        <td class="text-center"><?php echo $datawait->child_dob; ?></td>
                                        <td class="text-center"><?php echo $datawait->req_date; ?></td>
                                        <td class="text-center"><?php echo $datawait->req_days; ?></td>
                                        <td class="table-overflow-text"><?php echo $datawait->interest; ?></td>
                                        <td class="text-center"><?php echo $datawait->referal_from; ?></td>
                                        <td class="table-overflow-text"><?php echo $datawait->comments; ?></td>
                                        <td class="text-center"><?php
if ($datawait->status == 'accept') {?>
    <span class="btn btn-info btn-sm">Accepted</i></span>
                        <?php } elseif ($datawait->status == 'decline') {?>
<span class="btn btn-warning btn-sm">Rejected</i></span>
                        <?php } else {?>
<a href="#" class="btn btn-success btn-sm acceptdata" id="<?php echo $datawait->waitlist_id; ?>">Accept</i></a>
<a href="#" class="btn btn-danger btn-sm declinedata" id="<?php echo $datawait->waitlist_id; ?>">Decline</i></a>
                        <?php }?></td>
                                    </tr>
                                    <?php }} else {?>
                                        <tr>
                                            <td colspan="8" class="text-center">
                                                No data Found.
                                            </td>
                                        </tr>
                                    <?php }?>
                                </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="dytab2" role="tabpanel" aria-labelledby="dytab2-tab">
                                                <div class="table-responsive">
                                                    <table class="table table-striped-odd table-bordered mb-0 text-nowrap DataTables">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-center no-sort">#</th>
                                                                <th class="text-center">From</th>
                                                                <th class="text-center">To</th>
                                                                <th class="text-center">Days</th>
                                                                <th class="text-center">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
<?php
$vc_count = 1;
foreach ($vacancyData as $vacancy) {?>
                                                            <tr>
                                                                <td class="text-center"><?php echo $vc_count++; ?></td>
                                                                <td>
                                                                    <?php echo $vacancy->from_age . " , " . $vacancy->from_age_type; ?>
                                                                </td>
                                                                <td> <?php echo $vacancy->to_age . " , " . $vacancy->to_age_type; ?></td>
                                                                <td class="text-center"> <?php echo $vacancy->days; ?></td>
                                                                <td class="text-center align-middle">
                                                                    <a class="btn btn-success btn-sm" id="vacancies-tab" data-toggle="tab" href="#vacancies " role="tab"><i class="fa fa-eye"></i></a>
                                                                </td>
                                                            </tr>
                                                        <?php }?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="package-subscribe">
                                    <div class="package-title">
                                        <h2>Premium<?php if($subscriptions): ?> <?php echo ' - '. $subscriptions[0]->plan_interval; ?><?php endif; ?></h2>
                                        <h3><sup><?php echo $subscriptions[0]->plan_amount_currency; ?></sup><?php echo $subscriptions[0]->plan_amount; ?></h3>
                                    </div>
                                    <div class="package-purchasedate">
                                        <?php if($subscriptions): ?>
                                        <span class="package-date-title">Purchase Date</span>
                                        <span class="package-date-cal"><?php echo date("F j, Y", strtotime($subscriptions[0]->created)) ; ?></span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="package-expiredate">
                                        <?php if($subscriptions): ?>
                                        <span class="package-date-title">Expire Date</span>
                                        <span class="package-date-cal"><?php echo date("F j, Y", strtotime($subscriptions[0]->enddate)) ; ?></span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="package-action">
                                        <?php if($subscriptions): ?>
                                        <a href="#" class="btn btn-primary">Renew Plan</a>
                                        <?php else: ?>
                                        <a class="btn btn-primary" data-toggle="modal" data-target="#subPlan">Subscribe Plan</a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="servicedetails" aria-labelledby="servicedetails-tab">
                        <div class="myaccount-area pt-50 pb-50">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="section-title mb-3">
                                        <h2>Service Details</h2>
                                    </div>
                                </div>
                            </div>
                            <form method="post" enctype="multipart/form-data">
                                <input type="hidden" name="service_provider_id" value="<?php echo $providerData[0]->provider_id; ?>">
                                <fieldset class="mb-20">
                                    <input type="hidden" name="get_photo" value="<?php echo $providerData[0]->photo; ?>">
                                    <h2 class="form-title">Name & Type of service</h2>
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <label>Profile Avatar<i class="required">*</i></label>
                                            <input type="file" name="photo" class="btn btn-primary btn-md" accept="image/*">
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label>Name of Service<i class="required">*</i></label>
                                            <input type="text" class="form-control" name="provider_name" placeholder="Name of Service" required="" value="<?php echo $providerData[0]->provider_name; ?>" />
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <div class="checkbox-list">
                                                <ul>
                                                    <?php
foreach ($serviceData as $service) {
	?>
<li>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="longdaycarefees<?php echo $service->provider_service_id; ?>" name="provider_service_id[]" value="<?php echo $service->provider_service_id; ?>" <?php
$dataArr = explode(",", $providerData[0]->provider_service_id);
	foreach ($dataArr as $arr) {
		if ($service->provider_service_id == $arr) {echo "checked";}
	}
	?>>
                                                    <label class="custom-control-label" for="longdaycarefees<?php echo $service->provider_service_id; ?>"><?php echo $service->title; ?></label>
                                                </div>
                                            </li>
                                            <?php }?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="mb-20">
                                    <h2 class="form-title">Benefits</h2>
                                    <div class="row">
                                        <?php
foreach ($benefitsData as $benefit) {
	?>


                                        <div class="col-md-6">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" name="benifits[]" class="custom-control-input" value="<?php echo $benefit->benefit_id; ?>" id="firstaid<?php echo $benefit->benefit_id; ?>" <?php
$dataArr1 = explode(",", $providerData[0]->benifits);
	foreach ($dataArr1 as $arr1) {
		if ($benefit->benefit_id == $arr1) {echo "checked";}
	}
	?>/>
                                                <label class="custom-control-label" for="firstaid<?php echo $benefit->benefit_id; ?>"><i class="fas fa-book pr-1"></i><?php echo $benefit->benefit_title; ?></label>
                                            </div>
                                        </div>
                                        <?php }?>
                                    </div>
                                </fieldset>
                                <button type="submit" class="btn btn-primary" name="updateCareservice">Submit</button>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="carefees" aria-labelledby="carefees-tab">
                        <div class="myaccount-area pt-50 pb-50">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="section-title mb-3">
                                        <h2>Child Care Fees</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tabscustom-tabs">
                                        <ul class="nav nav-tabs custom-tabs" id="myTab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="dytab101-tab" data-toggle="tab" href="#dytab101" role="tab" aria-controls="dytab101" aria-selected="true">Care fees list</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="dytab201-tab" data-toggle="tab" href="#dytab201" role="tab" aria-controls="dytab201" aria-selected="false">Add Fees</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane fade show active" id="dytab101" role="tabpanel" aria-labelledby="dytab101-tab">
                                                <div class="table-responsive">
                                                    <table class="table table-striped-odd table-bordered mb-0 text-nowrap DataTables">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-center no-sort">#</th>
                                                                <!-- <th class="text-center no-sort">Service</th> -->
                                                                <th>Type / Fees</th>
                                                                <th>Age</th>
                                                                <th>Notes</th>
                                                                <th class="text-center">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
$feesCount = 1;
foreach ($feesData as $fees) {?>
                                                            <tr>
                                                                <td class="text-center"><?php echo $feesCount++; ?></td>
                                                                <!-- <td class="text-center">
                                                                   <?php echo $fees->title; ?>
                                                                </td> -->
                                                                <td class="text-center">
                                                                   <?php echo $fees->type . " / $" . $fees->price; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $fees->ages; ?>
                                                                </td>
                                                                <td><?php echo $fees->notes; ?></td>
                                                                <td class="text-center"><button type="submit" class="btn btn-danger btn-sm deleteCarefees" id="<?php echo $fees->fees_id; ?>" data-toggle="tooltip" data-placement="top" title="Remove"><i class="fa fa-trash"></i></button></td>
                                                            </tr>
                                                        <?php }?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="dytab201" role="tabpanel" aria-labelledby="dytab201-tab">
                                                <form method="post">
                                                <input type="hidden" name="fees_provider_id" value="<?php echo $providerData[0]->provider_id; ?>">
                                <fieldset class="mb-20">
                                    <div class="row">
                                        <!-- <div class="col-md-6 form-group">
                                            <label>Service Type<i class="required">*</i></label>
                                            <select class="form-control" name="provider_service_id">
                                                <option value="">Select Type</option>
                                                <?php
foreach ($serviceData as $service) {?>
                                                    <option value="<?php echo $service->provider_service_id; ?>"><?php echo $service->title; ?></option>
                                                <?php }?>


                                            </select>
                                        </div> -->
                                        <div class="col-md-6 form-group">
                                            <label>Fees<i class="required">*</i></label>
                                            <input type="text" class="form-control" name="price" placeholder="Price($)" required="" value="<?php echo $providerData[0]->price; ?>" />
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Type<i class="required">*</i></label>
                                            <select class="form-control" name="type">
                                                <option value="">Select Type</option>
                                                <option value="Day">Day</option>
                                                <option value="Weeks">Weeks</option>
                                                <option value="Months">Months</option>
                                                <option value="Years">Years</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Add your own type :<i class="required">*</i></label>
                                            <input type="text" class="form-control" name="ages" placeholder="e.g : For ages 6wks to 2yrs" required=""/>
                                            <small class="form-text text-muted">Note : e.g : For ages 6wks to 2yrs</small>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Notes</label>
                                            <input type="text" class="form-control" name="notes" placeholder="Notes"/>
                                        </div>
                                    </div>
                                </fieldset>
                                <input type="submit" value="Submit" name="addCarefees" class="btn btn-info">
                            </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile" aria-labelledby="profile-tab">
                        <div class="myaccount-area pt-50 pb-50">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="section-title mb-3">
                                        <h2>Profile</h2>
                                    </div>
                                </div>
                            </div>
                            <form method="post">
                                <input type="hidden" name="profile_provider_id" value="<?php echo $providerData[0]->provider_id; ?>">
                                <fieldset class="mb-20">
                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <label>Contact Name<i class="required">*</i></label>
                                            <input type="text" class="form-control" name="contact_name" placeholder="Contact Name" required="" value="<?php echo $providerData[0]->contact_name; ?>" />
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Email Address<i class="required">*</i></label>
                                            <input type="email" class="form-control" name="provider_email" placeholder="Email Address" required="" value="<?php echo $providerData[0]->provider_email; ?>"/>
                                            <small class="form-text text-muted">Please nominate the primary email address that you can be contacted on at all times</small>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Address<i class="required">*</i></label>
                                            <input type="text" id="address" onFocus="geolocate();" class="form-control" name="address" placeholder="Enter a full address" required="" value="<?php echo $providerData[0]->address; ?>"/>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Town/Suburb<i class="required">*</i></label>
                                            <input id="suburb" type="text" class="form-control" name="suburb" placeholder="Town/Suburb" required="" value="<?php echo $providerData[0]->suburb; ?>"/>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Postcode<i class="required">*</i></label>
                                            <input id="postcode" type="text" id="search-box" class="form-control" name="postcode" placeholder="Postcode" required="" value="<?php echo $providerData[0]->postcode; ?>"/>
                                            <div id="suggesstion-box"></div>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>City<i class="required">*</i></label>
                                            <input type="text" id="city" class="form-control" name="city" placeholder="City" required="" value="<?php echo $providerData[0]->city; ?>"/>
                                            <div id="suggesstion-box1"></div>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Country<i class="required">*</i></label>
                                            <select class="form-control" name="country">
                                                <option value="selectstate">Select Country</option>
                                                <option value="CA" <?php if ($providerData[0]->country == 'CA') {echo "selected";}?>>Canada</option>
                                                <option value="US" <?php if ($providerData[0]->country == 'US') {echo "selected";}?>>US</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Phone Number<i class="required">*</i></label>
                                            <input type="text" class="form-control" name="phone" placeholder="Phone Number" required="" value="<?php echo $providerData[0]->phone; ?>"/>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label>About</label>
                                            <textarea name="about" class="form-control summernote" required><?php echo $providerData[0]->about; ?></textarea>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label>Inspection Web Link</label>
                                            <input type="text" class="form-control" name="inspection_link" placeholder="https://" value="<?php echo $providerData[0]->inspection_link; ?>"/>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label>Website / APP Link (external)</label>
                                            <input type="text" class="form-control" name="web_app_link" placeholder="https://"  value="<?php echo $providerData[0]->web_app_link; ?>"/>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label>Facebook Link</label>
                                            <input type="text" class="form-control" name="fb_link" placeholder="https://"  value="<?php echo $providerData[0]->fb_link; ?>"/>
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label>Approved Places</label>
                                            <input type="Number" class="form-control" name="approved_places" min="0"  value="<?php echo $providerData[0]->approved_places; ?>"/>
                                        </div>

                                    </div>
                                </fieldset>
                                <input type="submit" value="Submit" name="updateProfile" class="btn btn-info">
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="vacancies" aria-labelledby="vacancies-tab">
                        <div class="myaccount-area pt-50 pb-50">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="section-title mb-3">
                                        <h2>Vacancies - Permanent</h2>
                                    </div>
                                </div>
                            </div>
                            <form method="post">
                                <fieldset class="mb-20">
                                    <input type="hidden" name="vacancyprovider_id" value="<?php echo $providerData[0]->provider_id; ?>">
                                    <div class="form-row d-flex align-items-center vacancies-box mb-20">
                                        <div class="col-auto padt-3 pr-2">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" name="isvacancy" class="custom-control-input" id="novacancies" <?php if ($providerData[0]->is_vacancy == 'No') {echo "checked";}?>>
                                                <input type="hidden" id="is_vacancy" name="is_vacancy" value="Yes">

                                                <label class="custom-control-label" for="novacancies">No Vacancies</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="vacancyinfo">
                                        <div class="col-md-12">
                                            <div class="add-additional-box mb-3">
                                                <a href="javascript:void(0)">
                                                    <div class="add-additional-icon"><i class="far fa-plus-square"></i></div>
                                                    <div class="add-additional-title" id="btnAdd">Add Vacancy Information</div>
                                                </a>
                                            </div>
                                            <div class="agegroup-area">
                                                <div class="no-more-tables">
                                                    <table class="table table-bordered table-alignmiddle">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-center">#</th>
                                                                <th>Age Group</th>
                                                                <th>Days</th>
                                                                <th>Start From</th>
                                                                <th class="text-center">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="TextBoxContainer">

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="hint mb-2">Click on the blue icon to add a row to remove a row click on the red icon. Ideally you should have a row for each age group you service (e.g., 0-2 years, 2-3 years, 3-5 years).</div>
                                            <div class="hint mb-4"><b>IMPORTANT:</b> you should enter the age groups you service whether you have vacancies for those age groups or not. Please only include vacancies that are available now not for wait listing. Purple means you have a vacancy.</div>
                                        </div>
                                    </div>
                                    <h2 class="form-title">Vacancies List</h2>
                                    <div class="table-responsive">
                                    <table class="table table-striped-odd table-bordered mb-0 text-nowrap DataTables">
                                        <thead>
                                            <tr>
                                                <th class="no-sort text-center">#</th>
                                                <th class="no-sort text-center">Child Age</th>
                                                <th class="no-sort text-center">Days</th>
                                                <th class="no-sort text-center">Started From</th>
                                                <th class="no-sort text-center">Created Date</th>
                                                <th class="no-sort text-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="htmldata">
                                            <?php
$count = 1;
if ($chilData) {
	foreach ($chilData as $data) {
		?>
                                            <tr>
                                                <td class="text-center"><?php echo $count++; ?></td>
                                                <td class="text-center"><b>From</b> <?php echo $data->from_age . " " . $data->from_age_type . " <b>To </b>" . $data->to_age . " " . $data->to_age_type; ?></td>
                                                <td class="text-center"><?php echo $data->days; ?></td>
                                                <td class="text-center"><?php echo $data->start_from; ?></td>
                                                <td class="text-center"><?php echo $data->created_date; ?></td>
                                                <td class="text-center"><button type="submit" class="btn btn-danger btn-sm deleteVacancy" id="<?php echo $data->vacancy_id; ?>" data-toggle="tooltip" data-placement="top" title="Remove"><i class="fa fa-trash"></i></button></td>
                                            </tr>
                                            <?php }} else {?>
                                                <tr>
                                                    <td colspan="8" class="text-center">
                                                        No data Found.
                                                    </td>
                                                </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                                </fieldset>
                                <fieldset class="mb-20" style="display: none;">
                                    <h2 class="form-title">The Vacancies Posted Are Available</h2>
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="VacanciesPostNow" name="vacancy_start_on" class="custom-control-input" value="Now" <?php if ($providerData[0]->vacancy_start_on == 'Now') {echo "checked";}?> />
                                                    <label class="custom-control-label" for="VacanciesPostNow">Now</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="VacanciesPostSelectDate" name="vacancy_start_on" class="custom-control-input" <?php if ($providerData[0]->vacancy_start_on != 'Now') {echo "checked";}?>/>
                                                    <label class="custom-control-label" for="VacanciesPostSelectDate">MM/YYYY</label>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="vacanciesvacancy_start_on" id="Vacanciesvacancy_start_on">
                                        <div class="col-md-6" id="VacanciesPostSelectDate-box">
                                            <div class="form-group">
                                                <label>Select Month & Year</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="VacanciesPostedDatePicker" name="vacancy_start_value" data-toggle="datetimepicker" data-target="#VacanciesPostedDatePicker"/>
                                                    <div class="input-group-append">
                                                        <div class="input-group-text"><i class="fas fa-calendar-alt"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <input type="submit" value="Submit" name="updateVacancy" class="btn btn-info">
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="waitlisting" aria-labelledby="waitlisting-tab">
                        <div class="myaccount-area pt-50 pb-50">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="section-title mb-3">
                                        <h2>Waitlisting</h2>
                                    </div>
                                </div>
                            </div>
                            <form method="post">
                                <input type="hidden" name="waitlist_provider_id" value="<?php echo $providerData[0]->provider_id; ?>">
                                <fieldset class="mb-20">
                                    <!-- <h2 class="form-title mb-0">Waitlisting</h2> -->
                                    <p>Our waitlisting service allows families to conveniently waitlist with you and best of all its free or you can use your own waitlist form.</p>
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <label>Add waitlist</label>
                                            <div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="addwaitlistyes" name="add_waitlist" value="Yes" class="custom-control-input" <?php if ($providerData[0]->add_waitlist == 'Yes') {echo "checked";}?>>
                                                    <label class="custom-control-label" for="addwaitlistyes">Yes please</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="addwaitlistno" name="add_waitlist" value="No" class="custom-control-input" <?php if ($providerData[0]->add_waitlist == 'No') {echo "checked";}?>>
                                                    <label class="custom-control-label" for="addwaitlistno">No thanks</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="addwaitlistown" name="add_waitlist" value="Own" class="custom-control-input" <?php if ($providerData[0]->add_waitlist == 'Own') {echo "checked";}?>>
                                                    <label class="custom-control-label" for="addwaitlistown">Add my own waitlist link</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="waitlistdiv">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Waitlist Link (external)</label>
                                                        <div class="input-group">
                                                            <div class="input-group-append">
                                                                <div class="input-group-text">http://</div>
                                                            </div>
                                                            <input type="text" class="form-control" name="waitlist_link" placeholder="Waitlist Link" value="<?php echo $providerData[0]->waitlist_link; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Waitlist Fee</label>
                                                        <input type="text" class="form-control" name="waitlist_fee" placeholder="Waitlist Fee" value="<?php echo $providerData[0]->waitlist_fee; ?>">
                                                        <small class="form-text text-muted">$ Numeric Value</small>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-12 form-group">
                                                            <label>Waitlist Policy</label>
                                                            <textarea class="form-control" name="waitlist_policy" rows="5"><?php echo $providerData[0]->waitlist_policy; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <input type="submit" value="Submit" name="updateWaitlist" class="btn btn-info">
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="gallery" aria-labelledby="gallery-tab">
                        <div class="myaccount-area pt-50 pb-50">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="section-title mb-5">
                                        <h2>Gallery</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card mb-4">
                                        <div class="card-body upolad-body">
                                            <form method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="banner_provider_id" value="<?php echo $providerData[0]->provider_id; ?>">
                                                <label>Upload Banner<i class="required">*</i></label>
                                                <div class="input-group mb-2">
                                                    <input type="file" name="banner[]" id="CustomGalaryFile" class="custom-file-input" multiple="" required="">
                                                    <div class="input-group-append">
                                                        <input type="submit" value="Submit" name="uploadBanner" class="btn btn-info">
                                                    </div>
                                                </div>
                                                <div class="hint text-bold">Hint : For better resolution upload 1920 X 1080</div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <?php
if ($galleryData) {
	foreach ($galleryData as $banner) {?>
<div class="col-lg-3 col-md-4">
                                    <div class="gallery-box">
                                        <div class="gallery-img" data-fancybox="gallery" href="assets/images/banner/slider-1.jpg">
                                            <img class="img-fluid" src="assets/images/banner/slider-1.jpg" alt="Thumb-1">
                                        </div>
                                        <button type="submit" class="btn btn-danger btn-md btn-block" data-toggle="tooltip" data-placement="top" title="Remove Image">Remove Image</button>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-4 gallery<?php echo $banner->banner_id; ?>">
                                    <div class="gallery-box">
                                        <div class="gallery-img" data-fancybox="gallery" href="upload_images/<?php echo $banner->banner_path; ?>">
                                            <img class="img-fluid" src="upload_images/<?php echo $banner->banner_path; ?>" alt="Thumb-1">
                                        </div>
                                        <button type="submit" class="btn btn-danger btn-md btn-block deleteBanner" id="<?php echo $banner->banner_id; ?>" data-toggle="tooltip" data-placement="top" title="Remove Image">Remove Image</button>
                                    </div>
                                </div>
                                <?php }} else {?>
                                   <fieldset class="mb-20">
                                   <button type="submit" class="btn btn-danger btn-md btn-block" data-placement="top" title="Remove Image">No Image available!</button></fieldset>
                                <?php }?>

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="changeemail" aria-labelledby="changeemail-tab"></div>
                    <div class="tab-pane fade" id="changepassword" aria-labelledby="changepassword-tab">..4.</div>
                    <div class="tab-pane fade" id="deleteprofile" aria-labelledby="deleteprofile-tab">..5.</div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inner Body :: End -->
<?php require_once USER_VIEW_PATH . 'footer.inc.php';?>
<!-- Delete Profile :: Start -->
<div class="modal custom-modal fade" id="DeleteProfileModal" tabindex="-1" role="dialog" aria-labelledby="DeleteProfileModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered sm-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="DeleteProfileModalLabel">Delete Profile</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
            </div>
            <div class="modal-body p-4">
            <form method="post" id="providerRemove">
                <h5>SORRY TO SEE YOU GO...</h5>
                <p>Are you sure you want to delete your account?</p>
                <input type="hidden" name="id" value="<?php echo $providerData[0]->provider_id; ?>">
                <input type="hidden" name="user_type" value="provider">
                <input type="hidden" name="action" value="submituserdata">
                <button type="submit" class="btn btn-primary">Yes</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </form>
        </div>
        </div>
    </div>
</div>

<div class="modal custom-modal fade" id="subPlan" tabindex="-1" role="dialog" aria-labelledby="subPlanLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered sm-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="subPlanLabel">Add Subcriptions</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
            </div>
            <div class="modal-body p-4">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" id="subscribePlan">
                        <div class="col-md-12 form-group">
                            <label>Select Premium Plan</label>
                            <select class="form-control" name="plan" id="plan">
                                <option value="">Select Plan</option>
                                <option value="0">$<?php echo PRONAN_MONTH_PRICE; ?> / Month</option>
                                <option value="1">$<?php echo PRONAN_YEAR_PRICE; ?> / Year</option>
                            </select>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>Card Number</label>
                            <input type="text" class="form-control" id="card_number" name="card_number" placeholder="Card Number" required />
                            <div id="card-suggesstion-box"></div>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>Expiry Month / Year</label>
                            <input type="text" class="form-control" id="card_exp" name="card_exp" placeholder="Month / Year" required />
                        </div>
                        <div class="col-md-12 form-group">
                            <label>CVC Code</label>
                            <input type="text" class="form-control" id="card_cvc" name="card_cvc" placeholder="CVC Code" required />
                        </div> 

                        <div class="col-md-12 form-group">
                            <label>Card Holder Name</label>
                            <input type="text" class="form-control" id="c_name" name="c_name" placeholder="Card Holder Name" required />
                        </div>

                        <div class="col-md-12 form-group">
                            <label>Postal Code</label>
                            <input type="text" class="form-control" id="post_code" name="post_code" placeholder="Postal Code" required />
                        </div>

                        <input type="hidden" name="subscribe_plan" id="subscribe_plan" value="subscribe_plan">
                        <div class="col-md-12 form-group">
                            <input type="submit" class="btn btn-primary" id="updatePlan" name="updatePlan" value="Subscribe" />
                        </div> 
                    </form>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>

<!-- Delete Profile :: End -->
<!-- Change Password :: Start -->
<div class="modal custom-modal fade " id="ChangePasswordModal" tabindex="-1" role="dialog" aria-labelledby="ChangePasswordModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered sm-modal" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="ChangePasswordModalLabel">Change Password</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
        </div>
        <div class="modal-body p-4">
            <p>Please complete the form below to change your <b>DayCare.com</b> Password.</p>
            <form method="post" id="careChangepassword">
                <div class="form-group">
                    <label>Current Password</label>
                    <input type="password" class="form-control" name="currentpassword" placeholder="Current Password" required="">
                </div>
                <div class="form-group">
                    <label>New Password</label>
                    <input type="password" id="password" class="form-control" name="newpassword" placeholder="New Password" required="">
                </div>
                <div class="form-group">
                    <label>Confirm Password</label>
                    <input type="password" class="form-control" name="confirmpassword" id="confirmpassword" placeholder="Confirm Password" required="">
                </div>

                <input type="hidden" name="id" value="<?php echo $providerData[0]->provider_id; ?>">
                <input type="hidden" name="user_type" value="provider">
                <input type="hidden" name="action" value="userpasswordchange">
                <button type="submit" class="btn btn-primary">Change</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <div style="color: red;" id="divCheckPasswordMatch">
            </form>

        </div>
    </div>
</div>
</div>
<!-- Change Password :: End -->
<script src="<?php echo HOME_URL; ?>assets/js/plugin/buttons.flash.min.js" type="text/javascript"></script>
<script src="<?php echo HOME_URL; ?>assets/js/plugin/buttons.html5.min.js" type="text/javascript"></script>
<script src="<?php echo HOME_URL; ?>assets/js/plugin/buttons.print.min.js" type="text/javascript"></script>
<script src="<?php echo HOME_URL; ?>assets/js/plugin/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<?php echo HOME_URL; ?>assets/js/plugin/jszip.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw==" crossorigin="anonymous"></script>

<script>
    $(document).on('submit', '#subscribePlan', function(event){
        event.preventDefault();
        /*$(document).on('click', '#updatePlan', function (event) {*/
        var postData = {
                plan: $("#plan").val(),
                card_number: $("#card_number").val(),
                card_exp: $("#card_exp").val(),
                card_exp: $("#card_exp").val(),
                card_cvc: $("#card_cvc").val(),
                c_name: $("#c_name").val(),
                post_code: $("#post_code").val(),
                provider_id: '<?php echo $providerData[0]->provider_id; ?>',
                subscribe_plan: $("#subscribe_plan").val(),
            };
        $.ajax({
            type: "POST",
            url: "ajax/validCard.php",
            data:postData,
            success: function(data){
                var res = $.parseJSON(data);
                $("#subPlan").modal('hide');
                if(res.success == 0){
                    Swal.fire(
                        'Failed!',
                        res.token,
                        'error'
                    ).then((result) => {
                      location.reload();
                    });
                }
                if(res.success == 1){
                    Swal.fire(
                        'Success!',
                        res.token,
                        'success'
                    ).then((result) => {
                      location.reload();
                    });
                }
            }
        });
        /*});*/
    });

function checkPasswordMatch() {
    var password = $("#password").val();
    var confirmPassword = $("#confirmpassword").val();
    if (password != confirmPassword){
        $("#divCheckPasswordMatch").text("Passwords do not match!");
    }
    else{
        $("#divCheckPasswordMatch").text("Passwords match.");
    }
}
</script>
<script>
    $(document).ready(function () {
        $('#card_number').mask('0000-0000-0000-0000');
        $('#card_exp').mask('00/00');
        $("#confirmpassword").keyup(checkPasswordMatch);
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#schemeDetails').css('display', 'none');
        $('#VacanciesPostSelectDate-box').css('display', 'none');
        $('#ChildcareCentreFees-area').css('display', 'none');
        $('#ChildcareCentreYearFees-area').css('display', 'none');
        $('#OccasionalCareCentreFees-area').css('display', 'none');
        $('#FamiltDayCareFees-area').css('display', 'none');
        $('#PreschoolFees-area').css('display', 'none');
        $('#NotRequiredFamilyDaycareFees-area').css('display', 'none');
        $('#BeforeAfterSchoolFees-area').css('display', 'none');
        $('#OccasionalCasualCentresFees-area').css('display', 'none');

        $("input[name='familydaycare']").click(function () {
            if ($("#familydaycare").is(":checked")) {
                $("#schemeDetails").show();
                $("#longdaycare").attr('disabled', true);
                $("#occasionalcare").attr('disabled', true);
                $("#preschool").attr('disabled', true);
            } else {
                $("#schemeDetails").hide();
            }
        });

        $("input[name='vacancy_start_on']").click(function () {
            if ($("#VacanciesPostSelectDate").is(":checked")) {
                $("#VacanciesPostSelectDate-box").show();
                $("#Vacanciesvacancy_start_on").val('on');
            } else {
                $("#VacanciesPostSelectDate-box").hide();
                $("#Vacanciesvacancy_start_on").val('Now');
            }
        });

        $("input[name='ChildcareCentreFees']").click(function () {
            if ($("#ChildcareCentreFees").is(":checked")) {
                $("#ChildcareCentreFees-area").show();
            } else {
                $("#ChildcareCentreFees-area").hide();
            }
        });

        $("input[name='ChildcareCentreYearFees']").click(function () {
            if ($("#ChildcareCentreYearFees").is(":checked")) {
                $("#ChildcareCentreYearFees-area").show();
            } else {
                $("#ChildcareCentreYearFees-area").hide();
            }
        });

        $("input[name='OccasionalCareCentreFees']").click(function () {
            if ($("#OccasionalCareCentreFees").is(":checked")) {
                $("#OccasionalCareCentreFees-area").show();
            } else {
                $("#OccasionalCareCentreFees-area").hide();
            }
        });

        $("input[name='FamiltDayCareFees']").click(function () {
            if ($("#FamiltDayCareFees").is(":checked")) {
                $("#FamiltDayCareFees-area").show();
            } else {
                $("#FamiltDayCareFees-area").hide();
            }
        });

        $("input[name='PreschoolFees']").click(function () {
            if ($("#PreschoolFees").is(":checked")) {
                $("#PreschoolFees-area").show();
            } else {
                $("#PreschoolFees-area").hide();
            }
        });

        $("input[name='BeforeAfterSchoolFees']").click(function () {
            if ($("#BeforeAfterSchoolFees").is(":checked")) {
                $("#BeforeAfterSchoolFees-area").show();
            } else {
                $("#BeforeAfterSchoolFees-area").hide();
            }
        });

        $("input[name='NotRequiredFamilyDaycareFees']").click(function () {
            if ($("#NotRequiredFamilyDaycareFees").is(":checked")) {
                $("#NotRequiredFamilyDaycareFees-area").show();
            } else {
                $("#NotRequiredFamilyDaycareFees-area").hide();
            }
        });

        $("input[name='OccasionalCasualCentresFees']").click(function () {
            if ($("#OccasionalCasualCentresFees").is(":checked")) {
                $("#OccasionalCasualCentresFees-area").show();
            } else {
                $("#OccasionalCasualCentresFees-area").hide();
            }
        });
    });
</script>
<script>
$(document).on('submit', '#careChangepassword', function(event){
event.preventDefault();
$.ajax({
    url:"ajax/userPasswordupdate.php",
    method:'POST',
    data:new FormData(this),
    contentType:false,
    processData:false,
    success:function(data)
    {
      $('#careChangepassword')[0].reset();
      $('#ChangePasswordModal').modal('hide');
      if(data == 'success-provider'){
        Swal.fire(
            'Success!',
            'Your Password has been changed.',
            'success'
        )
      }else{
        Swal.fire(
          'Failed!','Somthing goes wrong.','error'
        ).then((result) => {
          // Reload the Page
          //window.location.replace("<?php echo $_SERVER['PHP_SELF']; ?>");
        });
      }

    }
});
});
</script>
<script>
    $("#success-alert").fadeTo(2000, 900).slideUp(500, function(){
    $("#success-alert").slideUp(900);
    <?php $_SESSION['add_msg'] = '';?>
});
</script>
<script>
    $(document).on('click', '.deleteBanner', function(){
    var banner_id = $(this).attr("id");
    if(confirm("Are you sure you want to delete this?"))
    {
      $.ajax({
        url:"ajax/deleteBanner.php",
        method:"POST",
        data:{banner_id:banner_id,action:'bannerDelete'},
        success:function(data)
        {
            $(".gallery"+banner_id).hide();
            Swal.fire(
              'Removed!','Data Removed successfully.','success'
            ).then((result) => {
              // Reload the Page
              //window.location.replace("<?php echo $_SERVER['PHP_SELF']; ?>");
            });
        }
      });
    }
    else
    {
      return false;
    }
  });
</script>
<script>
    $(document).on('click', '.deleteVacancy', function(){
    var vancancy_id = $(this).attr("id");
    if(confirm("Are you sure you want to delete this?"))
    {
      $.ajax({
        url:"ajax/deleteVacancy.php",
        method:"POST",
        data:{vancancy_id:vancancy_id,action:'vancayDelete'},
        success:function(data)
        {
            Swal.fire(
              'Removed!','Data Removed successfully.','success'
            ).then((result) => {
              // Reload the Page
              window.location.replace("<?php echo $_SERVER['PHP_SELF']; ?>");
            });
        }
      });
    }
    else
    {
      return false;
    }
  });
</script>
<script>
    $(document).on('click', '.deleteCarefees', function(){
    var fees_id = $(this).attr("id");
    if(confirm("Are you sure you want to delete this?"))
    {
      $.ajax({
        url:"ajax/deleteCarefees.php",
        method:"POST",
        data:{fees_id:fees_id,action:'carefeesDelete'},
        success:function(data)
        {
            Swal.fire(
              'Removed!','Data Removed successfully.','success'
            ).then((result) => {
              // Reload the Page
              window.location.replace("<?php echo $_SERVER['PHP_SELF']; ?>");
            });
        }
      });
    }
    else
    {
      return false;
    }
  });
</script>
<script>
$(document).on('submit', '#providerRemove', function(event){
event.preventDefault();
$.ajax({
    url:"ajax/deactiveAccount.php",
    method:'POST',
    data:new FormData(this),
    contentType:false,
    processData:false,
    success:function(data)
    {
      $('#providerRemove')[0].reset();
      //$('#loginModal').modal('hide');
      if(data == 'success-provider'){
        window.location.replace("index.php");
      }else{
        Swal.fire(
          'Failed!','Somthing goes wrong.','error'
        ).then((result) => {
          // Reload the Page
          //window.location.replace("<?php echo $_SERVER['PHP_SELF']; ?>");
        });
      }

    }
});
});
</script>
<script>
$(function() {
   $("input[name='add_waitlist']").click(function() {
     if ($("#addwaitlistno").is(":checked")) {
       $("#waitlistdiv").hide();
     } else {
       $("#waitlistdiv").show();
     }
   });
 });
</script>
<script type="text/javascript">
    $(function () {
        var countt = 1;
        $("#btnAdd").bind("click", function () {
            var div = $("<tr />");
            div.html(GetDynamicTextBox(countt++));
            $("#TextBoxContainer").append(div);
        });
        $("#btnGet").bind("click", function () {
            var values = "";
            $("input[name=DynamicTextBox]").each(function () {
                values += $(this).val() + "\n";
            });
            //alert(values);
        });
        $("body").on("click", "#removefield", function () {
            $(this).closest("#custom-field").remove();
            countt--;
        });
    });
    function GetDynamicTextBox(values) {
        return '<tr class="dynamic-tr" id="custom-field"><td data-title="#" class="text-center">' + values + '</td><td data-title="Age Group"><div class="agegroup-td d-flex"><div class="tb-inline"><select class="form-control mr-2 w-70" name="from_age[]" required><option value="">--</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select><select class="form-control w-110" name="from_age_type[]" required><option value="">--</option><option value="weeks">Weeks</option><option value="months">Months</option><option value="years">Years</option></select></div><span class="td-divider">TO</span><div class="tb-inline"><select class="form-control mr-2 w-70" name="to_age[]" required><option value="">--</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select><select class="form-control w-110" name="to_age_type[]" required><option value="">--</option><option value="weeks">Weeks</option><option value="months">Months</option><option value="years">Years</option></select></div></div></td><td data-title="Days"><div class="selectday-area"><div class="btn-group btn-group-toggle" id="DaysRequired" data-toggle="buttons"><label class="btn btn-toggle"><input type="checkbox" name="days' + values + '[]" value="Sunday"> Sun</label><label class="btn btn-toggle"><input type="checkbox" name="days' + values + '[]" value="Monday"> Mon</label><label class="btn btn-toggle"><input type="checkbox" name="days' + values + '[]" value="Tuesday"> Tue</label><label class="btn btn-toggle"><input type="checkbox" name="days' + values + '[]" value="Wednesday"> Wed</label><label class="btn btn-toggle"><input type="checkbox" name="days' + values + '[]" value="Thursday"> Thu</label><label class="btn btn-toggle"><input type="checkbox" name="days' + values + '[]" value="Friday"> Fri</label><label class="btn btn-toggle"><input type="checkbox" name="days' + values + '[]" value="Saturday"> Sat</label></div></div></td><td class="text-center"><input type="date" name="start_from[]"></td><td class="text-center" data-title="Action"><a id="removefield" class="remove-agegroup"><i class="fa fa-minus-square"></i></a></td></tr>';
    }
</script>
<script type="text/javascript">
    // function valueChanged()
    // {
    //     if($('#novacancies').is(":checked"))
    //         $("#vacancyinfo").hide();
    //     else
    //         $("#vacancyinfo").show();
    // }
    $(document).ready(function() {
        $('#novacancies').change(function() {
            if(this.checked) {
                $("#is_vacancy").val('No');
                $("#vacancyinfo").hide();
            }else{
                $("#is_vacancy").val('Yes');
                $("#vacancyinfo").show();
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).on('click', '.acceptdata', function(){
        var waitlist_id = $(this).attr("id");
        var status = 'accept';
        //alert(user_id);
        Swal.fire({
            title: 'Are you sure?',
            text: "Once Accepted, you will not be able to recover this user data!",
            icon: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Yes, Change it!'
        }).then((result) => {
        if (result.value) {
            $.ajax({
                url:"ajax/acceptRequestdata.php",
                method:"POST",
                data:{waitlist_id:waitlist_id,status:status,action:'waitlistaccept'},
                success:function(data)
                {
                  window.location.replace("account-Provider.php");
                }
            });
        }
    })
});
</script>
<script type="text/javascript">
    $(document).on('click', '.declinedata', function(){
        var waitlist_id = $(this).attr("id");
        var status = 'decline';
        //alert(user_id);
        Swal.fire({
            title: 'Are you sure?',
            text: "Once Accepted, you will not be able to recover this user data!",
            icon: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Yes, Change it!'
        }).then((result) => {
        if (result.value) {
            $.ajax({
                url:"ajax/acceptRequestdata.php",
                method:"POST",
                data:{waitlist_id:waitlist_id,status:status,action:'waitlistaccept'},
                success:function(data)
                {
                  window.location.replace("account-Provider.php");
                }
            });
        }
    })
});
</script>
<script>
// AJAX call for autocomplete
$(document).ready(function(){
    $("#search-box").keyup(function(){
        $.ajax({
        type: "POST",
        url: "ajax/readPostcode.php",
        data:'keyword='+$(this).val(),
        beforeSend: function(){
            $("#search-box").css("background","#FFF url(assets/images/LoaderIcon.gif) no-repeat 165px");
        },
        success: function(data){
            $("#suggesstion-box").show();
            $("#suggesstion-box").html(data);
            $("#search-box").css("background","#FFF");
        }
        });
    });
});
//To select country name
function selectCountry(val) {
    $("#search-box").val(val);
    $("#suggesstion-box").hide();
}
</script>
<script>
// AJAX call for autocomplete
$(document).ready(function(){
    $("#search-box1").keyup(function(){
        $.ajax({
        type: "POST",
        url: "ajax/readCity.php",
        data:'keyword='+$(this).val(),
        beforeSend: function(){
            $("#search-box1").css("background","#FFF url(assets/images/LoaderIcon.gif) no-repeat 165px");
        },
        success: function(data){
            $("#suggesstion-box1").show();
            $("#suggesstion-box1").html(data);
            $("#search-box1").css("background","#FFF");
        }
        });
    });
});
//To select country name
function selectCountry1(val) {
    $("#search-box1").val(val);
    $("#suggesstion-box1").hide();
}
</script>
<script>
    $('#CustomGalaryFile').filestyle({
        input: false,
        buttonName: 'btn-default',
        iconName: 'fas fa-file-image'
    });

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZPcppwWN179bx8Asxh8Xsd-ZiLIvZNMM&libraries=places"></script>

<script type="text/javascript">
    var placeSearch, autocomplete;

    function initialize() {
        autocomplete = new google.maps.places.Autocomplete(
        (document.getElementById('address')), {
            types: ['geocode']
        });
        google.maps.event.addDomListener(document.getElementById('address'), 'focus', geolocate);

        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            console.log(place.address_components);
            var res = new Array();
            for(var k=0; k < place.address_components.length; k++){
                if(place.address_components[k].types.indexOf('postal_code') >= 0){
                    res['postal_code'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('locality') >= 0){
                    res['suberb'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('sublocality_level_2') >= 0){
                    res['address_line1'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('sublocality_level_1') >= 0){
                    res['address_line2'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('administrative_area_level_1') >= 0){
                    res['state'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('administrative_area_level_2') >= 0){
                    res['city'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('country') >= 0){
                    res['country'] = place.address_components[k].short_name;
                }

            }
            $("#postcode").val(res['postal_code']);
            $("#suburb").val(res['suberb']);
            $("#city").val(res['city']);
            $("#country").val(res['country']);
        });
    }

    function geolocate() {

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                
                $("#lat").val(position.coords.latitude);
                $("#long").val(position.coords.longitude);

                var geolocation = new google.maps.LatLng(
                position.coords.latitude, position.coords.longitude);
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }

    window.addEventListener('load', (event) => {
        initialize();
    });
</script>
<?php require_once USER_VIEW_PATH . 'frontouter.inc.php';?>