<?php
$pageName = 'home';
require_once USER_VIEW_PATH . 'header.inc.php';?>

<!-- Inner Body :: Start -->
<section class="innerbody-section pt-50 pb-50">
    <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 mb-md-30"></div>
                <div class="col-lg-6 col-md-6 mb-md-30">

                <h3 class="mb-10">Dashboard Login</h3>
                <form id="userlogin" method="post">
                    <div class="form-group">
                        <label>I am ..</label>
                        <div class="input-group rm-bg mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="ti-user"></i></div>
                            </div>
                            <select class="form-control" name="user_type" required="">
                                <option value="">---</option>
                                <option value="parent">Parent</option>
                                <option value="care_provider">Care Provider</option>
                                <option value="nanny">Nanny / Baby Sitter</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <div class="input-group rm-bg mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="ti-email"></i></div>
                            </div>
                            <input type="email" class="form-control" name="email" placeholder="Email Address" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <div class="input-group rm-bg mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="ti-lock"></i></div>
                            </div>
                            <input type="password" class="form-control" name="password" placeholder="Password" required="">
                        </div>
                    </div>
                    <div class="clearfix mb-3">
                        <div class="checkboxes float-left">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="rememberme">
                                <label class="custom-control-label" for="rememberme">Remember me</label>
                            </div>
                        </div>
                        <div class="float-right"><a href="javascript:void(0)" class="leftlink" data-toggle="modal" data-target="#ForgotPasswordModal" data-dismiss="modal">Forgot Password?</a></div>
                    </div>
                    <input type="hidden" name="action" value="submituserdata">
                    <button class="btn btn-primary btn-block" type="submit">Login</button>
                    <div class="text-center mt-2">
                        Don’t have an account? <a href="sign-up.php" class="link">Sign up</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- Inner Body :: End -->
<?php require_once USER_VIEW_PATH . 'footer.inc.php';?>
<?php require_once USER_VIEW_PATH . 'frontouter.inc.php';?>