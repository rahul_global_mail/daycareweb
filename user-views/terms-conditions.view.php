<?php
$pageName = 'home';
require_once USER_VIEW_PATH . 'header.inc.php';?>
<!-- Inner Banner :: Start -->
<section class="inner-banner">
    <div class="inner-content text-center">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <h1 class="page-title">Terms & Conditions of Use</h1>
                </div>
                <div class="col-12 col-sm-12 col-md-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                        </li>
                        <li class="active">Terms & Conditions</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inner Banner :: End -->
<!-- Inner Body :: Start -->
<section class="innerbody-section pt-50 pb-50">
    <div class="container">
        <div class="row mb-50">
            <div class="col-md-12">
                <h4 class="inner-title-t2 mb-2">User Agreement</h4>
                <p>Please read this agreement carefully before accessing or using this service. This agreement sets out the terms and conditions under which you may use the website Daycareseekers.com. By accessing or using this service, you agree to be bound by the terms and conditions below. DaycareSeekers.com may revise these terms and conditions at any time by updating this posting. You should visit this page periodically to review the terms and conditions as your continued use of this Website following the posting of any changes to the Terms and Conditions constitutes acceptance of those changes and will be binding on you.</p>
                <h4 class="inner-title-t2 mb-2">Definitions And Interpretation</h4>
                <p class="mb-1"><b>In this Agreement:</b></p>
                <ul class="list-dott">
                    <li>‘Agency’ means an organisation which acts as an intermediary between Parents and Carers.</li>
                    <li>‘Booking Fee' means the fee charged by Agencies at the time you book a Carer.</li>
                    <li>'Carers' includes babysitters, nannies, carers, and au-pairs.</li>
                    <li>'Child Care Provider' includes creches, pre-schools, kindergartens, family day care, occasional care centres, before and/or after school care, vacation care and long day care centres.</li>
                    <li>‘Material’ means ' texts, graphics, images, photos, video, html codes and ‘other material Parent' includes guardian and stepparent.</li>
                    <li>'Website' means Daycareseekers.com.</li>
                    <li>'Waitlist Fee' means the fee charged by Child Care Providers to place your child(ren)'s name(s) on a waiting list for enrolments.</li>
                    <li>'You' and 'User' means any individual or entity accessing this Website for any reason.</li>
                </ul>
                <p class="mb-1"><b>In this Agreement, unless the context otherwise requires:</b></p>
                <ul class="list-dott">
                    <li>reference to a person includes any other entity recognised by law and vice versa;</li>
                    <li>words importing the singular number include the plural number and vice versa;</li>
                    <li>words importing one gender include every gender;</li>
                    <li>any reference to any of the parties by the defined terms include that parties' executors, administrators or permitted assigns or being a company its successors or permitted assigns;</li>
                    <li>references to currency are in Canadian Dollars;</li>
                    <li>headings are for convenience only and do not affect interpretation.</li>
                </ul>
                <h4 class="inner-title-t2 mb-2">Password And Login</h4>
                <p>When you become a registered user, you are provided with a password and login name. You agree to be entirely responsible for any and all activities that occur under your login name.</p>
                <p>You are entirely responsible for maintaining the confidentiality of your password and login. You may change your password at any time by following instructions published on Daycareseekers.com.</p>
                <p>You agree to immediately notify DaycareSeekers.com of any unauthorised use of your login name or any other breach of security known to you.</p>
                <h4 class="inner-title-t2 mb-2">Venue Only</h4>
                <p>Daycareseekers.com is a venue only and acts as a communications platform for enabling the connection between prospective employers of Carers ('Parents') and candidates seeking to provide child care services or Child Care Providers. DaycareSeekers.com does not screen or censor the listings offered. DaycareSeekers.com is not involved in the actual transaction between Parents and Carers or Child Care Providers. All users including Parents, Child Care Providers and Carers do hereby represent, understand and expressly agree that DaycareSeekers.com does not have control over the quality, timing, safety, legality or any other aspect whatsoever of the truth or accuracy of the information posted, the services actually delivered by the Carers, Child Care Providers or Agencies nor the integrity, or responsibility for any of the actions whatsoever of the Parents.</p>
                <p>Both Parents and Carers acknowledge that there is no employee-employer relationship or independent contractor relationship between the Carers and DaycareSeekers.com.</p>
                <p>This Website provides a listing of Child Care Providers located throughout Canada. We do not represent that this Website provides a complete list of Child Care Providers in a particular area although Child Care Providers are welcome to list themselves on the Website free of charge. This listing is a free information service for our Users only and these Child Care Providers have not been screened or endorsed by DaycareSeekers.com. Vacancy information is provided by Child Care Providers on a voluntary and self-service basis. In addition, our Website enables Parents to join the waiting list for some of these Child Care Providers upon payment of any applicable Waitlist Fee. Waitlist Fees are set by Child Care Providers and are beyond the control of DaycareSeekers.com. You should make your own enquiries before completing the waitlist application or using the services of any of the Child Care Providers listed on the Website, as DaycareSeekers.com makes no representations about the accuracy of the information provided, or the quality or availability of the services they provide. DaycareSeekers.com has no influence over or any means of ensuring that Child Care Providers consider Vacancy Alerts and Waitlists provided through the Website in allocating any available places at their service, and Parents should contact the relevant Child Care Providers directly to ensure their position on any Waitlist.  Whilst Vacancy Alert is a tool offered to Parents to assist in their child care search, it should not be relied upon as a Parent's sole avenue of locating available child care.</p>
                <h4 class="inner-title-t2 mb-2">No Representations Or Warranties</h4>
                <p>DaycareSeekers.com does not provide any representations or warranties relating to the Carers or the service provided by the Carers. DaycareSeekers.com provides a referral service only and background reference checks or investigations related to Carers or the service to be provided by the Carers or Child Care Providers and Agencies are the sole responsibility of the Parent.</p>
                <p>Any enquiries regarding the wages and conditions of Carers, or information regarding background reference check should be referred to the Agency or Child Care Provider concerned.</p>
                <p>Further, DaycareSeekers.com makes no representations or warranties as to the online availability of its website and database. DaycareSeekers.com will not be responsible in any way for the availability of the website services and information.</p>
                <p>DaycareSeekers.com will not moderate any information posted by any User on the Website except for circumstances where information is drawn to the attention of DaycareSeekers.com as inappropriate by a User.</p>
                <p>Information published by DaycareSeekers.com on the website is intended to provide general information only.  DaycareSeekers.com does not warrant the accuracy, reliability, currency, or completeness of such information. You agree that DaycareSeekers.com will not be liable to You or anyone else for any loss resulting from any decision made or action taken by you or anyone else in reliance upon any information or material posted on this Website.</p>
                <p>Not all Child Care Service Providers may be represented in our referral services Child Care Connect and Child Care Concierge.</p>
                <h4 class="inner-title-t2 mb-2">Fees Payable To Daycareseekers.com</h4>
                <p>Fees payable to DaycareSeekers.com are payable on a pre-paid basis.</p>
                <p class="mb-1"><b>DaycareSeekers.com subscription services which include our Spot Alert and Premium service are for a fixed term and:</b></p>
                <ul class="list-dott">
                    <li>unless otherwise specified, payable on a pre-paid basis; and</li>
                    <li>are not transferable or refundable.</li>
                </ul>
                <p class="mb-1"><b>Without limiting our other rights and remedies at law, in the event of late or non-payment, DaycareSeekers.com may:</b></p>
                <ul class="list-dott">
                    <li>suspend or remove your subscription to the Website;</li>
                    <li>suspend or cancel the Services being supplied;</li>
                    <li>charge you for any legal, agency, and/or court fees associated with collection of amounts overdue and/or</li>
                    <li>terminate this Agreement.</li>
                </ul>
                <h4 class="inner-title-t2 mb-2">Fees Charged By Agencies, Carers And Child Care Providers</h4>
                <p>The Fee charged by child care providers is determined by them and the Booking Fee and Carer Rates charged is determined by each individual Agency (representing a Carer) and not DaycareSeekers.com. The Carer's fees are determined by, and payable to each individual Carer.</p>
                <p>Some Agencies have entered into an agreement with DaycareSeekers.com for the payment of Booking Fee to be made to DaycareSeekers.com. In this circumstance, DaycareSeekers.com collects the fee and then pays a transaction fee to the Agency. In other circumstances Agencies have entered into an agreement with DaycareSeekers.com for the payment of fees to be made directly into their own payment system and merchant facility. Neither of these arrangements affects the acknowledgments made elsewhere in this agreement regarding the legal relationship between the Parents, Carers and DaycareSeekers.com.</p>
                <p class="mb-1"><b>If you have a concern or dispute regarding:</b></p>
                <ul class="list-dott">
                    <li>The fees charged to you as a Parent (excluding the Booking Fee);</li>
                    <li>the service provided by the Carer or Child Care Provider; or</li>
                    <li>the fees paid to you as a Carer, then you should direct those concerns to the relevant Agency or Child Care Provider.</li>
                </ul>
                <p class="mb-1"><b>If you have a concern or dispute regarding the Booking Fee charged to you then the following procedures are to be followed:</b></p>
                <ul class="list-dott">
                    <li>If you paid the Agency and/or Child Care Provider directly (and this will be obvious at the time you made the payment) then you will need to contact the Child Care Provider directly. You should review the refund policy at the time of booking for the Child Care Provider or Agency representing the Carer you have selected as you will be bound by that policy. You acknowledge that DaycareSeekers.com has no control or influence over this policy or its implementation and is unable to intervene in disputes on your behalf or accept any liability in respect of such policy.</li>
                    <li>If you paid DaycareSeekers.com, then you should contact DaycareSeekers.com who shall be the final arbiter of the disputes concerning the refund of the Booking Fee and you agree to release DaycareSeekers.com, its successors and assigns, as well as its officers and agents, from all actions, suits, claims, demands and causes of action whatsoever at law, in equity and under statute which they may have, or but for this Agreement would or might at any time hereafter have or have had against DaycareSeekers.com in respect of, arising out of the exercise by DaycareSeekers.com of its discretion stated herein.</li>
                    <li>You acknowledge that fees published by Child Care Service providers may be subject to change without notice.</li>
                </ul>
                <h4 class="inner-title-t2 mb-2">Risk</h4>
                <p>It is noted that there are risks, including but not limited to the risk of physical harm, of dealing with strangers, underage persons, or people acting under false pretences. You assume all risks associated with dealing with other users with whom you come in contact through DaycareSeekers.com.</p>
                <p>All Users including both parents and Carers do hereby expressly agree not to hold DaycareSeekers.com (or its agents, assigns, representatives, advertisers or employees) liable for any instruction, advice or services delivered which originated through the Daycareseekers.com Website and releases DaycareSeekers.com from liability for any damages, suits, claims, and/or controversies that have arisen or may arise, whether known or unknown there from.</p>
                <h4 class="inner-title-t2 mb-2">Release From Liability</h4>
                <p>To the extent permitted by law, the Parent covenants and agrees to release DaycareSeekers.com, its successors and assigns, as well as its officers and agents, from all actions, suits, claims, demands and causes of action whatsoever at law, in equity and under statute which they may have, or but for this Agreement would or might at any time hereafter have or have had against DaycareSeekers.com in respect of, arising out of or resulting from the provision of any services to the Client by Carers, Agencies or  Child Care Providers from any other cause relating thereto, including but not limited to, the negligent acts of the Carers, Agencies or Child Care Providers.</p>
                <p>To the extent permitted by law, the Carer release DaycareSeekers.com, its successors and assigns, as well as its officers and agents, from all actions, suits, claims, demands and causes of action whatsoever at law, in equity and under statute which they may have, or but for this Agreement would or might at any time hereafter have or have had against DaycareSeekers.com in respect of, arising out of or resulting from the actions of DaycareSeekers.com or the release of the Carer's personal information on the Website.</p>
                <p>The Carers agree that the DaycareSeekers.com shall not be liable to the Carer or to any other person, for incidental or consequential losses, damages, or expenses, directly or indirectly arising from any action or omission by either a Parent, an Agency or DaycareSeekers.com.</p>
                <p>Users acknowledge that under no circumstances shall DaycareSeekers.com be liable for the Agency’s, Carer’s, or Child Care Provider’s fees.</p>
                <p>DaycareSeekers.com are not responsible for, and accept no liability with respect to, any Material uploaded, posted, transmitted, or otherwise made available on the Website by any person other than us. For the avoidance of doubt, we will not be taken to have uploaded, posted, transmitted, or otherwise made Material available on the Website simply by facilitating others to upload, post, transmit or otherwise make Material available.</p>
                <p>To the extent permitted by law, DaycareSeekers.com excludes all liability relating to your use of the Website or our products and services. To the extent that our liability cannot be excluded by law, our liability will be limited, at our option to the re-supply of our products or services, or the amount(s) paid by You (if any) for our products and services. In no circumstances will DaycareSeekers.com be liable to Users for any indirect, incidental, special, and/or consequential losses or damages (including loss of profits, goodwill, data, or opportunity).</p>
                <h4 class="inner-title-t2 mb-2">Privacy</h4>
                <p>The Privacy Act protects personal information about individuals handled by organisations. Personal information is information or an opinion that identifies an individual or allows their identity to be readily determined from the information. It includes information such as a person's name, address, financial information, marital status, or billing details. Some personal information, including information about ethnicity, religion and health is considered to be sensitive.</p>
                <p>You consent to allow DaycareSeekers.com to collect personal information about you, including information that is considered to be sensitive, so as to permit DaycareSeekers.com to carry out its services and activities.</p>
                <p>DaycareSeekers.com may use Application Programming Interface (API) software to send parent enquiries, request to tour, waitlists and Spot alert registrations to CRM systems managed by Child Care Providers in order for those systems to manage the enquiry/request to tour, waitlist or spot alert request. The API software may be the property of DaycareSeekers.com or a third party. You consent to the transmission of your information, including personal information in this way.</p>
                <p>Users who are Agencies or Child Care Providers undertake that they will only collect, hold, use and disclose personal information obtained through the Website or an API from leads generated by Parents submitting enquiries, for the sole purpose of contacting the person in connection with their enquiry.</p>
                <p>Please refer to our privacy policy for information about our collection, use and disclosure of personal information. The terms of our privacy policy from time to time form part of this agreement.</p>
                <h4 class="inner-title-t2 mb-2">Security</h4>
                <p>DaycareSeekers.com takes all reasonable steps to ensure the security of our systems. Any information which we hold for you is stored on secure servers that are protected in controlled facilities. In addition, our employees and the contractors who provide services relating to our information systems are obliged to respect the confidentiality of any personal information held by DaycareSeekers.com. However, DaycareSeekers.com will not be held responsible for any loss that may arise from unauthorised access to your personal information.</p>
                <p>Advertisers on Daycareseekers.com have been asked to comply with Canadian Privacy Principles with regard to their information handling practices. However, the use of any information given to advertisers is not within the control of DaycareSeekers.com and we cannot accept responsibility for the conduct of these Advertisers.</p>
                <p>This Website also contains links to other sites. DaycareSeekers.com is not responsible for the privacy practices or the content of such third-party websites.</p>
                <p>DaycareSeekers.com exercises all due care to ensure that your information is secure on its system, however, the possibility exists that this information could be unlawfully observed by a third party while the data is being transmitted over the internet or while stored on Daycareseekers.com. DaycareSeekers.com accepts no liability that may arise should any other individuals obtain the information you submit to this Website.</p>
                <h4 class="inner-title-t2 mb-2">Advertisers And Third Parties</h4>
                <p class="mb-1"><b>Third Party services and products either:</b></p>
                <ul class="list-dott">
                    <li>linked to this Website; and/or</li>
                    <li>advertised on this Website are the responsibility of the third party and their inclusion on this website does not imply endorsement by DaycareSeekers.com.</li>
                </ul>
                <p>DaycareSeekers.com is not responsible for the content of Third-Party websites or advertisements and does not make any representations regarding the content or accuracy of materials on such Third-Party websites or advertisements. DaycareSeekers.com will not be liable for any damages or loss arising in any way out of or in connection with any information or third-party service provided.</p>
                <h4 class="inner-title-t2 mb-2">Copyright And Intellectual Property</h4>
                <p>This Website contains Material, which is protected by Canadian and international copyright and trademark Laws and remain the property of DaycareSeekers.com. You may not sell or modify the Material or copy, reproduce, display, upload, distribute or otherwise use the Material in any way for any public or commercial purpose. You may download Material from this Website for your personal, non-commercial use provided you keep intact all copyright and other proprietary notices.</p>
                <h4 class="inner-title-t2 mb-2">No Guarantees As To Service</h4>
                <p>DaycareSeekers.com does not warrant that the Website will operate error-free or that the website and its server are free of computer viruses or other harmful mechanisms. DaycareSeekers.com will not be responsible if your use of the Website or of the Material results in the need for servicing or replacing of equipment or data.  You must take your own precautions to ensure that your access to this Website does not expose you to the risk of viruses, malicious computer code or other forms of interference.</p>
                <h4 class="inner-title-t2 mb-2">Website Usage</h4>
                <p class="mb-1"><b>Users are prohibited from violating or attempting to violate the security of the Website, including, without limitation:</b></p>
                <ul class="list-dott">
                    <li>accessing data not intended for such User;</li>
                    <li>logging into a server or account which the User is unauthorised to access;</li>
                    <li>attempting to test the vulnerability of a system or network or to breach authentication processes;</li>
                    <li>attempting to interfere with a service to any user, host or network by submitting a virus to the Website, 'spamming' overloading, 'crashing' or otherwise engage in conduct which causes a threatened or actual nuisance to Users or to the Website;</li>
                    <li>Sending unsolicited e-mail or correspondence, including advertising or promotional material; or</li>
                    <li>Forging any TCP/IP packet header or any part of the header information in any e-mail or testimonial posting.</li>
                </ul>
                <p class="mb-1"><b>Users may not use the Website in order to post, transmit, distribute, store, or destroy material:</b></p>
                <ul class="list-dott">
                    <li>in violation of any applicable law or regulation;</li>
                    <li>in a manner which will infringe the copyright, trademark, or other intellectual property rights of others;</li>
                    <li>which violates the privacy, publicity, or other personal rights of others;</li>
                    <li>that is defamatory, obscene, threatening, abusive, hateful, or embarrassing to another User or any other person or entity; including, without limitation Carers or Child Care Services;</li>
                    <li>which comprises a chain letter or pyramid scheme;</li>
                    <li>which misleads or may mislead other Users of the Website or any other person or entity as to the true identity of the User posting, transmitting, distributing, storing, or destroying the relevant material;</li>
                    <li>that promotes discrimination based on race, sex, religion, nationality, disability, sexual orientation, or age;</li>
                    <li>which may be likely to deceive any person;</li>
                    <li>which may infringe any copyright, or any intellectual property rights of any person;</li>
                    <li>which is used to impersonate any person, or to misrepresent your identity or affiliation with any person.</li>
                </ul>
                <h4 class="inner-title-t2 mb-2">Ratings And Reviews</h4>
                <p class="mb-1"><b>DaycareSeekers.com values user feedback, and in particular encourages parents to post testimonials, ratings, and reviews on the Website about their experiences. You acknowledge and agree that:</b></p>
                <ul class="list-dott">
                    <li>The testimonials, reviews and ratings are a means of public and not private communication;</li>
                    <li><b>In order for a Parent rating or review to be considered for publication you must have:</b>
                        <ol>
                            <li>a child attending or has had a child attend the applicable Child Care Provider’s centre at some time over the 12 months prior to submitting the rating/review;</li>
                            <li>not submitted a rating/review previously about your Child Care Provider;</li>
                            <li>read and agree to our Terms & Conditions and our Privacy Policy and</li>
                            <li>agree to a copy of the rating/review being provided to the Child Care Provider.</li>
                        </ol>
                    </li>
                    <li>Any reviews posted must be factually true, and must be your genuine opinion based on actual first-hand experience;</li>
                    <li>You will not post a testimonial, review, or rating if you have been offered or received a monetary or non-monetary reward or any other form of compensation for submitting a review;</li>
                    <li>You take full responsibility for the testimonial, rating or review, or comment that you submit, publish or post to the Website;</li>
                    <li>You understand and agree that the review you submit is subject to the Child Care Provider confirming you have a child currently attending, or who has attended the centre in the last 12 months, and that if the Child Care Provider flags the review as malicious or inappropriate it will not be published;</li>
                    <li>DaycareSeekers.com does not endorse any comments, opinions or reviews made by anyone on this Website, and we do not review their accuracy or content, but we can, in its absolute discretion edit, refuse to post, withdraw or remove testimonials, ratings or reviews from the Website it deems inappropriate, objectionable or otherwise in breach of this agreement;</li>
                    <li>DaycareSeekers.com does not take any responsibility for mediating between parties who disagree with review and ratings and we encourage both parties to contact each other directly to discuss and resolve any issues.</li>
                </ul>
                <p>DaycareSeekers.com can reproduce, distribute, transmit, create derivative works of, and publicly display any materials and other information (including your name, ideas for new or improved products and services) that you submit to the Website or by email to Daycareseekers.com.</p>
                <p>You shall indemnify to Daycareseekers.com against all damages, losses and expenses incurred to Daycareseekers.com arising as a result of any material posted, submitted, or linked to our Website by You and subsequently published.</p>
                <p>DaycareSeekers.com does not represent or guarantee the truthfulness, accuracy, or reliability of communications, testimonials, ratings, or reviews posted by Users or endorse any opinions expressed by Users. You acknowledge that any reliance on material posted by other Users will be at your own risk.</p>
                <p>DaycareSeekers.com reserves the right to remove Users from the Website for defamatory, obscene, threatening, abusive, hateful, or embarrassing actions or behaviour.</p>
                <h4 class="inner-title-t2 mb-2">Child Care Providers</h4>
                <p class="mb-1"><b>Child Care Providers, by signing up to our Services, and by uploading, or otherwise making available Material to DaycareSeekers.com for the publishing or uploading to the Website:</b></p>
                <ul class="list-dott">
                    <li>To the extent that any Material is proprietary in nature, grant to DaycareSeekers.com a non-exclusive, worldwide, royalty-free, perpetual, transferrable and irrevocable licence to use, reproduce, edit, modify, adapt, publish and prepare derivative works from and otherwise deal with the Material in any form and for any purpose; and</li>
                    <li>You also grant each User of the Website the right to use, reproduce, edit, modify, adapt, publish and prepare derivative works from and otherwise deal with the Material in any form as permitted by the terms of this Agreement.</li>
                    <li>Warrant that the Material does not infringe the intellectual property rights of any third party.</li>
                    <li>Unconditionally waive all moral rights (as defined by the Copyright Act 1968) which you may have in respect of the Material.</li>
                    <li>Give permission to DaycareSeekers.com to make available the Material and also the name, address, and contact details for the Child Care Provider for publication on third party websites (for example in a list of Child Care Providers in a particular locality).</li>
                    <li>Acknowledge that DaycareSeekers.com may remove or alter any or all of the Material in its sole discretion without prior notice to you.</li>
                    <li>Except in relation to any of your Material uploaded to the Website, you do not have any right, title or interest in or relating to our Website and you may not use any Material on our Website in any publication without giving due acknowledgment and credit to DaycareSeekers.com.</li>
                </ul>
                <p>DaycareSeekers.com lead generation service Child Care Connect is not transferable or refundable and we do not guarantee any minimum number of leads will be generated for Child Care Providers, nor do we guarantee to Child Care Providers that Child Care Connect will convert to a place/enrolment at your service. </p>
                <p>Agencies and Child Care Providers shall be responsible for keeping their listings (including vacancy and fee information) current and up to date on the Website. You shall indemnify DaycareSeekers.com against all damages, losses and expenses incurred to DaycareSeekers.com arising as a result of any material posted, submitted, or linked to our Website by You and subsequently published.</p>
                <h4 class="inner-title-t2 mb-2">Termination</h4>
                <p class="mb-1"><b>We may terminate this agreement, or the subscription services provided to you without cause by giving you at least 30 days’ notice. Without limiting our other rights, we may immediately terminate or suspend in part or entirely this agreement or your services if:</b></p>
                <ul class="list-dott">
                    <li>You fail to pay any fees or charges due to us within 30 days after the due date;</li>
                    <li>Any of your warranties, representations or undertakings in agreement are incorrect; or;</li>
                    <li>You are in material breach of your obligations under this agreement;</li>
                    <li>You are in breach of this Agreement whether or not the breach is material, and fail to rectify the breach within 7 days of us giving you written notice of the breach and requiring that it be remedied;</li>
                    <li>In the event that we exercise our right to terminate or suspend this Agreement pursuant to THIS agreement, you will remain liable for any Fees until the date of termination.</li>
                </ul>
                <h4 class="inner-title-t2 mb-2">General Provisions</h4>
                <p>It is agreed that there is no representation, warranty, collateral agreement, or condition affecting this agreement except as expressed in it. Furthermore, it is agreed that this written instrument embodies the entire agreement of the parties with regard to the matters dealt with in it, and that no understandings or agreements, verbal or otherwise, exist between the parties except as expressly set out in this instrument.</p>
                <p>The invalidity or unenforceability of any provision of this Agreement shall not invalidate or render unenforceable the remaining provisions of this Agreement, but rather the void, illegal or invalid provision or provisions shall to the extent possible be severed from this Agreement and the remaining provisions shall continue in full force and effect, provided that the basic and underlying nature, purpose and intent of this Agreement is not materially altered.</p>
                <p>The rights and obligations of the parties hereto shall not merge upon completion of any transaction contemplated by this Agreement but shall survive the execution and delivery of any instrument entered into for the purpose of completion.</p>
                <p>This Agreement shall be governed by the laws of the Province of British Columbia, Canada and the parties submit to the jurisdiction of the Courts within that State.</p>
            </div>
        </div>
    </div>
</section>
<!-- Inner Body :: End -->
<?php require_once USER_VIEW_PATH . 'footer.inc.php';?>
<?php require_once USER_VIEW_PATH . 'frontouter.inc.php';?>