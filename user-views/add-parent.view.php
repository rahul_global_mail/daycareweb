<?php
$pageName = 'home';
require_once USER_VIEW_PATH . 'header.inc.php';?>
<!-- Inner Banner :: Start -->
<section class="inner-banner">
    <div class="inner-content text-center">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <h1 class="page-title">Register</h1>
                </div>
                <div class="col-12 col-sm-12 col-md-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li class="active">Register</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inner Banner :: End -->
<!-- Inner Body :: Start -->
<section class="innerbody-section pt-50 pb-50">
    <div class="clearfix"></div>
    <div class="register-wizard-area pb-50">
        <div class="container">
            <div class="row">
                <div class="col-4">
                    <a href="javascript:void(0);" class="register-wizard-list">
                        <span class="register-wizard-icon"><i class="fas fa-check"></i><span class="register-wizard-num">1</span></span>
                        <span class="register-wizard-title">1. Check In</span>
                    </a>
                </div>
                <div class="col-4">
                    <a href="javascript:void(0);" class="register-wizard-list">
                        <span class="register-wizard-icon"><i class="fas fa-check"></i><span class="register-wizard-num">2</span></span>
                        <span class="register-wizard-title">2. Sign Up</span>
                    </a>
                </div>
                <div class="col-4">
                    <a href="javascript:void(0);" class="register-wizard-list disabled">
                        <span class="register-wizard-icon"><i class="fas fa-check"></i><span class="register-wizard-num">3</span></span>
                        <span class="register-wizard-title">3. Verify</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
            <?php if ($_SESSION['add_msg'] == 'exist') {?>
            <div class="alert alert-danger alert-dismissible" id="success-alert">
              <strong>Alert!</strong> Data already exist.
            </div>
            <?php }?>
            <?php if ($_SESSION['add_msg'] == 'error') {?>
            <div class="alert alert-danger alert-dismissible" id="success-alert">
              <strong>Failed!</strong> Somthing goes wrong.Please try again.
            </div>
            <?php }?>
        <div class="row">
            <form method="post" class="col-md-12">
        	<input type="hidden" name="lat" value="" id="lat">
        	<input type="hidden" name="long" value="" id="long">
            <div class="col-md-6">
                <div class="signup-area">
                    <div class="section-title mb-30">
                        <h2 class="mb-10">Join us now!</h2>
                        <h6 class="mb-30">Register today & gain access to thousand of child care services across the world!</h6>
                    </div>
                    <div class="signup-form">
                    <div class="form-group">
                        <label>First Name<i class="required">*</i></label>
                        <input type="text" class="form-control" name="fname" placeholder="First Name" required="">
                    </div>
                    <div class="form-group">
                        <label>Last Name<i class="required">*</i></label>
                        <input type="text" class="form-control" name="lname" placeholder="Last Name" required="">
                    </div>
                    <div class="form-group">
                        <label>Email Address<i class="required">*</i></label>
                        <input type="email" class="form-control" name="parent_email" placeholder="Email Address" required="">
                    </div>
                    <div class="form-group">
                        <label>Postcode or Suburb<i class="required">*</i></label>
                        <div class="input-group" id="frmSearch">
                            <input type="text" id="search-box" onFocus="geolocate();" class="form-control" name="postcode" placeholder="Postcode or suburb" required="" autocomplete="off">
                            <div class="input-group-append">
                                <a class="btn btn-primary btn-gup" onclick="getLocation();"><i class="fas fa-map-marker-alt pr-2"></i> Locate Me</a>
                            </div>
                        </div>
                        <div id="suggesstion-box"></div>
                    </div>
                    </div>
                </div>
            </div>
            <section>
                <fieldset>
                    <div class="section-title text-center mb-30">
                        <h2 class="mb-10">Choose the best plan for your early childhood services</h2>
                        <h6 class="mb-30"><b>Need help?</b> Click here to <a href="contact-us.php" class="link">contact us</a> for more information</h6>
                    </div>
                    <div class="priceing-table pt-50 pb-50">
                        <!-- <div class="row">
                            <div class="col-md-4 offset-md-4">
                                <div class="selectplan-box">
                                    <label>Select Your Plan</label>
                                    <select class="form-control">
                                        <option value="0">-- Select Plan --</option>
                                        <option value="1">1 Month</option>
                                        <option value="2">6 Months</option>
                                        <option value="3">12 Months</option>
                                    </select>
                                </div>
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="col-lg-2 col-md-2"></div>
                            <div class="col-lg-4 col-md-4">
                                <div class="price-block price-clr1">
                                    <div class="price-block-header">
                                        <h4>Basic</h4>
                                        <h3><sup>$</sup>0.00</h3>
                                        <h5>Free</h5>
                                    </div>
                                    <div class="price-block-body">
                                        <div class="price-description">This <b>free</b> community service enables you to be seen by families searching for child care</div>
                                        <div class="price-list">
                                            <ul>
                                                <li>Lists your service name and basic contact details</li>
                                                <li>Details your fee and vacancy information: a red map pin indicates current vacancies and location</li>
                                                <li>Parents can contact you via email</li>
                                                <li>Parents attending your service can post a rating and review</li>
                                                <li>Parents can conveniently waitlist with you</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="price-block-foot">
                                        <input type="radio" id="selectbasicplan" name="subscription" class="custom-control-input" value="free" required="">
                                        <label class="btn btn-primary pricebtn-clr1" for="selectbasicplan">Select Basic</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="price-block price-clr2">
                                    <div class="price-block-header">
                                        <h4>Premium</h4>
                                        <h3 id="sub_price"><sup>$</sup>50.00</h3>
                                        <h5 id="sub_year">Year</h5>
                                    </div>
                                    <div class="price-block-area">
                                        <h5>Select Your Plan</h5>
                                        <div class="selectpricearea">
                                            <div class="form-check form-check-inline">
                                              <input class="form-check-input" type="radio" name="price_id" id="PremiumRadio5" value="0">
                                              <label class="form-check-label" for="PremiumRadio5">$5 Month</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                              <input class="form-check-input" type="radio" name="price_id" id="PremiumRadio50" value="1">
                                              <label class="form-check-label" for="PremiumRadio50">$50 year</label>
                                            </div>
                                        </div>
                                        <!--<select class="form-control">-->
                                        <!--    <option value="10-1">1 Month</option>-->
                                        <!--    <option value="45-5">6 Months</option>-->
                                        <!--    <option value="100-12">12 Months</option>-->
                                        <!--</select>-->
                                    </div>
                                    <div class="price-block-body">
                                        <div class="price-description">Stand out from the crowd by highlighting the unique selling points of your service and make it easier for families to contact you. <b>Includes all the features of a basic listing plus:</b></div>
                                        <div class="price-list">
                                            <ul>
                                                <li>Priority ranking in search results</li>
                                                <li>An enhanced listing displaying your logo</li>
                                                <li><b>A detailed web page</b> where you can showcase inclusions, display photos, logo and link to your website</li>
                                                <li>Advanced features including ‘request a tour’</li>
                                                <li>Showcase your video content and social media by adding your Facebook page</li>
                                                <li>Live Chat – speak to parents directly and in real time on your page</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="price-block-foot">
                                        <input type="radio" id="selectbasicpremium" name="subscription" class="custom-control-input" value="paid" required="">
                                        <label class="btn btn-primary pricebtn-clr2" for="selectbasicpremium">Select Premium</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2"></div>
                        </div>
                    </div>
                </fieldset>
            </section>
            <div class="form-group" id="card_details">
            	<div class="row">
                    <div class="col-md-4 form-group">
                        <label>Card Number</label>
                        <input type="text" class="form-control" id="card_number" name="card_number" placeholder="Card Number" required />
                        <div id="card-suggesstion-box"></div>
                    </div>
                    <div class="col-md-4 form-group">
                        <label>Expiry Month / Year</label>
                        <input type="text" class="form-control" id="card_exp" name="card_exp" placeholder="Month / Year" required />
                    </div>
                    <div class="col-md-4 form-group">
                        <label>CVC Code</label>
                        <input type="text" class="form-control" id="card_cvc" name="card_cvc" placeholder="CVC Code" required />
                    </div> 

                    <div class="col-md-6 form-group">
                        <label>Card Holder Name</label>
                        <input type="text" class="form-control" id="c_name" name="c_name" placeholder="Card Holder Name" required />
                    </div>

                    <div class="col-md-6 form-group">
                        <label>Postal Code</label>
                        <input type="text" class="form-control" id="post_code" name="post_code" placeholder="Postal Code" required />
                    </div> 
                    <input type="hidden" name="token" id="token" data-rule-checkvalidcard="true" value=""> 
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="agreedtc">
                    <label class="custom-control-label" for="agreedtc">I have read and agreed to the <a href="terms-conditions.php" class="link" target="_blank">terms and conditions</a> and <a href="#" class="link" target="_blank">privacy policy</a>.</label>
                </div>
            </div>
                    <input type="submit" class="btn btn-secondary" id="signupparent" name="submit">
            </form>
        </div>
    </div>
</section>

<!-- Inner Body :: End -->
<?php require_once USER_VIEW_PATH . 'footer.inc.php';?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw==" crossorigin="anonymous"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZPcppwWN179bx8Asxh8Xsd-ZiLIvZNMM&libraries=places"></script>

<script type="text/javascript">
	var placeSearch, autocomplete;
	function initialize() {
	    autocomplete = new google.maps.places.Autocomplete(
	    (document.getElementById('search-box')), {
	        types: ['geocode']
	    });
	    google.maps.event.addDomListener(document.getElementById('search-box'), 'focus', geolocate);
	}

	function fillInAddress(){
		const place = autocomplete.getPlace();
	}

	function geolocate() {

	    if (navigator.geolocation) {
	        navigator.geolocation.getCurrentPosition(function (position) {
	        	
	        	$("#lat").val(position.coords.latitude);
				$("#long").val(position.coords.longitude);

	            var geolocation = new google.maps.LatLng(
	            position.coords.latitude, position.coords.longitude);
	            var circle = new google.maps.Circle({
	                center: geolocation,
	                radius: position.coords.accuracy
	            });
	            autocomplete.setBounds(circle.getBounds());
	            console.log(autocomplete.getBounds());
                 console.log('autocomplete.getBounds() >>> ', autocomplete.getBounds());
                console.log('autocomplete.getPlace() >>> ', autocomplete.getPlace());
	        });
	    }
	}

	window.addEventListener('load', (event) => {
        initialize();
    });
</script>
<script type="text/javascript">

	function getLocation() {
	  	if(navigator.geolocation){
	    	navigator.geolocation.getCurrentPosition(showPosition);
	  	}else{
	    	alert("Geolocation is not supported by this browser.");
	  	}
	}

	function showPosition(position) {
		$("#lat").val(position.coords.latitude);
		$("#long").val(position.coords.longitude);
		fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.coords.latitude},${position.coords.longitude}&key=AIzaSyAZPcppwWN179bx8Asxh8Xsd-ZiLIvZNMM`)
	    .then(response => response.json())
	    .then(result => {
	    	getTypeadd(result);
	    });

	}
	window.addEventListener('load', (event) => {
	  //getLocation();
	});


	function getTypeadd(new_cords){
		var res = new Array();
		var components = new_cords.results[0].address_components;
		for(var k=0; k < components.length; k++){
			if(components[k].types.indexOf('postal_code') >= 0){
				res['postal_code'] = components[k].long_name;
			}else if(components[k].types.indexOf('sublocality_level_2') >= 0){
				res['address_line1'] = components[k].long_name;
			}else if(components[k].types.indexOf('sublocality_level_1') >= 0){
				res['address_line2'] = components[k].long_name;
			}else if(components[k].types.indexOf('administrative_area_level_1') >= 0){
				res['state'] = components[k].long_name;
			}else if(components[k].types.indexOf('administrative_area_level_2') >= 0){
				res['city'] = components[k].long_name;
			}else if(components[k].types.indexOf('country') >= 0){
				res['country'] = components[k].long_name;
			}

		}
		$("#search-box").val(res['postal_code']);
		return res;
	}

	
</script>
<script>
	$(document).on('change', '#card_number, #card_exp, #card_cvc', function (event) {
        $.ajax({
            type: "POST",
            url: "ajax/validCard.php",
            data: {
                card_number: $("#card_number").val(),
                card_exp: $("#card_exp").val(),
                card_cvc: $("#card_cvc").val(),
                c_name: $("#c_name").val(),
                post_code: $("#post_code").val(),
            },
            success: function(data){
                var res = $.parseJSON(data);
                if(res.success == 0){
                	$("#card-suggesstion-box").html(res.token).css('color', 'red').show();
                    $("#signupparent").attr('disabled', true);
                }
                if(res.success == 1){
                    $("#token").val(res.token);
                    $("#card-suggesstion-box").hide();
                    $("#signupparent").attr('disabled', false);
                }
            }
        });
    });
	$(document).ready(function () {
		$('input[name="price_id"]').on('change', function() {
	      	var radioValue = $('input[name="price_id"]:checked').val();        
	      	if(radioValue == 0){
	          	$("#sub_price").html('<sup>$</sup>5.00');
	          	$("#sub_year").html('MONTH');
	      	}
	      	if(radioValue == 1){
	          	$("#sub_price").html('<sup>$</sup>50.00');
	          	$("#sub_year").html('YEAR');
	      	}
	    });

	    $('#card_number').mask('0000-0000-0000-0000');
        $('#card_exp').mask('00/00');
    });


    $("#success-alert").fadeTo(2000, 900).slideUp(500, function(){
    $("#success-alert").slideUp(900);
    <?php $_SESSION['add_msg'] = '';?>
});
</script>
<script>
// AJAX call for autocomplete
$(document).ready(function(){
    /*$("#search-box").keyup(function(){
        $.ajax({
        type: "POST",
        url: "ajax/readLocation.php",
        data:'keyword='+$(this).val(),
        beforeSend: function(){
            $("#search-box").css("background","#FFF url(assets/images/LoaderIcon.gif) no-repeat 165px");
        },
        success: function(data){
            $("#suggesstion-box").show();
            $("#suggesstion-box").html(data);
            $("#search-box").css("background","#FFF");
        }
        });
    });*/
});
//To select country name
function selectCountry(val) {
    $("#search-box").val(val);
    $("#suggesstion-box").hide();
}
</script>
<script>
    $("input[name='subscription']").on("change", function () {
            $('.price-block').removeClass('active');
            $(this).parent().parent().toggleClass('active', this.checked);
            if($(this).val() == 'free'){
                $("#card_details").hide();
                $("#card_details").find(':input').removeAttr('required');
            }else{
                $("#card_details").show();
                $("#card_details").find(':input').attr('required', true);
            }
            //
        });
</script>
<?php require_once USER_VIEW_PATH . 'frontouter.inc.php';?>