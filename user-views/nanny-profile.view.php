<?php
$pageName = 'home';
require_once USER_VIEW_PATH . 'header.inc.php';?>
<!-- Inner Banner :: Start -->
<section class="inner-banner">
    <div class="inner-content text-center">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <h1 class="page-title">Daycare seekers found..</h1>
                    <p class="text-white"><i class="fas fa-info-circle pr-1"></i>Babysitter/In Home Carer/Nanny affiliated with us.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inner Banner :: End -->
<!-- Inner Body :: Start -->
<section id="services-menu" class="maindetailsmenu-section innermenu">
    <div class="maindetails-menu">
        <div class="container scroll-container">
            <ul class="maindetails-nav scroll-nav" id="pills-tab">
                <li class="nav-item">
                    <a class="nav-link active" href="#info">Info</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#education">Education</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#interests">Interests</a>
                </li>
                <?php
if (isset($_SESSION['parentData']) && !empty($_SESSION['parentData'])) {?>
                <li class="nav-item">
                    <a class="nav-link" href="#reviews">Reviews</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0)" data-toggle="modal" data-target="#bookModal">Book Now</a>
                </li>
                <?php } else {?>
                    <li class="nav-item">
                    <a class="nav-link" data-toggle="modal" data-target="#loginModal">Reviews</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="modal" data-target="#loginModal">Book Now</a>
                </li>
                <?php }?>
                <li class="nav-item">
                    <a class="nav-link" href="#contact" data-toggle="modal" data-target="#ContactModal">Contact</a>
                </li>

            </ul>
        </div>
    </div>
</section>
<section id="info" class="pt-50 pb-50">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <div class="view-profile-box">
                    <div class="view-profile-img mb-3">
                        <img class="img-fluid" src="upload_images/<?php echo $nannyData[0]->photo; ?>" alt="Nanny"/>
                    </div>
                    <div class="view-profile-action">
                        <a href="javascript:void(0);" class="btn btn-primary btn-block" data-toggle="modal" data-target="#bookModal">Book Now!</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-md-12">
                <div class="view-profile-intro">
                    <h2 class="intro-title"><?php echo ucwords($nannyData[0]->fullname); ?></h2>
                </div>

                <div class="view-profile-innerintro">
                    <div class="view-profile-innerintro-title">Classification</div>
                    <div class="view-profile-innerintro-label"><?php echo $nannyData[0]->classification; ?></div>
                </div>
                <div class="view-profile-innerintro">
                <div class="view-profile-innerintro-title">Graduation & Institute</div>
                    <div class="view-profile-innerintro-label"> <?php echo $nannyData[0]->graduation; ?> From <?php echo $nannyData[0]->institute; ?></div>
                </div>
                <p><b><?php echo ucfirst($nannyData[0]->fname); ?></b> [ <a href="<?php echo $nannyData[0]->website; ?>" data-toggle="tooltip" data-placement="top" title="Facebook Link" target="_blank"><i class="fab fa-facebook-f"></i></a> ]<br><?php echo $nannyData[0]->description; ?></p>
                <div class="view-profile-innerbox">
                    <p>Carer Rates</p>
                    <div class="hours-rate-profile">
                        <span class="label green"><span class="fas fa-dollar-sign"></span> <?php echo $nannyData[0]->salary; ?></span> Per <?php echo $nannyData[0]->salary_type; ?>
                    </div>
                </div>
                <div class="view-profile-innerbox">
                    <p>Qualifications & Experience</p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="firstaid" checked="" disabled="">
                                <label class="custom-control-label" for="firstaid"><i class="fas fa-briefcase-medical pr-1"></i>First Aid/CPR Training</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="firstaid1" disabled="">
                                <label class="custom-control-label" for="firstaid1"><i class="fas fa-clock pr-1"></i>Australian Nanny Member</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="employment" checked="" disabled="">
                                <label class="custom-control-label" for="employment"><i class="fas fa-plane pr-1"></i>Opportunity for employment overseas</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="professional" disabled="">
                                <label class="custom-control-label" for="professional"><i class="fas fa-book pr-1"></i>Professional development/education/training</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="superannuation" checked="" disabled="">
                                <label class="custom-control-label" for="superannuation"><i class="fas fa-coins pr-1"></i>Superannuation</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="uniforms" checked="" disabled="">
                                <label class="custom-control-label" for="uniforms"><i class="fas fa-universal-access pr-1"></i>Uniforms supplied</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="firstaid" checked="" disabled="">
                                <label class="custom-control-label" for="firstaid"><i class="fas fa-briefcase-medical pr-1"></i>No Smoking</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="firstaid" checked="" disabled="">
                                <label class="custom-control-label" for="firstaid"><i class="fas fa-briefcase-medical pr-1"></i>Working with Childcare Check</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="education" class="pt-50 pb-50 bg-grey">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="info-title mb-20">About Me</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mb-20">
                <?php echo $nannyData[0]->about; ?>
                <!-- <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                <h4 class="inner-title-t1">Other Courses</h4>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                <h4 class="inner-title-t1">About Me</h4>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p> -->
            </div>
        </div>
    </div>
</section>
<section id="interests" class="pt-50 pb-50">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="info-title mb-20">Interests</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mb-20">
                <h4 class="inner-title-t1 mb-10">Skills</h4>
                <ul class="list-badge">
                    <?php
$skills = $nannyData[0]->skills;
$arrSkill = explode(",", $skills);
foreach ($arrSkill as $skill) {?>
                        <li><span><?php echo $skill; ?></span></li>
                    <?php }
?>
                </ul>
            </div>
        </div>
    </div>
</section>
<section id="reviews" class="pt-50 pb-50 bg-grey">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h4 class="info-title mb-20">Reviews <span class="count-review">(2 Reviews)</span></h4>
            </div>
            <div class="col-md-6 text-right pt-2">
                <a href="javascript:void(0)" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#PostReviewModal">Post A Review</a>
            </div>
        </div>
        <div class="row mb-20">
            <div class="col-md-6">
                <div class="review-box">
                    <div class="review-body">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    </div>
                    <div class="review-author">Malou Galvez</div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="review-box">
                    <div class="review-body">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    </div>
                    <div class="review-author">Malou Galvez</div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p>We don't have any reviews just yet.</p>
                <a href="javascript:void(0)" class="btn btn-primary f-18" data-toggle="modal" data-target="#PostReviewModal">Post A Review</a>
            </div>
        </div>
    </div>
</section>
<!-- Inner Body :: End -->
<!-- Reviews Modal :: Start -->
<div class="modal custom-modal fade " id="PostReviewModal" tabindex="-1" role="dialog" aria-labelledby="PostReviewModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered md-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="PostReviewModal">Post A Review</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
            </div>
            <form>
                <div class="modal-body p-4">
                    <div class="form-group">
                        <label>Full Name<span class="required">*</span></label>
                        <input type="text" class="form-control" name="reviewfullname" placeholder="Full Name" required="">
                    </div>
                    <div class="form-group">
                        <label>Email Address<span class="required">*</span></label>
                        <input type="email" class="form-control" name="reviewemail" placeholder="Email Address" required="">
                    </div>
                    <div class="form-group">
                        <label>Post a Review<span class="required">*</span></label>
                        <textarea class="form-control" rows="5" placeholder="Post a Review"></textarea>
                    </div>
                    <div class="clearfix mb-3">
                        <div class="checkboxes">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="rememberme">
                                <label class="custom-control-label" for="rememberme">I here by confirm</label>
                            </div>
                        </div>
                    </div>
                    <ul class="list-disc pl-4">
                        <li>I have a child currently attending this service, or a child who has attended sometime in the last 12 months</li>
                        <li>Have not submitted a review previously</li>
                        <li>Read and agree to our Terms & Conditions and Privacy Policy.</li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Post Review</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Reviews Modal :: End -->
<!-- Contact Modal :: Start -->
<div class="modal custom-modal fade " id="ContactModal" tabindex="-1" role="dialog" aria-labelledby="ContactModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered sm-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ContactModalLabel">Contact Us</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
            </div>
            <div class="modal-body p-4">
                <form>
                    <div class="form-group">
                        <label>First Name<span class="required">*</span></label>
                        <input type="text" class="form-control" name="firstname" placeholder="First Name" required="">
                    </div>
                    <div class="form-group">
                        <label>Last Name<span class="required">*</span></label>
                        <input type="text" class="form-control" name="lastname" placeholder="Last Name" required="">
                    </div>
                    <div class="form-group">
                        <label>Email Address<span class="required">*</span></label>
                            <input type="email" class="form-control" name="emailaddress" placeholder="Email Address" required="">
                    </div>
                    <div class="form-group">
                        <label>Phone Number<span class="required">*</span></label>
                        <input type="text" class="form-control" name="phonenumber" placeholder="Phone Number" required="">
                    </div>
                    <div class="form-group">
                        <label>Enquiry<span class="required">*</span></label>
                        <input type="text" class="form-control" name="enquiry" placeholder="Enquiry" required="">
                    </div>
                    <a href="myaccount.php" class="btn btn-primary">Contact This Agency</a>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Contact Modal :: End -->
<!-- Book Modal :: Start -->
<div class="modal custom-modal fade " id="bookModal" tabindex="-1" role="dialog" aria-labelledby="bookModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered sm-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ContactModalLabel">Booking Form</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
            </div>
            <div class="modal-body p-4">
                <form method="post" id="bookingnanny">
                    <div class="form-group">
                        <label>Contact Name<span class="required">*</span></label>
                        <input type="text" class="form-control" name="contact_name" placeholder="Full Name" required="">
                    </div>
                    <div class="form-group">
                        <label>Your Email<span class="required">*</span></label>
                        <input type="email" class="form-control" name="contact_email" placeholder="Your Email" required="">
                    </div>
                    <div class="form-group">
                        <label>Contact<span class="required">*</span></label>
                            <input type="number" class="form-control" name="contact_phone" placeholder="" required="">
                    </div>
                    <div class="form-group">
                        <label>Select Date<span class="required">*</span></label>
                        <input type="date" class="form-control" name="from_date" required="">
                    </div>
                    <div class="form-group">
                        <label>Comment<span class="required">*</span></label>
                        <textarea class="form-control" name="comment" placeholder="Your Comment"></textarea>
                    </div>
                    <input type="hidden" name="parent_id" value="<?php echo $_SESSION['parentData'][0]->parent_id; ?>">
                    <input type="hidden" name="nanny_id" value="<?php echo $nannyData[0]->nanny_id; ?>">
                    <input type="hidden" name="fees" value="<?php echo $nannyData[0]->salary; ?>">
                    <input type="hidden" name="fees_type" value="<?php echo $nannyData[0]->salary_type; ?>">
                    <input type="hidden" name="action" value="booknewnanny">

                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Book Modal :: End -->
<?php require_once USER_VIEW_PATH . 'footer.inc.php';?>
<script>
$(document).on('submit', '#bookingnanny', function(event){
event.preventDefault();
$.ajax({
    url:"ajax/booknanny.php",
    method:'POST',
    data:new FormData(this),
    contentType:false,
    processData:false,
    success:function(data)
    {
      $('#bookingnanny')[0].reset();
      $('#bookModal').modal('hide');
      if(data == 'success-nanny'){
        Swal.fire(
            'Success!',
            'Your Booking has been submitted.',
            'success'
        )
      }else{
        Swal.fire(
          'Failed!','Somthing goes wrong.','error'
        ).then((result) => {
          // Reload the Page
          //window.location.replace("<?php echo $_SERVER['PHP_SELF']; ?>");
        });
      }

    }
});
});
</script>
<?php require_once USER_VIEW_PATH . 'frontouter.inc.php';?>