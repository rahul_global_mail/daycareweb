<?php
$pageName = 'home';
require_once USER_VIEW_PATH . 'header.inc.php';
?>
<!-- Inner Banner :: Start -->
<section class="inner-banner">
    <div class="inner-content text-center">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <h1 class="page-title">Resources For ECE</h1>
                </div>
                <div class="col-12 col-sm-12 col-md-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li class="active">Resources For ECE</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inner Banner :: End -->
<section class="innerbody-section pt-50 pb-50 bg-grey">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center mb-30">
                    <h2 class="mb-10">Some compelling reasons to join the DayCare.com community</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <?php
$count = 1;
if ($resourcedata) {
	foreach ($resourcedata as $data) {?>
            <div class="col-md-3 mb-30">
                <div class="community-box">
                    <div class="community-resource-img">
                        <img class="img-fluid" src="upload_images/<?php echo $data->banner; ?>" alt="Community"/>
                    </div>
                    <div class="community-desc">
                        <a href="<?php echo $data->weblink; ?>" target="_blank"><h4 class="resource-title"><?php echo $data->title; ?></h4></a>
                    </div>
                </div>
            </div>
<?php }} else {?>
                                        <div class="col-md-3 mb-30">
                <div class="community-box">
                    <div class="community-desc">
                        <h4>Coming Soon..</h4>
                    </div>
                </div>
            </div>
                                    <?php }?>
        </div>
    </div>
</section>

<!-- Inner Body :: End -->
<?php require_once USER_VIEW_PATH . 'footer.inc.php';?>
<?php require_once USER_VIEW_PATH . 'frontouter.inc.php';?>