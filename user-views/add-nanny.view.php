<?php
$pageName = 'home';
require_once USER_VIEW_PATH . 'header.inc.php';
?>
<!-- Inner Banner :: Start -->
<section class="inner-banner">
    <div class="inner-content text-center">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <h1 class="page-title">Register</h1>
                </div>
                <div class="col-12 col-sm-12 col-md-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li class="active">Register</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inner Banner :: End -->
<!-- Inner Body :: Start -->
<section class="innerbody-section pt-50 pb-50">
    <div class="container">
        <div class="signup-area">
            <div class="signup-form">
                <form id="signup-form" method="post">
                	<input type="hidden" name="lat" value="" id="lat">
        			<input type="hidden" name="long" value="" id="long">
                    <h3>Register</h3>
                    <section>
                        <fieldset class="mb-20">
                            <h2 class="form-title">Tell us about your self?</h2>
                            <!-- <div class="row">
                                <div class="col-md-2 form-group">
                                    <label class="bold-label">Profile Avatar</label>
                                    <div class="uploadimage-block">
                                        <div class="uploadimage-area">
                                            <img class="portimg img-fluid" src="assets/images/default-img.jpg" alt="your image">
                                            <a class="add-img" href="javascript:void(0)" onclick="$('.fileUpload').click()"></a>
                                            <input type="file" class="form-control fileUpload" name="fileUpload" accept=".jpg,.jpeg, .png" style="display: none;">
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <!-- <input type="hidden" name="type" value="<?php //echo $_GET['type']; ?>"> -->
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <select name="type" class="form-control" required="">
                                        <option value="">-- Select Your Type --</option>
                                        <option value="nanny">Nanny</option>
                                        <option value="babysitter">Babysitter</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label>Your Full Name<i class="required">*</i></label>
                                    <input type="text" class="form-control" name="fullname" placeholder="e.g : Akash Sathvara" required="">
                                </div>
                                <div class="col-md-12 form-group">
                                    <label>About your self</label>
                                    <textarea name="about" class="form-control summernote" required><?php echo $nannyData[0]->about; ?></textarea>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label>Facebook Link</label>
                                    <input type="text" class="form-control" name="website" placeholder="Facebook Link">
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="mb-20">
                            <h2 class="form-title">Contact Details</h2>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label>Email Address<i class="required">*</i></label>
                                    <input type="email" class="form-control" name="email_id" placeholder="Email Address" required="">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Phone Number<i class="required">*</i></label>
                                    <input type="text" class="form-control" name="contact" placeholder="Phone Number" required="">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>First Name<i class="required">*</i></label>
                                    <input type="text" class="form-control" name="fname" placeholder="First Name" required="">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>last Name<i class="required">*</i></label>
                                    <input type="text" class="form-control" name="lname" placeholder="Last Name" required="">
                                </div>
                                <div class="col-md-12 form-group">
                                    <label>Address<i class="required">*</i></label>
                                    <input type="text" class="form-control"  onFocus="geolocate();"  id="address" name="address" placeholder="Address" required="">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Suburb<i class="required">*</i></label>
                                    <input type="text" class="form-control" id="suburb" name="suburb" placeholder="Suburb" required="">
                                </div>
                                <div class="col-md-6 form-group">
			                        <label>Postcode<i class="required">*</i></label>
			                        <div class="input-group" id="frmSearch">
			                            <input type="text" id="search-box" class="form-control" name="postcode" placeholder="Postcode" required="">
			                            <div class="input-group-append">
			                                <a class="btn btn-primary btn-gup" onclick="getLocation();"><i class="fas fa-map-marker-alt pr-2"></i> Locate Me</a>
			                            </div>
			                        </div>
			                        <div id="suggesstion-box"></div>
			                    </div>
                                <div class="col-md-6 form-group">
                                    <label>City<i class="required">*</i></label>
                                    <input type="text" id="search-box1" class="form-control" name="city" placeholder="City" required="">
                                    <div id="suggesstion-box1"></div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Country<i class="required">*</i></label>
                                    <select id="country" name="country" class="form-control" required="">
                                        <option value="">-- Select Country --</option>
                                        <option value="CA">Canada</option>
                                        <option value="US">US</option>
                                        <option value="IN">India</option>
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="mb-20">
                            <h2 class="form-title">Now just some things to keep your login secure</h2>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label>Password<i class="required">*</i></label>
                                    <input type="password" class="form-control" name="password" placeholder="Password" required="">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Repeat Password<i class="required">*</i></label>
                                    <input type="password" class="form-control" name="repeatpassword" placeholder="Repeat Password" required="">
                                </div>
                            </div>
                        </fieldset>
                    </section>
                    <h3>Profile</h3>
                    <section>
                        <fieldset class="mb-20">
                            <h2 class="form-title">Profile details</h2>
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label>Your Availablity</label>
                                    <div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="CasualRadio" name="availablity" class="custom-control-input" value="casual">
                                            <label class="custom-control-label" for="CasualRadio">Casual</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="ContractRadio" name="availablity" class="custom-control-input" value="contract">
                                            <label class="custom-control-label" for="ContractRadio">Contract</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="FullTimeRadio" name="availablity" class="custom-control-input" value="fulltime">
                                            <label class="custom-control-label" for="FullTimeRadio">Full Time</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="PartTimeRadio" name="availablity" class="custom-control-input" value="parttime">
                                            <label class="custom-control-label" for="PartTimeRadio">Part Time</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="TemporaryRadio" name="availablity" class="custom-control-input" value="temporary">
                                            <label class="custom-control-label" for="TemporaryRadio">Temporary</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <h2 class="form-title">What are the benefits & salary on offer?</h2>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label>Salary</label>
                                    <input type="text" class="form-control" name="salary" placeholder="Salary">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Select Type</label>
                                    <select name="salary_type" class="form-control">
                                        <option value="Hours">Hours</option>
                                        <option value="Weekly">Weekly</option>
                                        <option value="Monthly">Monthly</option>
                                    </select>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label>Services</label>
                                    <div class="row">
                                        <?php
foreach ($benefitData as $benefit) {
	?>
                                            <div class="col-md-6">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="ben<?php echo $benefit->service_id; ?>" name="services[]" value="<?php echo $benefit->service_id; ?>">
                                                    <label class="custom-control-label" for="ben<?php echo $benefit->service_id; ?>"><i class="fas fa-briefcase-medical pr-1"></i><?php echo $benefit->service_name; ?></label>
                                                </div>
                                            </div>
                                        <?php }?>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Interests / Skills</label>
                                   <input type="hidden" class="variantFieldTags" name="skills">
                                </div>
                                <input type="hidden" class="form-control" name="classification" value="-">
                                <!--<div class="col-md-6 form-group">-->
                                <!--    <label>Classification<i class="required">*</i></label>-->
                                <!--    <input type="text" class="form-control" name="classification" placeholder="Classification" required="">-->
                                <!--</div>-->

                            </div>
                        </fieldset>
                        <fieldset>
                            <h2 class="form-title">Where is the job located?</h2>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label>Location<i class="required">*</i></label>
                                    <select name="job_location" class="form-control" required="">
                                        <option value="">-- Select Country --</option>
                                        <option value="Canada">Canada</option>
                                        <option value="US">US</option>
                                    </select>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Suburb/Town<i class="required">*</i></label>
                                    <input type="text" class="form-control" name="job_suburb" placeholder="Suburb/Town" required="">
                                </div>
                            </div>
                        </fieldset>
                    </section>
                    <h3>Select The Plan</h3>
                    <section>
                        <fieldset>
                            <div class="section-title text-center mb-30">
                                <h2 class="mb-10">Choose the best plan for your early childhood services</h2>
                                <h6 class="mb-30"><b>Need help?</b> Click here to <a href="contact-us.php" class="link">contact us</a> for more information</h6>
                            </div>
                            <div class="priceing-table pt-50 pb-50">
                                <!-- <div class="row">
                                    <div class="col-md-4 offset-md-4">
                                        <div class="selectplan-box">
                                            <label>Select Your Plan</label>
                                            <select class="form-control">
                                                <option value="0">-- Select Plan --</option>
                                                <option value="1">1 Month</option>
                                                <option value="2">6 Months</option>
                                                <option value="3">12 Months</option>
                                            </select>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="row">
                                    <div class="col-lg-2 col-md-2"></div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="price-block price-clr1">
                                            <div class="price-block-header">
                                                <h4>Basic</h4>
                                                <h3><sup>$</sup>0.00</h3>
                                                <h5>Free</h5>
                                            </div>
                                            <div class="price-block-body">
                                                <div class="price-description">This <b>free</b> community service enables you to be seen by families searching for child care</div>
                                                <div class="price-list">
                                                    <ul>
                                                        <li>Lists your service name and basic contact details</li>
                                                        <li>Details your fee and vacancy information: a red map pin indicates current vacancies and location</li>
                                                        <li>Parents can contact you via email</li>
                                                        <li>Parents attending your service can post a rating and review</li>
                                                        <li>Parents can conveniently waitlist with you</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="price-block-foot">
                                                <input type="radio" id="selectbasicplan" name="subscription" class="custom-control-input" value="free">
                                                <label class="btn btn-primary pricebtn-clr1" for="selectbasicplan">Select Basic</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="price-block price-clr2">
                                            <div class="price-block-header">
                                                <h4>Premium</h4>
                                                <h3 id="sub_price"><sup>$</sup>15.00</h3>
                                                <h5 id="sub_year">Month</h5>
                                            </div>
                                            <div class="price-block-area">
                                                <h5>Select Your Plan</h5>
                                                <div class="selectpricearea">
                                                    <div class="form-check form-check-inline">
                                                      <input class="form-check-input" type="radio" name="price_id" id="PremiumRadio5" value="0">
                                                      <label class="form-check-label" for="PremiumRadio5">$15 Month</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                      <input class="form-check-input" type="radio" name="price_id" id="PremiumRadio50" value="1">
                                                      <label class="form-check-label" for="PremiumRadio50">$150 year</label>
                                                    </div>
                                                </div>
                                                <!--<select class="form-control">-->
                                                <!--    <option value="10-1">1 Month</option>-->
                                                <!--    <option value="45-5">6 Months</option>-->
                                                <!--    <option value="100-12">12 Months</option>-->
                                                <!--</select>-->
                                            </div>
                                            <div class="price-block-body">
                                                <div class="price-description">Stand out from the crowd by highlighting the unique selling points of your service and make it easier for families to contact you. <b>Includes all the features of a basic listing plus:</b></div>
                                                <div class="price-list">
                                                    <ul>
                                                        <li>Priority ranking in search results</li>
                                                        <li>An enhanced listing displaying your logo</li>
                                                        <li><b>A detailed web page</b> where you can showcase inclusions, display photos, logo and link to your website</li>
                                                        <li>Advanced features including ‘request a tour’</li>
                                                        <li>Showcase your video content and social media by adding your Facebook page</li>
                                                        <li>Live Chat – speak to parents directly and in real time on your page</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="price-block-foot">
                                                <input type="radio" id="selectbasicpremium" name="subscription" class="custom-control-input" value="paid">
                                                <label class="btn btn-primary pricebtn-clr2" for="selectbasicpremium">Select Premium</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2"></div>
                                </div>
                            </div>
                        </fieldset>
                    </section>
                    <h3>Post</h3>
                    <!-- Inner Body :: Start -->
                    <section class="innerbody-section pt-50 pb-50">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 offset-md-2">
                                    <div class="section-title text-center mb-30">
                                        <h2 class="mb-0">ONE LAST Final Step</h2>
                                    </div>
                                    <!-- <div class="signupconfirm-area">
                                        <h6>Welcome, Care Provider</h6>
                                        <p>We’ve just sent you a message to verify your email address.</p>
                                        <p>We have sent a Verification email to <b>loremipsum@gmail.com</b> and follow the instructions.</p>
                                    </div> -->
                                    <div class="row" id="card_details">
                                        <div class="col-md-4 form-group">
                                            <label>Card Number</label>
                                            <input type="text" class="form-control" id="card_number" name="card_number" placeholder="Card Number" required />
                                            <div id="card-suggesstion-box"></div>
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label>Expiry Month / Year</label>
                                            <input type="text" class="form-control" id="card_exp" name="card_exp" placeholder="Month / Year" required />
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label>CVC Code</label>
                                            <input type="text" class="form-control" id="card_cvc" name="card_cvc" placeholder="CVC Code" required />
                                        </div>

                                        <div class="col-md-6 form-group">
                                            <label>Card Holder Name</label>
                                            <input type="text" class="form-control" id="c_name" name="c_name" placeholder="Card Holder Name" required />
                                        </div>

                                        <div class="col-md-6 form-group">
                                            <label>Postal Code</label>
                                            <input type="text" class="form-control" id="post_code" name="post_code" placeholder="Postal Code" required />
                                        </div> 
                                        <input type="hidden" name="token" id="token" value=""> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- Inner Body :: End -->
<?php require_once USER_VIEW_PATH . 'footer.inc.php';?>
<script src="<?php echo HOME_URL; ?>assets/js/jquery.steps.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw==" crossorigin="anonymous"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZPcppwWN179bx8Asxh8Xsd-ZiLIvZNMM&libraries=places"></script>
<script type="text/javascript">
    var placeSearch, autocomplete;

    function initialize() {
        autocomplete = new google.maps.places.Autocomplete(
        (document.getElementById('address')), {
            types: ['geocode']
        });
        google.maps.event.addDomListener(document.getElementById('address'), 'focus', geolocate);

        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            console.log(place.address_components);
            var res = new Array();
            for(var k=0; k < place.address_components.length; k++){
                if(place.address_components[k].types.indexOf('postal_code') >= 0){
                    res['postal_code'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('locality') >= 0){
                    res['suberb'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('sublocality_level_2') >= 0){
                    res['address_line1'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('sublocality_level_1') >= 0){
                    res['address_line2'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('administrative_area_level_1') >= 0){
                    res['state'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('administrative_area_level_2') >= 0){
                    res['city'] = place.address_components[k].long_name;
                }else if(place.address_components[k].types.indexOf('country') >= 0){
                    res['country'] = place.address_components[k].short_name;
                }
            }

            $("#search-box").val(res['postal_code']);
            $("#suburb").val(res['suberb']);
            $("#search-box1").val(res['city']);
            $("#country").val(res['country']);
        });
    }

    function geolocate() {

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                
                $("#lat").val(position.coords.latitude);
                $("#long").val(position.coords.longitude);

                var geolocation = new google.maps.LatLng(
                position.coords.latitude, position.coords.longitude);
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }

    window.addEventListener('load', (event) => {
        initialize();
    });
</script>

<script type="text/javascript">

	

	function getLocation() {
	  	if(navigator.geolocation){
	    	navigator.geolocation.getCurrentPosition(showPosition);
	  	}else{
	    	alert("Geolocation is not supported by this browser.");
	  	}
	}

	function showPosition(position) {
		$("#lat").val(position.coords.latitude);
		$("#long").val(position.coords.longitude);
		fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.coords.latitude},${position.coords.longitude}&key=AIzaSyAZPcppwWN179bx8Asxh8Xsd-ZiLIvZNMM`)
	    .then(response => response.json())
	    .then(result => {
	    	getTypeadd(result);
	    });
	}
	window.addEventListener('load', (event) => {
  		//getLocation();
	});

	function getTypeadd(new_cords){
		var res = new Array();
		var components = new_cords.results[0].address_components;

		for(var k=0; k < components.length; k++){
			if(components[k].types.indexOf('postal_code') >= 0){
				res['postal_code'] = components[k].long_name;
			}else if(components[k].types.indexOf('sublocality_level_2') >= 0){
				res['address_line1'] = components[k].long_name;
			}else if(components[k].types.indexOf('sublocality_level_1') >= 0){
				res['address_line2'] = components[k].long_name;
			}else if(components[k].types.indexOf('administrative_area_level_1') >= 0){
				res['state'] = components[k].long_name;
			}else if(components[k].types.indexOf('administrative_area_level_2') >= 0){
				res['city'] = components[k].long_name;
			}else if(components[k].types.indexOf('country') >= 0){
				res['country'] = components[k].short_name;
			}

		}
		$("#search-box").val(res['postal_code']);
		$("#address").val(new_cords.results[0].formatted_address);
		$("#suburb").val(res['address_line2']);
		$("#search-box1").val(res['city']);
		$("#country").val(res['country']);
		return res;
	}
</script>
<script type="text/javascript">
	$(document).on('change', '#card_number, #card_exp, #card_cvc', function (event) {
        $.ajax({
            type: "POST",
            url: "ajax/validCard.php",
            data: {
                card_number: $("#card_number").val(),
                card_exp: $("#card_exp").val(),
                card_cvc: $("#card_cvc").val(),
                c_name: $("#c_name").val(),
                post_code: $("#post_code").val(),
            },
            success: function(data){
                var res = $.parseJSON(data);
                if(res.success == 0){
                    $("#card-suggesstion-box").html(res.token).css('color', 'red').show();
                    $("#signupnanny").attr('disabled', true);
                }
                if(res.success == 1){
                    $("#token").val(res.token);
                    $("#card-suggesstion-box").hide();
                    $("#signupnanny").attr('disabled', false);
                }
            }
        });
    });
    $(document).ready(function () {
         $(function () {
            $('.variantFieldTags').tagit({
                singleField: true,
                placeholderText: "e.g. Reading , Humble with baby"
            });
        });
        var form = $('#signup-form').show();
        form.steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            enableFinishButton: false,

            labels: {
                finish: "Submit",
                next: "Next",
                previous: "Back",

            },
            onStepChanging: function (event, currentIndex, newIndex)
            {
                form.validate().settings.ignore = ':disabled,:hidden';
                return form.valid();
            },
            onStepChanged: function (event, currentIndex, priorIndex) {
                if (currentIndex == 3) {
                    var $input = $('<input type="submit" id="signupnanny" name="signupnanny" class="btn btn-primary" value="Submit"/>');
                    $input.appendTo($('ul[aria-label=Pagination]'));
                } else {
                    $('ul[aria-label=Pagination] input[value="Submit"]').remove();
                }
            },
            onFinishing: function (event, currentIndex)
            {
                form.validate().settings.ignore = ":disabled";
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                alert("Good job!", "Submitted!", "success");
            }
        });

        $('input[name="price_id"]').on('change', function() {
          var radioValue = $('input[name="price_id"]:checked').val();        
          if(radioValue == 0){
              $("#sub_price").html('<sup>$</sup><?php echo number_format(PRONAN_MONTH_PRICE,2); ?>');
              $("#sub_year").html('MONTH');
          }
          if(radioValue == 1){
              $("#sub_price").html('<sup>$</sup><?php echo number_format(PRONAN_YEAR_PRICE,2); ?>');
              $("#sub_year").html('YEAR');
          }
        });


        $('#card_number').mask('0000-0000-0000-0000');
        $('#card_exp').mask('00/00');
        $('#schemeDetails').css('display', 'none');
        $('#VacanciesPostSelectDate-box').css('display', 'none');

        $("input[name='familydaycare']").click(function () {
            if ($("#familydaycare").is(":checked")) {
                $("#schemeDetails").show();
                $("#longdaycare").attr('disabled', true);
                $("#occasionalcare").attr('disabled', true);
                $("#preschool").attr('disabled', true);
            } else {
                $("#schemeDetails").hide();
            }
        });

        $("input[name='VacanciesPost']").click(function () {
            if ($("#VacanciesPostSelectDate").is(":checked")) {
                $("#VacanciesPostSelectDate-box").show();
            } else {
                $("#VacanciesPostSelectDate-box").hide();
            }
        });

        $("input[name='subscription']").on("change", function () {
            $('.price-block').removeClass('active');
            $(this).parent().parent().toggleClass('active', this.checked);
            if($(this).val() == 'free'){
                $("#card_details").hide();
                $("#card_details").find(':input').removeAttr('required');
            }else{
                $("#card_details").show();
                $("#card_details").find(':input').attr('required', true);
            }
        });

    });
</script>
<script>
// AJAX call for autocomplete
$(document).ready(function(){
    $("#search-box").keyup(function(){
        $.ajax({
        type: "POST",
        url: "ajax/readPostcode.php",
        data:'keyword='+$(this).val(),
        beforeSend: function(){
            $("#search-box").css("background","#FFF url(assets/images/LoaderIcon.gif) no-repeat 165px");
        },
        success: function(data){
            $("#suggesstion-box").show();
            $("#suggesstion-box").html(data);
            $("#search-box").css("background","#FFF");
        }
        });
    });
});
//To select country name
function selectCountry(val) {
    $("#search-box").val(val);
    $("#suggesstion-box").hide();
}
</script>
<script>
// AJAX call for autocomplete
$(document).ready(function(){
    /*$("#search-box1").keyup(function(){
        $.ajax({
        type: "POST",
        url: "ajax/readCity.php",
        data:'keyword='+$(this).val(),
        beforeSend: function(){
            $("#search-box1").css("background","#FFF url(assets/images/LoaderIcon.gif) no-repeat 165px");
        },
        success: function(data){
            $("#suggesstion-box1").show();
            $("#suggesstion-box1").html(data);
            $("#search-box1").css("background","#FFF");
        }
        });
    });*/
});
//To select country name
function selectCountry1(val) {
    $("#search-box1").val(val);
    $("#suggesstion-box1").hide();
}
</script>

<style type="text/css">
	.wizard > .steps > ul > li {
		width: 25%;
	}
</style>
<?php require_once USER_VIEW_PATH . 'frontouter.inc.php';?>