<?php
$pageName = 'home';
require_once USER_VIEW_PATH . 'header.inc.php';
if (isset($_GET['action']) && !empty($_GET['action'])) {
	if ($_GET['action'] == "provider") {
		$pagetitle = "For Provider";
	} elseif ($_GET['action'] == "parent") {
		$pagetitle = "For Parent";
	} elseif ($_GET['action'] == "nanny") {
		$pagetitle = "For Nanny | Babbysitter";
	} else {
		$pagetitle = "404";
	}
}
?>
<!-- Inner Banner :: Start -->
<section class="inner-banner">
    <div class="inner-content text-center">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <h1 class="page-title"><?php echo $pagetitle; ?></h1>
                </div>
                <div class="col-12 col-sm-12 col-md-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li class="active"><?php echo $pagetitle; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inner Banner :: End -->
<!-- Inner Body :: Start -->
<section class="innerbody-section pt-50 pb-50">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="section-title mb-30">
                    <h2 class="mb-10"><?php echo $pagedata[0]->page_title; ?></h2>
                    <h6 class="mb-30"><?php echo $pagedata[0]->sub_title; ?></h6>
                </div>
                <div><?php echo $pagedata[0]->page_content; ?></div>
            </div>
            <div class="col-md-6">
                <div class="adv-banner pt-50">
                    <div class="owl-carousel owl-theme">
                        <?php
foreach ($galleryData as $banner) {?>
                        <div class="adv-box">
                            <div class="adv-image">
                                <img class="img-fluid" src="upload_images/<?php echo $banner->image_url; ?>" alt="Adv"/>
                            </div>
                            <div class="adv-content">
                                <p>Finding a brilliant babysitter</p>
                            </div>
                        </div>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="innerbody-section pt-50 pb-50 bg-grey">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center mb-30">
                    <h2 class="mb-10">Some compelling reasons to join the DayCare.com community</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="community-box">
                    <div class="community-img">
                        <img class="img-fluid" src="assets/images/about-2.jpg" alt="Community"/>
                    </div>
                    <div class="community-desc">
                        <h4>What is Lorem Ipsum?</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="community-box">
                    <div class="community-img">
                        <img class="img-fluid" src="assets/images/about-2.jpg" alt="Community"/>
                    </div>
                    <div class="community-desc">
                        <h4>What is Lorem Ipsum?</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="community-box">
                    <div class="community-img">
                        <img class="img-fluid" src="assets/images/about-2.jpg" alt="Community"/>
                    </div>
                    <div class="community-desc">
                        <h4>What is Lorem Ipsum?</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Inner Body :: End -->
<?php require_once USER_VIEW_PATH . 'footer.inc.php';?>
<?php require_once USER_VIEW_PATH . 'frontouter.inc.php';?>