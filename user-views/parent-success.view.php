<?php
$pageName = 'home';
require_once USER_VIEW_PATH . 'header.inc.php';?>
<!-- Inner Banner :: Start -->
<section class="inner-banner">
    <div class="inner-content text-center">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <h1 class="page-title">Register</h1>
                </div>
                <div class="col-12 col-sm-12 col-md-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li class="active">Register</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inner Banner :: End -->
<!-- Inner Body :: Start -->
<section class="innerbody-section pt-50 pb-50">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="section-title text-center mb-30">
                    <h2 class="mb-0">Thanks For Registration!</h2>
                </div>
                <div class="signupconfirm-area">
                    <h6>Hello, Lorem Ipsum</h6>
                    <p>We’ve just sent you a message to verify your email address.</p>
                    <p>All you need to do to join the Daycare.com community is visit your <b><?php echo (isset($_SESSION['parent_mail']) ? $_SESSION['parent_mail'] : ''); ?></b> inbox and follow the instructions.</p>
                    <a href="verify-login.php?verifyID=<?php echo $_GET['verifyID']; ?>&action=verifyuser&uID=<?php echo $_GET['verifyID']; ?>" class="btn btn-primary">Go to Profile</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inner Body :: End -->
<?php require_once USER_VIEW_PATH . 'footer.inc.php';?>
<?php require_once USER_VIEW_PATH . 'frontouter.inc.php';?>