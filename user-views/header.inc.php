<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <meta name="description" content="DayCare Seekers" />
        <meta name="keywords" content="DayCare Seekers" />
        <meta name="author" content="DayCare Seekers">
        <meta name="theme-color" content="#02a04b" />
        <meta name="msapplication-navbutton-color" content="#02a04b">
        <meta name="apple-mobile-web-app-status-bar-style" content="#02a04b">
        <title>DayCare Seekers</title>

        <!-- Favicon -->
        <link rel="apple-touch-icon" type="image/x-icon" sizes="76x76" href="assets/images/favicon.png" />
        <link rel="icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- Icon -->
        <link href="assets/fonts/fontawesome/css/fontawesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/fonts/themify/themify.css" rel="stylesheet" type="text/css"/>
        <link href="assets/fonts/summernote/css/summernote.css" rel="stylesheet" type="text/css"/>

        <!-- Css -->
        <link href="<?php echo HOME_URL; ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo HOME_URL; ?>assets/css/masterslider.main.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo HOME_URL; ?>assets/css/select2.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo HOME_URL; ?>assets/css/owl.carousel.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo HOME_URL; ?>assets/css/owl.theme.default.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo HOME_URL; ?>assets/css/summernote.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo HOME_URL; ?>assets/css/bootstrap-datetimepicker-v4.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo HOME_URL; ?>assets/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo HOME_URL; ?>assets/css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo HOME_URL; ?>assets/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo HOME_URL; ?>DayCare-AdminPanel/assets/css/sweetalert2.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo HOME_URL; ?>assets/css/jquery.fancybox.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo HOME_URL; ?>assets/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo HOME_URL; ?>assets/css/responsive.css" rel="stylesheet" type="text/css"/>
        
    </head>
    <body class="wrapper">
    	 <!-- Header :: Start -->
            <header class="main-header">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <div class="container">
                        <a class="navbar-brand" href="<?php echo HOME_URL; ?>">
                            <img class="img-fluid" src="<?php echo HOME_URL; ?>assets/images/logo.png" alt="DayCare Seekers"/>
                        </a>
                        <div class="header-right-wrap ml-auto ml-auto-links order-lg-2">
                            <ul><?php
if (isset($_SESSION['parentData']) && !empty($_SESSION['parentData'])) {?>
                                    <li class="mr-2">
                                    <a href="account-Parent.php" class="btn btn-primary header-search"><i class="ti-user pr-1"></i><span> My Profile</span></a>
                                </li>
                                <li>
                                    <a href="logout.php" class="btn btn-danger header-search"><i class="ti-power-off pr-1"></i><span> Logout</span></a>
                                </li>
                                <?php } elseif (isset($_SESSION['nannyData']) && !empty($_SESSION['nannyData'])) {?>
                               <li class="mr-2">
                                    <a href="account-Nanny.php" class="btn btn-primary header-search"><i class="ti-user pr-1"></i><span> My Profile</span></a>
                                </li>
                                <li>
                                    <a href="logout.php" class="btn btn-danger header-search"><i class="ti-power-off pr-1"></i><span> Logout</span></a>
                                </li>
                            <?php } elseif (isset($_SESSION['providerData']) && !empty($_SESSION['providerData'])) {?>
                                <li class="mr-2">
                                    <a href="account-Provider.php" class="btn btn-primary header-search"><i class="ti-user pr-1"></i><span> My Profile</span></a>
                                </li>
                                <li>
                                    <a href="logout.php" class="btn btn-danger header-search"><i class="ti-power-off pr-1"></i><span> Logout</span></a>
                                </li>
                            <?php } else {?>
                                <li class="mr-2">
                                    <a href="javascript:void(0)" class="btn btn-primary header-search" data-toggle="modal" data-target="#loginModal"><i class="ti-lock pr-1"></i><span>Login</span></a>
                                </li>
                                <li>
                                    <a href="sign-up.php" class="btn btn-secondary header-search"><i class="ti-user pr-1"></i><span>Register</span></a>
                                </li>
                            <?php }?>
                            </ul>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="ti-menu"></span></button>
                        <div class="main-menu collapse navbar-collapse order-lg-1" id="navbarSupportedContent">
                            <ul class="navbar-nav custom-navbar ml-auto">
                                <li class="nav-item <?php
if (isset($pageName) && $pageName == 'home') {
	echo 'active';
}
?>">
                                    <a class="nav-link" href="index.php"><span>Home</span></a>
                                </li>
                                <li class="nav-item <?php
if (isset($pageName) && $pageName == 'search') {
	echo 'active';
}
?>">
                                    <a class="nav-link" href="search-area.php?type=All"><span>Search</span></a>
                                </li>
                                <li class="nav-item dropdown <?php
if (isset($pageName) && $pageName == 'parents') {
	echo 'active';
}
?>">
                                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Services</a>
                                    <ul class="dropdown-menu submenu">
                                        <li><a href="for-provider.php?action=provider">For Provider</a></li>
                                        <li><a href="for-provider.php?action=parent">For Parent</a></li>
                                        <li><a href="for-provider.php?action=nanny">For Babysitter/Nanny</a></li>
                                        <li><a href="resources.php">Resources For ECE</a></li>
                                    </ul>
                                </li>
                                <li class="nav-item <?php
if (isset($pageName) && $pageName == 'contact') {
	echo 'active';
}
?>">
                                    <a class="nav-link" href="contact-us.php"><span>Contact Us</span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>
            <!-- Header :: End -->
            <!-- https://jsfiddle.net/ES4TF/ -->