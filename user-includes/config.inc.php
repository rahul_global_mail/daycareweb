<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
session_start();
////////////////////////////////////////////////////////////////////////////////
// Configure the default time zone
////////////////////////////////////////////////////////////////////////////////
//date_default_timezone_set('Europe/London');

////////////////////////////////////////////////////////////////////////////////
// Configure the default currency
////////////////////////////////////////////////////////////////////////////////
setlocale(LC_MONETARY, 'en_US');

////////////////////////////////////////////////////////////////////////////////
// Define constants for database connectivty
////////////////////////////////////////////////////////////////////////////////
defined('DATABASE_HOST') ? NULL : define('DATABASE_HOST', 'localhost');
defined('DATABASE_NAME') ? NULL : define('DATABASE_NAME', 'daycareseek');
defined('DATABASE_USER') ? NULL : define('DATABASE_USER', 'root');
defined('DATABASE_PASSWORD') ? NULL : define('DATABASE_PASSWORD', '');
// defined('DATABASE_HOST') ? NULL : define('DATABASE_HOST', 'localhost');
// defined('DATABASE_NAME') ? NULL : define('DATABASE_NAME', 'fons360c_digitalform'); //fons360c_digitalform
// defined('DATABASE_USER') ? NULL : define('DATABASE_USER', 'fons360c_digitalform'); //fons360c_digitalform
// defined('DATABASE_PASSWORD') ? NULL : define('DATABASE_PASSWORD', 'HCxwCGohBKn%'); //HCxwCGohBKn%

////////////////////////////////////////////////////////////////////////////////
// Define absolute application paths
////////////////////////////////////////////////////////////////////////////////

// Use PHP's directory separator for windows/unix compatibility
defined('DS') ? NULL : define('DS', DIRECTORY_SEPARATOR);
defined('RDS') ? NULL : define('RDS', '/');
// Project Name
defined('PROJECTTITLE') ? NULL : define('PROJECTTITLE', 'DayCare Seekeres| Care Provider | Nanny | Baby Sitter');
// Define relative path to server root
//defined('HOME_URL') ? NULL : define('HOME_URL', 'https://daycareseekers.com/');
defined('HOME_URL') ? NULL : define('HOME_URL', 'https://daycareseekers.com/');
defined('IMAGEROOT') ? NULL : define('IMAGEROOT', HOME_URL . 'assets/images/');

// Define absolute path to server root
defined('SITE_ROOT') ? NULL : define('SITE_ROOT', dirname(dirname(__FILE__)) . DS);

// Define absolute path to includes
defined('INCLUDE_PATH') ? NULL : define('INCLUDE_PATH', SITE_ROOT . 'user-includes' . DS);
defined('FUNCTION_PATH') ? NULL : define('FUNCTION_PATH', INCLUDE_PATH . 'functions' . DS);
defined('LIB_PATH') ? NULL : define('LIB_PATH', INCLUDE_PATH . 'libraries' . DS);
defined('USER_MODEL_PATH') ? NULL : define('USER_MODEL_PATH', SITE_ROOT . 'user-models' . DS);
defined('USER_VIEW_PATH') ? NULL : define('USER_VIEW_PATH', SITE_ROOT . 'user-views' . DS);

defined('UPLOADS_PATH') ? NULL : define('UPLOADS_PATH', SITE_ROOT . 'uploads' . DS);

/*defined('STRIPE_API_SECRET_KEY') ? NULL : define('STRIPE_API_SECRET_KEY','sk_test_JKjKUqijygLxeNtcOrRuyiwV'); 
defined('STRIPE_PUBLISHABLE_KEY') ? NULL : define('STRIPE_PUBLISHABLE_KEY','pk_test_1GZ3sUmVOzFYGwN9C6m3CikA');*/ 

defined('CURRENCY') ? NULL : define('CURRENCY','$');

/**************** Live Details Start ************************/

/*defined('STRIPE_API_SECRET_KEY') ? NULL : define('STRIPE_API_SECRET_KEY','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');
defined('STRIPE_PUBLISHABLE_KEY') ? NULL : define('STRIPE_PUBLISHABLE_KEY','pk_live_O9rN2m3Wid2c3l7lFFO5yOaX00o3fxUzFm'); 

defined('PARENT_PRICE_MONTH_ID') ? NULL : define('PARENT_PRICE_MONTH_ID','price_1HgbB8KvioPuR3kydlviKh1V');
defined('PARENT_PRICE_YEAR_ID') ? NULL : define('PARENT_PRICE_YEAR_ID','price_1HgbB3KvioPuR3kys5WkzdJc');

defined('PRONAN_PRICE_MONTH_ID') ? NULL : define('PRONAN_PRICE_MONTH_ID','price_1HgbAwKvioPuR3kySVhPPfIJ');
defined('PRONAN_PRICE_YEAR_ID') ? NULL : define('PRONAN_PRICE_YEAR_ID','price_1HgbAwKvioPuR3kygHj3cl1J');*/

/**************** Live Details End ************************/

/**************** Test Details Start ************************/

defined('STRIPE_API_SECRET_KEY') ? NULL : define('STRIPE_API_SECRET_KEY','sk_test_51GGn2iKvioPuR3kyigLS1u9jtdLRoAt5WSZSz1cnEzDc3QVpQtik28AAYX7klYKt0g8VMcdoWA3TVCuu0RDPdMkU00WCAvoe38');
defined('STRIPE_PUBLISHABLE_KEY') ? NULL : define('STRIPE_PUBLISHABLE_KEY','pk_test_rxdCDPX8RsV7g8Q8ZBvqUVDP00yhQv9QRS'); 

defined('PARENT_PRICE_MONTH_ID') ? NULL : define('PARENT_PRICE_MONTH_ID','price_1HdyjLKvioPuR3kydwkTPEN6');
defined('PARENT_PRICE_YEAR_ID') ? NULL : define('PARENT_PRICE_YEAR_ID','price_1HdykDKvioPuR3kygRA7sNSN');

defined('PRONAN_PRICE_MONTH_ID') ? NULL : define('PRONAN_PRICE_MONTH_ID','price_1Hec6hKvioPuR3kyQFHgcmXA');
defined('PRONAN_PRICE_YEAR_ID') ? NULL : define('PRONAN_PRICE_YEAR_ID','price_1Hec6hKvioPuR3ky9MUF9rlO');

/**************** Test Details End ************************/

defined('PARENT_MONTH_PRICE') ? NULL : define('PARENT_MONTH_PRICE', 5.00);
defined('PARENT_YEAR_PRICE') ? NULL : define('PARENT_YEAR_PRICE', 50.00);

defined('PRONAN_MONTH_PRICE') ? NULL : define('PRONAN_MONTH_PRICE', 15.00);
defined('PRONAN_YEAR_PRICE') ? NULL : define('PRONAN_YEAR_PRICE',150.00);


// defined('CSS_URL') ? NULL : define('CSS_URL', HOME_URL . 'css' . RDS);
// defined('JS_URL') ? NULL : define('JS_URL', HOME_URL . 'js' . RDS);
// defined('INCLUDE_URL') ? NULL : define('INCLUDE_URL', HOME_URL . 'includes' . RDS);
// defined('UPLOADS_URL') ? NULL : define('UPLOADS_URL', HOME_URL . 'uploads' . RDS);

////////////////////////////////////////////////////////////////////////////////
// Include library, helpers, functions
////////////////////////////////////////////////////////////////////////////////
require_once FUNCTION_PATH . 'functions.inc.php';
require_once LIB_PATH . 'database.class.php';

// $model_database = new Database();
// $variables = $model_database->executeSqlQueryGetData('SELECT * FROM tbl_company');
// if (!empty($variables)) {
// 	foreach ($variables[0] as $key => $value) {
// 		define('CP_' . strtoupper($key), $value);
// 	}
// }
?>