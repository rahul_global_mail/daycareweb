<?php
ob_start();
require_once 'user-includes/config.inc.php';
require_once USER_MODEL_PATH . 'parent-management.model.php';
$model_parent = new ModelParentmanage();
$parent_id = $_SESSION['parentData'][0]->parent_id;
$chilData = $model_parent->get_parentchilds($parent_id);
$cityData = $model_parent->get_allcity();
$parentData = $model_parent->get_parentdetail($parent_id);
$bookNanny = $model_parent->get_booknanny($parent_id);
$waitlist = $model_parent->get_waitlist($parent_id);
$vacancydata = $model_parent->get_vacancyalert($parent_id);
$subscriptions = $model_parent->get_parentSubscriptions($parent_id);

$redirect = "account-Parent.php";
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$_SESSION['add_msg'] = '';
	if (isset($_POST['updateParent'])) {
		$res = $model_parent->parent_update($_POST);
		if ($res > 0) {
			$_SESSION['add_msg'] = 'success';
			header("Location:" . $redirect);
			exit;
		} else {
			$_SESSION['add_msg'] = 'error';
			header("Location:" . $failredirect);
			exit;
		}
	}
}
require_once USER_VIEW_PATH . 'account-parent.view.php';

?>
