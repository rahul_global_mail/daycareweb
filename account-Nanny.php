<?php

ob_start();
require_once 'user-includes/config.inc.php';
require_once USER_MODEL_PATH . 'nanny-management.model.php';
$model_nanny = new ModelNannymanage();
$nanny_id = $_SESSION['nannyData'][0]->nanny_id;
$nannyData = $model_nanny->get_nannyDetail($nanny_id);
$bookingdata = $model_nanny->get_nannybooking($nanny_id);
$cityData = $model_nanny->get_allcity();
$benefitData = $model_nanny->get_allbenefits();
$subscriptions = $model_nanny->get_nannySubscriptions($nanny_id);
$redirect = "account-Nanny.php";
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$_SESSION['add_msg'] = '';
	if (isset($_POST['nanny_profile_update'])) {
		$res = $model_nanny->nanny_profile_update($_POST);
		if ($res > 0) {
			$_SESSION['add_msg'] = 'success';
			header("Location:" . $redirect);
			exit;
		} else {
			$_SESSION['add_msg'] = 'error';
			header("Location:" . $failredirect);
			exit;
		}
	}
	if (isset($_POST['updateJobdetail'])) {
		$res = $model_nanny->nanny_job_update($_POST);
		if ($res > 0) {
			$_SESSION['add_msg'] = 'success';
			header("Location:" . $redirect);
			exit;
		} else {
			$_SESSION['add_msg'] = 'error';
			header("Location:" . $failredirect);
			exit;
		}
	}
}
require_once USER_VIEW_PATH . 'account-nanny.view.php';
?>