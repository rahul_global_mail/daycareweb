<?php
class ModelNannymanage {
	private $model_database;
	private $table_database;

	public function __construct() {
		$this->model_database = new Database();
		$this->table_database = "dk_nanny";
		$this->table_database_profile = "dk_nanny_profiledata";
		$this->table_database_subscriptions = "dk_subscriptions";
	}

	//benefits / services
	public function get_allbenefits() {
		$sql = "SELECT * FROM dk_nanny_services";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	//city
	public function get_allcity() {
		$sql = "SELECT * FROM search_location GROUP BY city";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	// data exist
	public function nanny_chkExist($post) {
		$sql = "SELECT nanny_id,email_id FROM " . $this->table_database . " WHERE email_id = '" . $post['email_id'] . "' ";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	// get nanny user
	public function get_nannyDetail($nanny_id) {
		$sql = "SELECT t1.nanny_id,t1.subscription,t1.type,t1.email_id,t1.fname,t1.lname,t1.contact,t1.address,t1.suburb,t1.postcode,t1.city,t1.country,t1.status,t2.* FROM dk_nanny as t1 INNER JOIN dk_nanny_profiledata as t2 ON t1.nanny_id=t2.nanny_id WHERE t1.nanny_id = '" . $nanny_id . "'";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	//Login Detail
	public function get_NannyLogin($post) {
		$sql = "SELECT * FROM " . $this->table_database . " WHERE email_id = '" . $post['email'] . "'";
		$sql1 = $this->model_database->executeSqlQueryGetData($sql);
		$getpassword = $sql1[0]->password;
		$password = password_verify($post['password'], $getpassword);
		if ($password > 0) {
			return $sql1;
		} else {
			return false;
		}
		return $this->model_database->executeSqlQueryGetData($sql);
	}

	// Get Subscriptions
	public function get_nannySubscriptions($provider_id) {
		$sql = "SELECT * FROM " . $this->table_database_subscriptions . " WHERE user_id = '" . $provider_id . "' AND user_type = 1 ";
		return $this->model_database->executeSqlQueryGetData($sql);
	}

	public function nanny_payment_update($data){
		extract($data);
		$sql = "INSERT INTO " . $this->table_database_subscriptions . "(`user_type`, `user_id`, `stripe_subscription_id`, `stripe_customer_id`, `plan_amount`, `plan_amount_currency`, `plan_interval`, `plan_interval_count`, `payer_email`, `created`, `enddate`, `status`) VALUES ( 1, '" . $user_id . "', '" . $stripe_subscription_id . "', '" . $stripe_customer_id . "', '" . $plan_amount . "', '" . $plan_amount_currency . "', '" . $plan_interval . "', '" . $plan_interval_count . "', '" . $payer_email . "', '" . $created . "', '" . $enddate . "', '" . $status . "')";
		$subscription_id = $this->model_database->executeSqlQuery($sql);


		$sql_upd = "UPDATE " . $this->table_database . " SET
			`customer_id`='" . $stripe_customer_id . "',
			`subscription_id`='" . $stripe_subscription_id . "',
			`subscription` = 'paid' 
			WHERE nanny_id = '" . $user_id . "' ";
		$this->model_database->executeSqlUpdateQuery($sql_upd);
		return $subscription_id;
	}

	public function nanny_save($post) {
		$getdate = date("Y-m-d H:i:s");
		$guid = getGUID();
		$address = $this->model_database->escapeString($post['address']);
		$about = $this->model_database->escapeString($post['about']);
		$website = $this->model_database->escapeString($post['website']);
		$description = $this->model_database->escapeString($post['description']);
		$classification = $this->model_database->escapeString($post['classification']);
		$services = implode(",", $post['services']);
		$code = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"), 0, 6);
		//$code = "123456";
		$password = password_hash($code, PASSWORD_DEFAULT);

		$sql = "INSERT INTO " . $this->table_database . "(`subscription`, `type`, `email_id`, `password`, `fname`, `lname`, `contact`, `address`, `suburb`,`lat`, `lng`, `postcode`, `city`, `country`, `created_date`, `updated_date`) VALUES ('free', '" . $post['type'] . "', '" . $post['email_id'] . "', '" . $password . "', '" . $post['fname'] . "', '" . $post['lname'] . "', '" . $post['contact'] . "', '" . $address . "', '" . $post['suburb'] . "','".$post['lat']."', '".$post['long']."', '" . $post['postcode'] . "', '" . $$post['city'] . "', '" . $post['country'] . "', '" . $getdate . "', '" . $getdate . "')";

		$nanny_id = $this->model_database->executeSqlQuery($sql);
		// add nanny details
		$sqlDetails = "INSERT INTO " . $this->table_database_profile . "(`nanny_id`,`photo`, `fullname`,`description`,`classification`, `about`, `website`, `availablity`, `salary`, `salary_type`, `services`,`skills`, `job_location`, `job_suburb`) VALUES ('" . $nanny_id . "','default.jpg', '" . $post['fullname'] . "','" . $description . "','" . $classification . "', '" . $about . "', '" . $website . "', '" . $post['availablity'] . "', '" . $post['salary'] . "', '" . $post['salary_type'] . "', '" . $services . "','" . $post['skills'] . "', '" . $post['job_location'] . "', '" . $post['job_suburb'] . "')";

		$nanny_profile_id = $this->model_database->executeSqlQuery($sqlDetails);
		// send mail
		$to = $post['email_id'];
		$subject = 'Registration successfull on Daycareseekerer.com';
		$message = 'Hi ' . $post['fullname'] . ',<br><br>Great news, you are just one click away from accessing your account.<br><h5>Verify & activate your account</h5><br><br><p>So we know it was you who registered, click below.</p><br><br><a href="#" target="_blank">Verify & Activate »</a>Thanks.';
		$headers = 'From: info@akashworld.com' . "\r\n" .
		'MIME-Version: 1.0' . "\r\n" .
		'Content-Type: text/html; charset=UTF-8' . "\r\n" .
		'X-Mailer: PHP/' . phpversion();

		if (mail($to, $subject, $message, $headers)) {
			echo '';
		} else {
			echo "";
		}
		return $nanny_id;
	}
	// update data
	public function nanny_profile_update($post) {
		$getdate = date("Y-m-d H:i:s");
		$fullname = $this->model_database->escapeString($post['fullname']);
		$description = $this->model_database->escapeString($post['description']);
		$classification = $this->model_database->escapeString($post['classification']);
		$institute = $this->model_database->escapeString($post['institute']);
		$graduation = $this->model_database->escapeString($post['graduation']);

		$about = $this->model_database->escapeString($post['about']);
		$address = $this->model_database->escapeString($post['address']);
		if ($_FILES["photo"]["name"] != '') {
			$extension1 = explode('.', $_FILES['photo']['name']);
			$photo = rand() . '.' . $extension1[1];
			$destination1 = 'upload_images/' . $photo;
			move_uploaded_file($_FILES['photo']['tmp_name'], $destination1);
		} else {
			$photo = $post['get_photo'];
		}
		$sql = "UPDATE " . $this->table_database . " SET
			`email_id`='" . $post['email_id'] . "',
			`contact`='" . $post['contact'] . "',
			`fname`='" . $post['fname'] . "',
			`lname`='" . $post['lname'] . "',
			`address`='" . $address . "',
			`suburb`='" . $post['suburb'] . "',
			`postcode`='" . $post['postcode'] . "',
			`city`='" . $post['city'] . "',
			`country`='" . $post['country'] . "'
			WHERE `nanny_id` = '" . $post['nanny_id'] . "' ";
		$update = $this->model_database->executeSqlUpdateQuery($sql);
		// update details
		$sqldetail = "UPDATE " . $this->table_database_profile . " SET
			`photo`='" . $photo . "',
			`fullname`='" . $fullname . "',
			`description`='" . $description . "',
			`classification`='" . $classification . "',
			`institute`='" . $institute . "',
			`graduation`='" . $graduation . "',
			`about`='" . $about . "',
			`website`='" . $post['website'] . "'
			WHERE `nanny_id` = '" . $post['nanny_id'] . "' ";
		$updatedetail = $this->model_database->executeSqlUpdateQuery($sqldetail);
		return $update;
	}
	// update job detail data
	public function nanny_job_update($post) {
		$getdate = date("Y-m-d H:i:s");
		$services = implode(",", $post['services']);
		$sql = "UPDATE " . $this->table_database_profile . " SET
			`availablity`='" . $post['availablity'] . "',
			`salary`='" . $post['salary'] . "',
			`salary_type`='" . $post['salary_type'] . "',
			`services`='" . $services . "',
			`skills`='" . $post['skills'] . "',
			`job_location`='" . $post['job_location'] . "',
			`job_suburb`='" . $post['job_suburb'] . "'
			WHERE `nanny_id` = '" . $post['job_nanny_id'] . "' ";
		return $this->model_database->executeSqlUpdateQuery($sql);
	}
	//change password
	public function accountPassword_nanny($post) {
		$sql = "SELECT * FROM " . $this->table_database . " WHERE nanny_id = '" . $post['id'] . "' AND status !='remove'";
		$sql1 = $this->model_database->executeSqlQueryGetData($sql);
		$getpassword = $sql1[0]->password;
		$password = password_verify($post['currentpassword'], $getpassword);
		if ($password > 0) {
			$getdate = date("Y-m-d H:i:s");
			$newpassword = password_hash($post['newpassword'], PASSWORD_DEFAULT);
			$sql = "UPDATE " . $this->table_database . " SET `password`='" . $newpassword . "',`updated_date`='" . $getdate . "' WHERE `nanny_id` = '" . $post['id'] . "'";
			return $this->model_database->executeSqlUpdateQuery($sql);
		} else {
			return false;
		}
	}
	//update deactive account
	public function accountStatus_nanny($id, $status) {
		$getdate = date("Y-m-d H:i:s");
		$sql = "UPDATE " . $this->table_database . " SET `status`='" . $status . "',`updated_date`='" . $getdate . "' WHERE `nanny_id` = '" . $id . "'";
		return $this->model_database->executeSqlUpdateQuery($sql);
	}
	// get nanny booking
	public function get_nannybooking($nanny_id) {
		$sql = "SELECT * FROM dk_book_nanny WHERE nanny_id = '" . $nanny_id . "'";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	//update book account
	public function bookingStatus_nanny($book_nanny_id, $status) {
		$sql = "UPDATE dk_book_nanny SET `booking_status`='" . $status . "' WHERE `book_nanny_id` = '" . $book_nanny_id . "'";
		// send mail
		$getcontactmail = "SELECT `contact_email` FROM `dk_book_nanny` WHERE `book_nanny_id` = '" . $book_nanny_id . "'";
		$datacontactmail = $this->model_database->executeSqlQueryGetData($getcontactmail);

		$to = $datacontactmail[0]->contact_email;
		$subject = 'Daycareseekerer.com | Booking Request';
		$message = 'Daycareseekerer.com<br><br>This is inform you to you request for booking Nanny / Babysitter has been <b> ' . $status . '.<br><br><br>Thanks.';
		$headers = 'From: info@akashworld.com' . "\r\n" .
		'MIME-Version: 1.0' . "\r\n" .
		'Content-Type: text/html; charset=UTF-8' . "\r\n" .
		'X-Mailer: PHP/' . phpversion();

		if (mail($to, $subject, $message, $headers)) {
			echo '';
		} else {
			echo "";
		}
		return $this->model_database->executeSqlUpdateQuery($sql);
	}

}