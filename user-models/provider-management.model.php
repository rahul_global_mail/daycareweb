<?php
class ModelProvidermanage {
	private $model_database;
	private $table_database;

	public function __construct() {
		$this->model_database = new Database();
		$this->table_database = "dk_provider";
		$this->table_database_vacancy = "dk_provider_vacancy";
		$this->table_database_waitlist = "dk_waitlist";
		$this->table_database_subscriptions = "dk_subscriptions";

	}
	// service list
	public function get_careservices() {
		$sql = "SELECT * FROM dk_provider_services";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	// service list
	public function getsearch_get_careservices($id) {
		$sql = "SELECT * FROM dk_provider_services WHERE provider_service_id = '" . $id . "'";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	// benefit list
	public function get_carebenefits() {
		$sql = "SELECT * FROM dk_care_benefits";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	// city list
	public function get_city() {
		$sql = "SELECT id,city FROM search_location group by city";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	// data exist
	public function careprovider_chkExist($post) {
		$sql = "SELECT provider_id,provider_email,customer_id,subscription_id FROM " . $this->table_database . " WHERE provider_email = '" . $post['provider_email'] . "' ";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	//Login Detail
	public function get_careproviderLogin($post) {
		$sql = "SELECT * FROM " . $this->table_database . " WHERE provider_email = '" . $post['email'] . "'";
		$sql1 = $this->model_database->executeSqlQueryGetData($sql);
		$getpassword = $sql1[0]->password;
		$password = password_verify($post['password'], $getpassword);
		if ($password > 0) {
			return $sql1;
		} else {
			return false;
		}
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	// get provider
	public function get_providerDetail($provider_id) {
		$sql = "SELECT * FROM " . $this->table_database . " WHERE provider_id = '" . $provider_id . "' ";
		return $this->model_database->executeSqlQueryGetData($sql);
	}

	// Get Subscriptions
	public function get_providerSubscriptions($provider_id) {
		$sql = "SELECT * FROM " . $this->table_database_subscriptions . " WHERE user_id = '" . $provider_id . "' AND user_type = 0 ";
		return $this->model_database->executeSqlQueryGetData($sql);
	}

	//update subscription detaild 

	public function provider_payment_update($data){
		extract($data);
		$sql = "INSERT INTO " . $this->table_database_subscriptions . "(`user_type`, `user_id`, `stripe_subscription_id`, `stripe_customer_id`, `plan_amount`, `plan_amount_currency`, `plan_interval`, `plan_interval_count`, `payer_email`, `created`, `enddate`, `status`) VALUES ( 0, '" . $user_id . "', '" . $stripe_subscription_id . "', '" . $stripe_customer_id . "', '" . $plan_amount . "', '" . $plan_amount_currency . "', '" . $plan_interval . "', '" . $plan_interval_count . "', '" . $payer_email . "', '" . $created . "', '" . $enddate . "', '" . $status . "')";
		$subscription_id = $this->model_database->executeSqlQuery($sql);


		$sql_upd = "UPDATE " . $this->table_database . " SET
			`customer_id`='" . $stripe_customer_id . "',
			`subscription_id`='" . $stripe_subscription_id . "' 
			WHERE provider_id = '" . $user_id . "' ";
		$this->model_database->executeSqlUpdateQuery($sql_upd);
		return $subscription_id;
	}

	public function provider_sub_update(){
		
	}

	// add data
	public function provider_save($post) {
		$getdate = date("Y-m-d H:i:s");
		$guid = getGUID();
		$contact_name = $this->model_database->escapeString($post['contact_name']);
		$provider_name = $this->model_database->escapeString($post['provider_name']);
		$address = $this->model_database->escapeString($post['address']);

		if(isset($_POST['price_id'])){
			$subscription = 'paid'; 
		}else{
			$subscription = 'free';
		}
		$code = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"), 0, 6);
		//$code = "123456";
		$password = password_hash($code, PASSWORD_DEFAULT);
		$provider_service_id = implode(",", $post['provider_service_id']);
		$sql = "INSERT INTO " . $this->table_database . "(`subscription`, `provider_name`, `provider_service_id`, `contact_name`, `provider_email`, `password`, `address`, `suburb`, `lat`, `lng`, `postcode`, `city`, `country`, `phone`,`photo`, `is_vacancy`, `vacancy_start_on`, `add_waitlist`, `waitlist_link`, `waitlist_fee`, `waitlist_policy`, `web_app_link`, `benifits`, `created_date`) VALUES ('".$subscription."', '" . $provider_name . "', '" . $provider_service_id . "', '" . $contact_name . "', '" . $post['provider_email'] . "', '" . $password . "', '" . $address . "', '" . $post['suburb'] . "','".$post['lat']."', '".$post['long']."', '" . $post['postcode'] . "', '" . $post['city'] . "', '" . $post['country'] . "', '" . $post['phone'] . "','default.jpg', '" . $post['is_vacancy'] . "', '" . $post['vacancy_start_on'] . "', '" . $post['add_waitlist'] . "', '" . $post['waitlist_link'] . "', '" . $post['waitlist_fee'] . "', '" . $post['waitlist_policy'] . "', '" . $post['web_app_link'] . "', '" . $post['benifits'] . "', '" . $getdate . "')";

		$provider_id = $this->model_database->executeSqlQuery($sql);
		if (isset($post['from_age']) && !empty($post['from_age']) && $post['is_vacancy'] != 'No') {
			foreach ($post['from_age'] as $key => $vacancydata) {
				$sqlprovider_vacancy = "INSERT INTO `dk_provider_vacancy`(`provider_id`, `from_age`, `from_age_type`, `to_age`, `to_age_type`, `days`, `created_date`) VALUES ('" . $provider_id . "', '" . $post['from_age'][$key] . "', '" . $post['from_age_type'][$key] . "', '" . $post['to_age'][$key] . "', '" . $post['to_age_type'][$key] . "', '2', '" . $getdate . "')";
				$provider_vacancy = $this->model_database->executeSqlQuery($sqlprovider_vacancy);
			}

		}

		// send mail
		$to = $post['provider_email'];
		$subject = 'Registration successfull on Daycareseekerer.com';
		$message = 'Welcome to Daycareseekerer.com!<br><br>Here are your credentials to login and get going!Make sure you have to wait till Admin approve your profile.<br><b>Email : ' . $post['provider_email'] . '<br>Password : ' . $code . '</b><br><br>Thanks.';
		$headers = 'From: info@akashworld.com' . "\r\n" .
		'MIME-Version: 1.0' . "\r\n" .
		'Content-Type: text/html; charset=UTF-8' . "\r\n" .
		'X-Mailer: PHP/' . phpversion();

		if (mail($to, $subject, $message, $headers)) {
			echo '';
		} else {
			echo "";
		}
		return $provider_id;
	}
	// update profile detail data
	public function update_careprofile($post) {
		$getdate = date("Y-m-d H:i:s");
		$address = $this->model_database->escapeString($post['address']);
		$about = $this->model_database->escapeString($post['about']);
		$sql = "UPDATE " . $this->table_database . " SET
			`contact_name`='" . $post['contact_name'] . "',
			`provider_email`='" . $post['provider_email'] . "',
			`address`='" . $address . "',
			`suburb`='" . $post['suburb'] . "',
			`postcode`='" . $post['postcode'] . "',
			`city`='" . $post['city'] . "',
			`country`='" . $post['country'] . "',
			`phone`='" . $post['phone'] . "',
			`about`='" . $about . "',
			`web_app_link` = '" . $post['web_app_link'] . "',
			`inspection_link` = '" . $post['inspection_link'] . "',
			`fb_link` = '" . $post['fb_link'] . "',
			`approved_places` = '" . $post['approved_places'] . "',
			`updated_date` = '" . $getdate . "'
			WHERE `provider_id` = '" . $post['profile_provider_id'] . "' ";
		return $this->model_database->executeSqlUpdateQuery($sql);
	}
	// update waitlist detail data
	public function update_profile_waitlist($post) {
		$getdate = date("Y-m-d H:i:s");
		$address = $this->model_database->escapeString($post['address']);
		$sql = "UPDATE " . $this->table_database . " SET
			`add_waitlist`='" . $post['add_waitlist'] . "',
			`waitlist_link`='" . $post['waitlist_link'] . "',
			`waitlist_fee`='" . $post['waitlist_fee'] . "',
			`waitlist_policy`='" . $post['waitlist_policy'] . "',
			`updated_date` = '" . $getdate . "'
			WHERE `provider_id` = '" . $post['waitlist_provider_id'] . "' ";
		return $this->model_database->executeSqlUpdateQuery($sql);
	}
	// update service detail data
	public function update_service_details($post) {
		$getdate = date("Y-m-d H:i:s");
		$benifits = implode(",", $post['benifits']);
		$provider_service_id = implode(",", $post['provider_service_id']);
		$provider_name = $this->model_database->escapeString($post['provider_name']);
		if ($_FILES["photo"]["name"] != '') {
			$extension1 = explode('.', $_FILES['photo']['name']);
			$photo = rand() . '.' . $extension1[1];
			$destination1 = 'upload_images/' . $photo;
			move_uploaded_file($_FILES['photo']['tmp_name'], $destination1);
		} else {
			$photo = $post['get_photo'];
		}
		$sql = "UPDATE " . $this->table_database . " SET
			`provider_name`='" . $provider_name . "',
			`provider_service_id`='" . $provider_service_id . "',
			`photo` = '" . $photo . "',
			`benifits`='" . $benifits . "',
			`updated_date` = '" . $getdate . "'
			WHERE `provider_id` = '" . $post['service_provider_id'] . "' ";
		return $this->model_database->executeSqlUpdateQuery($sql);
	}
	//change password
	public function accountPassword_provider($post) {
		$sql = "SELECT * FROM " . $this->table_database . " WHERE provider_id = '" . $post['id'] . "' AND status !='remove'";
		$sql1 = $this->model_database->executeSqlQueryGetData($sql);
		$getpassword = $sql1[0]->password;
		$password = password_verify($post['currentpassword'], $getpassword);
		if ($password > 0) {
			$getdate = date("Y-m-d H:i:s");
			$newpassword = password_hash($post['newpassword'], PASSWORD_DEFAULT);
			$sql = "UPDATE " . $this->table_database . " SET `password`='" . $newpassword . "',`updated_date`='" . $getdate . "' WHERE `provider_id` = '" . $post['id'] . "'";
			return $this->model_database->executeSqlUpdateQuery($sql);
		} else {
			return false;
		}
	}
	// care fees
	public function add_care_fees($post) {
		$getdate = date("Y-m-d H:i:s");
		$notes = $this->model_database->escapeString($post['notes']);
		$sql = "INSERT INTO `dk_care_fees`(`provider_id`, `provider_service_id`, `price`, `type`, `ages`, `notes`, `created_date`) VALUES ('" . $post['fees_provider_id'] . "', '0', '" . $post['price'] . "', '" . $post['type'] . "', '" . $post['ages'] . "', '" . $notes . "', '" . $getdate . "')";

		return $this->model_database->executeSqlQuery($sql);
	}
	//get my vacancy
	public function get_myVacancy($provider_id) {
		$sql = "SELECT * FROM dk_provider_vacancy WHERE provider_id ='" . $provider_id . "'";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	//get my vacancy
	public function get_carelatlong($postcode) {
		$sql = "SELECT * FROM `postcodes` WHERE `postcode` = '" . $postcode . "' ORDER BY `postcode` ASC";
		return $this->model_database->executeSqlQueryGetData($sql);
	}

	// service list
	public function day_get_vacancy($id, $day) {
		$sql = "SELECT count(vacancy_id) as day FROM `dk_provider_vacancy` WHERE `vacancy_id` = '" . $id . "' AND `days` LIKE '%" . $day . "%'";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	//get fees
	public function get_myFees($provider_id) {
		$sql = "SELECT t1.*,t2.title FROM dk_care_fees as t1 LEFT JOIN dk_provider_services as t2 ON t1.provider_service_id=t2.provider_service_id WHERE t1.provider_id ='" . $provider_id . "'";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	// gallery
	public function get_myGallery($provider_id) {
		$sql = "SELECT * FROM dk_care_banners WHERE `provider_id` ='" . $provider_id . "'";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	// add banner
	public function upload_banner($post) {
		$getdate = date("Y-m-d H:i:s");
		if ($_FILES['banner']) {
			foreach ($_FILES['banner']['tmp_name'] as $key => $imagedata) {
				// image upload
				$extension = explode('.', $_FILES['banner']['name'][$key]);
				$getimage_path = rand() . '.' . $extension[1];
				$destination = 'upload_images/' . $getimage_path;
				move_uploaded_file($_FILES['banner']['tmp_name'][$key], $destination);
				$sqlImage = "INSERT INTO `dk_care_banners` (`provider_id`, `banner_path`,`created_date`) VALUES ('" . $post['banner_provider_id'] . "','" . $getimage_path . "','" . $getdate . "')";
				$addQuery = $this->model_database->executeSqlQuery($sqlImage);
			}
		}
		return $post['banner_provider_id'];
	}
	//Delete banner
	public function removebanner($banner_id) {
		$sql = "DELETE FROM dk_care_banners WHERE banner_id= '" . $banner_id . "' ";
		return $this->model_database->executeSqlQuery($sql);
	}
	//update deactive account
	public function accountStatus_provider($id, $status) {
		$getdate = date("Y-m-d H:i:s");
		$sql = "UPDATE " . $this->table_database . " SET `status`='" . $status . "',`updated_date`='" . $getdate . "' WHERE `provider_id` = '" . $id . "'";
		return $this->model_database->executeSqlUpdateQuery($sql);
	}
	// parent waitlist details
	public function get_provider_waitlist($provider_id) {
		$sql = "SELECT * FROM " . $this->table_database_waitlist . " WHERE `provider_id` = '" . $provider_id . "'";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	//update waitlist account
	public function waitlitStatus_provider($waitlist_id, $status) {
		$getdate = date("Y-m-d H:i:s");
		$sql = "UPDATE " . $this->table_database_waitlist . " SET `status`='" . $status . "' WHERE `waitlist_id` = '" . $waitlist_id . "'";
		// send mail
		$getcontactmail = "SELECT `email` FROM " . $this->table_database_waitlist . " WHERE `waitlist_id` = '" . $waitlist_id . "'";
		$datacontactmail = $this->model_database->executeSqlQueryGetData($getcontactmail);

		$to = $datacontactmail[0]->contact_email;
		$subject = 'Daycareseekerer.com | Booking Request';
		$message = 'Daycareseekerer.com<br><br>This is inform you to you request for Waitlist has been <b> ' . $status . '.<br><br><br>Thanks.';
		$headers = 'From: info@akashworld.com' . "\r\n" .
		'MIME-Version: 1.0' . "\r\n" .
		'Content-Type: text/html; charset=UTF-8' . "\r\n" .
		'X-Mailer: PHP/' . phpversion();

		if (mail($to, $subject, $message, $headers)) {
			echo '';
		} else {
			echo "";
		}
		return $this->model_database->executeSqlUpdateQuery($sql);
	}
	public function update_profile_vancancy($post) {
		$getdate = date("Y-m-d H:i:s");
		if ($post['vacanciesvacancy_start_on'] == 'on') {
			$vacancy_start_on = date_format(date_create($post['vacancy_start_value']), "Y-m-d");
		} else {
			$vacancy_start_on = "Now";
		}

		$sql = "UPDATE `dk_provider` SET `is_vacancy`='" . $post['is_vacancy'] . "',`vacancy_start_on`='" . $vacancy_start_on . "' WHERE `provider_id` = '" . $post['vacancyprovider_id'] . "'";
		$updatedata = $this->model_database->executeSqlUpdateQuery($sql);
		//vacancy data
		if (isset($post['from_age'])) {
			echo "<pre>";
			foreach ($post['from_age'] as $key => $vacancydata) {
				$nums = $key + 1;
				$arrDay = $post["days$nums"];
				$arr = array();
				foreach ($arrDay as $j => $d1) {
					$arr[] = $d1;
				}
				$daysimlode = implode(",", $arr);
				//echo "<br>";
				$sqlprovider_vacancy = "INSERT INTO `dk_provider_vacancy`(`provider_id`, `from_age`, `from_age_type`, `to_age`, `to_age_type`, `days`,`start_from`, `created_date`) VALUES ('" . $post['vacancyprovider_id'] . "', '" . $vacancydata . "', '" . $post['from_age_type'][$key] . "', '" . $post['to_age'][$key] . "', '" . $post['to_age_type'][$key] . "', '" . $daysimlode . "','" . $post['start_from'][$key] . "', '" . $getdate . "')";
				$provider_vacancy = $this->model_database->executeSqlQuery($sqlprovider_vacancy);
			}
		}
		return $updatedata;
	}
	// parent vacancy details
	public function get_provider_vacancy($provider_id) {
		$sql = "SELECT * FROM `dk_provider_vacancy` WHERE `provider_id` = '" . $provider_id . "'";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	// delete vacancy
	public function removevancancy($vancancy_id) {
		$sql = "DELETE FROM dk_provider_vacancy WHERE vacancy_id= '" . $vancancy_id . "' ";
		return $this->model_database->executeSqlQuery($sql);
	}
	// delete care fees
	public function removedeleteCarefees($fees_id) {
		$sql = "DELETE FROM dk_care_fees WHERE fees_id= '" . $fees_id . "' ";
		return $this->model_database->executeSqlQuery($sql);
	}

}