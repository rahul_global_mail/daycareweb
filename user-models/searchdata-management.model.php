<?php
class ModelSearchdata {
	private $model_database;
	private $table_database;

	public function __construct() {
		$this->model_database = new Database();
	}
	// service list
	public function search_get_careservices() {
		$sql = "SELECT * FROM dk_provider_services";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	// service list
	public function getsearch_get_careservices($id) {
		$sql = "SELECT * FROM dk_provider_services WHERE provider_service_id = '" . $id . "'";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	// parent list
	public function search_get_allnannyusers($type, $locationdata, $get) {
		//$type = babysitter , nanny;
		if ($type == 'babysitter') {
			$qry = " ORDER BY `t1`.`type` ASC";
		} else {
			$qry = " ORDER BY `t1`.`type` DESC";
		}

		$where = " AND t1.status = 'active'";

		if($get['lat'] != '' && $get['lng'] != '' && $get['radius'] != ''){
			$distance = explode('-', $get['radius']);
			 $where .= " HAVING distance < $distance[1]";
		}


		$sql = "SELECT t1.*, SQRT( POW(69.1 * (t1.lat - 23.0095), 2) + POW(69.1 * (72.5109 - t1.lng) * COS(t1.lat / 57.3), 2)) AS distance ,t2.photo,t2.fullname,t2.classification,t2.website,t2.salary,t2.salary_type,t2.availablity,t2.services FROM dk_nanny as t1 INNER JOIN dk_nanny_profiledata as t2 ON t1.nanny_id=t2.nanny_id WHERE 1 = 1 $where $qry";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	// parent list
	public function search_get_allprovider($type, $locationdata, $service, $radius, $is_vacancy, $get) {
		if (isset($locationdata) && !empty($locationdata)) {
			$qryLocation = " AND city = '" . $locationdata . "'";
		} else {
			$qryLocation = "";
		}
		if (isset($is_vacancy) && !empty($is_vacancy)) {
			$qryvacancy = " AND is_vacancy = '" . $is_vacancy . "'";
		} else {
			$qryvacancy = "";
		}

		$where = " AND status = 'active'";

		if($get['lat'] != '' && $get['lng'] != '' && !empty($get['radius'])){
			$distance = explode('-', $get['radius']);
			$where .= " HAVING distance < $distance[1]";
		}

		$sql = "SELECT *,  SQRT( POW(69.1 * (lat - 23.0095), 2) + POW(69.1 * (72.5109 - lng) * COS(lat / 57.3), 2)) AS distance FROM dk_provider WHERE 1 = 1 $where $qryLocation $qryvacancy";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	public function get_service($id) {
		$sql = "SELECT * FROM `dk_nanny_services` WHERE `service_id` = '" . $id . "'";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
}