<?php
class ModelParentmanage {
	private $model_database;
	private $table_database;

	public function __construct() {
		$this->model_database = new Database();
		$this->table_database = "dk_parents";
		$this->table_database_child = "dk_parent_childs";
		$this->table_database_subscriptions = "dk_subscriptions";
	}
	// postcode list
	public function get_allpostcode($keyword) {
		$sql = "SELECT * FROM postcodes WHERE `postcode` LIKE '%" . $keyword . "%' OR `city` LIKE '%" . $keyword . "%' LIMIT 10";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	// city list
	public function get_allcitilocation($keyword) {
		$sql = "SELECT * FROM postcodes WHERE `city` LIKE '%" . $keyword . "%' GROUP BY city LIMIT 10";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	// postcode list
	public function get_allpostcodelocation($keyword) {
		$sql = "SELECT * FROM postcodes WHERE `postcode` LIKE '%" . $keyword . "%' GROUP BY postcode LIMIT 10";
		return $this->model_database->executeSqlQueryGetData($sql);
	}

	public function get_allcity() {
		$sql = "SELECT * FROM search_location GROUP BY city";
		return $this->model_database->executeSqlQueryGetData($sql);
	}

	// data exist
	public function parent_chkExist($post) {
		$sql = "SELECT parent_id,parent_email FROM " . $this->table_database . " WHERE parent_email = '" . $post['parent_email'] . "' ";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	//Login Detail
	public function get_ParentLogin($post) {
		$sql = "SELECT * FROM " . $this->table_database . " WHERE parent_email = '" . $post['email'] . "' AND status !='remove'";
		$sql1 = $this->model_database->executeSqlQueryGetData($sql);
		$getpassword = $sql1[0]->password;
		$password = password_verify($post['password'], $getpassword);
		if ($password > 0) {
			return $sql1;
		} else {
			return false;
		}
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	// get parent
	public function get_parentdetail($parent_id) {
		$sql = "SELECT * FROM " . $this->table_database . " WHERE `parent_id` = '" . $parent_id . "'";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	//update deactive account
	public function accountStatus_parent($id, $status) {
		$getdate = date("Y-m-d H:i:s");
		$sql = "UPDATE `dk_parents` SET `status`='" . $status . "',`updated_date`='" . $getdate . "' WHERE `parent_id` = '" . $id . "'";
		return $this->model_database->executeSqlUpdateQuery($sql);
	}
	//update email account
	public function accountEmail_parent($id, $email) {
		$getdate = date("Y-m-d H:i:s");
		$sql = "UPDATE `dk_parents` SET `parent_email`='" . $email . "',`updated_date`='" . $getdate . "' WHERE `parent_id` = '" . $id . "'";
		return $this->model_database->executeSqlUpdateQuery($sql);
	}
	public function accountPassword_parent($post) {
		$sql = "SELECT * FROM " . $this->table_database . " WHERE parent_id = '" . $post['id'] . "' AND status !='remove'";
		$sql1 = $this->model_database->executeSqlQueryGetData($sql);
		$getpassword = $sql1[0]->password;
		$password = password_verify($post['currentpassword'], $getpassword);
		if ($password > 0) {
			$getdate = date("Y-m-d H:i:s");
			$newpassword = password_hash($post['newpassword'], PASSWORD_DEFAULT);
			$sql = "UPDATE `dk_parents` SET `password`='" . $newpassword . "',`updated_date`='" . $getdate . "' WHERE `parent_id` = '" . $post['id'] . "'";
			return $this->model_database->executeSqlUpdateQuery($sql);
		} else {
			return false;
		}

	}

	// Get Subscriptions
	public function get_parentSubscriptions($parent_id) {
		$sql = "SELECT * FROM " . $this->table_database_subscriptions . " WHERE user_id = '" . $parent_id . "' AND user_type = 2 ";
		return $this->model_database->executeSqlQueryGetData($sql);
	}

	public function parent_payment_update($data){
		extract($data);
		$sql = "INSERT INTO " . $this->table_database_subscriptions . "(`user_type`, `user_id`, `stripe_subscription_id`, `stripe_customer_id`, `plan_amount`, `plan_amount_currency`, `plan_interval`, `plan_interval_count`, `payer_email`, `created`, `enddate`, `status`) VALUES ( 2, '" . $user_id . "', '" . $stripe_subscription_id . "', '" . $stripe_customer_id . "', '" . $plan_amount . "', '" . $plan_amount_currency . "', '" . $plan_interval . "', '" . $plan_interval_count . "', '" . $payer_email . "', '" . $created . "', '" . $enddate . "', '" . $status . "')";
		$subscription_id = $this->model_database->executeSqlQuery($sql);

		$sql_upd = "UPDATE " . $this->table_database . " SET
			`customer_id`='" . $stripe_customer_id . "',
			`subscription_id`='" . $stripe_subscription_id . "',
			`subscription` = 'paid' 
			WHERE parent_id = '" . $user_id . "' ";
		$this->model_database->executeSqlUpdateQuery($sql_upd);
		return $subscription_id;
	}

	public function parent_save($post) {
		$getdate = date("Y-m-d H:i:s");
		$guid = getGUID();
		$code = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"), 0, 6);
		$code = "123456";
		$password = password_hash($code, PASSWORD_DEFAULT);
		$suburb = explode("-", $post['postcode']);
		$sql = "INSERT INTO " . $this->table_database . "(`fname`, `lname`, `parent_email`, `password`, `lat`, `lng`, `postcode`,`city`,`verify_code`, `created_date`,`updated_date`) VALUES ('" . $post['fname'] . "', '" . $post['lname'] . "', '" . trim($post['parent_email']) . "', '" . $password . "','".$post['lat']."', '".$post['long']."', '" . $suburb[0] . "', '" . $suburb[0] . "', '" . $guid . "', '" . $getdate . "', '" . $getdate . "')";

		$parent_id = $this->model_database->executeSqlQuery($sql);
		// send mail
		$to = $post['parent_email'];
		$subject = 'Registration successfull on Daycareseekerer.com';
		$message = 'Welcome to Daycareseekerer.com!<br><br>Here are your credentials to login and get going!<br><b>Email : ' . $post['parent_email'] . '<br>Password : ' . $code . '</b>';
		$headers = 'From: info@akashworld.com' . "\r\n" .
		'MIME-Version: 1.0' . "\r\n" .
		'Content-Type: text/html; charset=UTF-8' . "\r\n" .
		'X-Mailer: PHP/' . phpversion();

		/*if (mail($to, $subject, $message, $headers)) {
			echo '';
		} else {
			echo "";
		}*/
		return $parent_id;
	}
	// update parent details
	public function parent_update($post) {
		$getdate = date("Y-m-d H:i:s");
		$about = $this->model_database->escapeString($post['about']);
		// if ($_FILES["photo"]["name"] != '') {
		// 	$extension1 = explode('.', $_FILES['photo']['name']);
		// 	$photo = rand() . '.' . $extension1[1];
		// 	$destination1 = 'driver_uploads/' . $photo;
		// 	move_uploaded_file($_FILES['photo']['tmp_name'], $destination1);
		// } else {
		// 	$photo = $post['get_photo'];
		// }
		$language = implode(",", $post['language']);
		//$suburb = explode("-", $post['postcode']);
		$sql = "UPDATE " . $this->table_database . " SET
			`fname`='" . $post['fname'] . "',
			`lname`='" . $post['lname'] . "',
			`address`='" . $post['address'] . "',
			`suburb`='" . $post['suburb'] . "',
			`postcode`='" . $post['postcode'] . "',
			`city`='" . $post['city'] . "',
			`phone`='" . $post['phone'] . "',
			`contact_preference`='" . $post['contact_preference'] . "',
			`occupation`='" . $post['occupation'] . "',
			`language`='" . $language . "',
			`about`='" . $about . "'
			WHERE `parent_id` = '" . $post['parent_id'] . "' ";
		$updateQry = $this->model_database->executeSqlUpdateQuery($sql);
		//add new
		if (isset($post['child_name']) && !empty($post['child_name'])) {
			foreach ($post['child_name'] as $key => $val) {
				$date = date_create($post['child_dob'][$key]);
				$get_date = date_format($date, "Y-m-d");
				echo $sqlAdd = "INSERT INTO " . $this->table_database_child . "(`parent_id`, `child_name`, `child_dob`, `updated_date`) VALUES ('" . $post['parent_id'] . "', '" . $val . "', '" . $get_date . "', '" . $getdate . "')";
				$child_id = $this->model_database->executeSqlQuery($sqlAdd);
			}

		}

		return $updateQry;

	}
	// parent childs
	public function get_parentchilds($parent_id) {
		$sql = "SELECT * FROM " . $this->table_database_child . " WHERE `parent_id` = '" . $parent_id . "'";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	//Delete child data
	public function remove_childata($parent_id, $child_id) {
		$sql = "DELETE FROM " . $this->table_database_child . " WHERE parent_id= '" . $parent_id . "' AND child_id = '" . $child_id . "' ";
		return $this->model_database->executeSqlQuery($sql);
	}
	// add book nanny
	public function booknanny_save($post) {
		$getdate = date("Y-m-d H:i:s");

		$sql = "INSERT INTO `dk_book_nanny`(`parent_id`, `nanny_id`, `contact_name`, `contact_email`, `contact_phone`, `from_date`, `fees`,`fees_type`, `comment`, `created_date`) VALUES ('" . $post['parent_id'] . "', '" . $post['nanny_id'] . "', '" . $post['contact_name'] . "', '" . $post['contact_email'] . "', '" . $post['contact_phone'] . "', '" . $post['from_date'] . "', '" . $post['fees'] . "', '" . $post['fees_type'] . "', '" . $post['comment'] . "', '" . $getdate . "')";

		$book_nanny_id = $this->model_database->executeSqlQuery($sql);
		// send mail
		/*$to = $post['parent_email'];
		$subject = 'Registration successfull on Daycareseekerer.com';
		$message = 'Welcome to Daycareseekerer.com!<br><br>Here are your credentials to login and get going!<br><b>Email : ' . $post['parent_email'] . '<br>Password : ' . $password . '</b>';
		$headers = 'From: info@akashworld.com' . "\r\n" .
		'MIME-Version: 1.0' . "\r\n" .
		'Content-Type: text/html; charset=UTF-8' . "\r\n" .
		'X-Mailer: PHP/' . phpversion();

		if (mail($to, $subject, $message, $headers)) {
			echo '';
		} else {
			echo "";
		}*/
		return $book_nanny_id;
	}
	// parent book nanny
	public function get_booknanny($parent_id) {
		$sql = "SELECT t1.book_nanny_id,t1.nanny_id,t3.photo,t1.from_date,t2.fname,t2.lname,t2.email_id,t2.contact FROM dk_book_nanny as t1 INNER JOIN dk_nanny as t2 ON t1.nanny_id=t2.nanny_id INNER JOIN dk_nanny_profiledata as t3 ON t2.nanny_id=t3.nanny_id WHERE t1.parent_id = '" . $parent_id . "'";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	// add waitlist
	public function providerwaitlit_save($post) {
		$getdate = date("Y-m-d H:i:s");
		$req_days = implode(",", $post['req_days']);
		$interest = implode(",", $post['interest']);
		$comments = $this->model_database->escapeString($post['comments']);
		$sql = "INSERT INTO `dk_waitlist`(`parent_id`, `provider_id`, `carer_fullname`, `email`, `mobile`, `child_fname`, `child_lname`, `child_dob`, `req_date`, `req_days`, `interest`, `referal_from`, `comments`, `created_date`) VALUES ('" . $post['parent_id'] . "', '" . $post['provider_id'] . "', '" . $post['carer_fullname'] . "', '" . $post['email'] . "', '" . $post['mobile'] . "', '" . $post['child_fname'] . "', '" . $post['child_lname'] . "', '" . $post['child_dob'] . "', '" . $post['req_date'] . "', '" . $req_days . "', '" . $interest . "', '" . $post['referal_from'] . "', '" . $comments . "', '" . $getdate . "')";

		$waitlist_id = $this->model_database->executeSqlQuery($sql);
		// send mail
		/*$to = $post['parent_email'];
		$subject = 'Registration successfull on Daycareseekerer.com';
		$message = 'Welcome to Daycareseekerer.com!<br><br>Here are your credentials to login and get going!<br><b>Email : ' . $post['parent_email'] . '<br>Password : ' . $password . '</b>';
		$headers = 'From: info@akashworld.com' . "\r\n" .
		'MIME-Version: 1.0' . "\r\n" .
		'Content-Type: text/html; charset=UTF-8' . "\r\n" .
		'X-Mailer: PHP/' . phpversion();

		if (mail($to, $subject, $message, $headers)) {
			echo '';
		} else {
			echo "";
		}*/
		return $waitlist_id;
	}
	// add vacancy alert
	public function vacancyalert_save($post) {
		$getdate = date("Y-m-d H:i:s");
		$sql = "INSERT INTO `dk_parent_vacancy_alert`(`parent_id`, `provider_id`, `created_date`) VALUES ('" . $post['vacancy_parent_id'] . "', '" . $post['provider_id'] . "', '" . $getdate . "')";

		$vacancy_alert_id = $this->model_database->executeSqlQuery($sql);
		// send mail
		/*$to = $post['parent_email'];
		$subject = 'Registration successfull on Daycareseekerer.com';
		$message = 'Welcome to Daycareseekerer.com!<br><br>Here are your credentials to login and get going!<br><b>Email : ' . $post['parent_email'] . '<br>Password : ' . $password . '</b>';
		$headers = 'From: info@akashworld.com' . "\r\n" .
		'MIME-Version: 1.0' . "\r\n" .
		'Content-Type: text/html; charset=UTF-8' . "\r\n" .
		'X-Mailer: PHP/' . phpversion();

		if (mail($to, $subject, $message, $headers)) {
			echo '';
		} else {
			echo "";
		}*/
		return $vacancy_alert_id;
	}
	// data exist alert
	public function vacancyalert_chkExist($post) {
		$sql = "SELECT parent_id,provider_id FROM dk_parent_vacancy_alert WHERE parent_id = '" . $post['vacancy_parent_id'] . "' AND provider_id = '" . $post['provider_id'] . "' ";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	// parent waitlist
	public function get_waitlist($parent_id) {
		$sql = "SELECT t1.child_fname,t1.child_lname,t1.req_date,t1.req_days,t2.provider_name,t2.provider_email,t2.photo,t2.provider_id FROM dk_waitlist as t1 INNER JOIN dk_provider as t2 ON t1.provider_id=t2.provider_id WHERE t1.parent_id = '" . $parent_id . "'";
		return $this->model_database->executeSqlQueryGetData($sql);
	}
	// parent vacancy alert
	public function get_vacancyalert($parent_id) {
		$sql = "SELECT t1.*,t2.provider_name,t2.provider_email,t2.photo FROM dk_parent_vacancy_alert as t1 INNER JOIN dk_provider as t2 ON t1.provider_id=t2.provider_id WHERE t1.parent_id = '" . $parent_id . "'";
		return $this->model_database->executeSqlQueryGetData($sql);
	}

}