<?php
ob_start();
require_once 'user-includes/config.inc.php';
if (isset($_GET['verifyID']) && !empty($_GET['verifyID']) && isset($_GET['action']) && $_GET['action'] == "verify") {
	require_once USER_VIEW_PATH . 'parent-success.view.php';
} else {
	require_once USER_VIEW_PATH . 'nodata.view.php';
}

?>